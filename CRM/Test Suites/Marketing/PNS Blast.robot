*** Settings ***
Resource    ../../Resources/Keywords/Global/GlobalSettings.robot

*** Test Cases ***
CRM-2793 Push Notification Blast - User group
    [Tags]    ST    AT    RT
    [Setup]    Run Keywords    Launch CRM application    User login using valid Admin account
    [Teardown]    Run Keywords    User Logout    Close Browser
    Given User navigate to Access > User Groups
    When User edit the UserGroup
    And Grant Marketing > Push Notifications Access
    Then Push Notifications Management is available    

CRM-2794 Push Notification Blast Listing
    [Tags]    ST    AT    RT
    [Setup]    Valid Login
    [Teardown]    Run Keywords    User Logout    Close Browser
    When User navigate to Marketing > Push Notifications
    Then Push Notification Blast Listing is displayed
     
CRM-2795 Create New Push Notification Blast
    [Tags]    ST    AT    RT
    [Setup]    Valid Login
    [Teardown]    Run Keywords    User Logout    Close Browser
    Given User navigate to Marketing > Push Notifications
    When User click Create New Push Notification Blast icon
    And Populate Push Notification Blast Details    1
    And Approver approve PNS Blast
    And User start the PNS Blast
    Then PNS Blast status should be Completed
    
    