*** Settings ***
Resource    ../../Resources/Keywords/Global/GlobalSettings.robot
Suite Setup    Valid Login

*** Test Cases ***
CRM-2728 Email Blast_Perform Email Blast
    [Tags]    AT    RT
    [Teardown]    Close Browser
    Given User navigate to Marketing > Email Blast
    When User click Create New Email Blast icon
    And Populate Email Blast Details    1
    And Approver approve Email Blast
    And User start the Email Blast
    Then Recipients should receive the email
    And Email Blast status should be Completed
    
#TEST
#    @{recipients}    Split String    test1;test2;test3    ;
#   ${count}    Get Length    ${recipients}
#    :FOR    ${i}    IN RANGE    ${count}
#    \    ${value}    Set Variable    TEST    
#    Log To Console    ${value}               
     
    
    
     