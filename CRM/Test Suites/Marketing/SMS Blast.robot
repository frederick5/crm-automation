*** Settings ***
Resource    ../../Resources/Keywords/Global/GlobalSettings.robot


Suite Setup    Valid Login

*** Test Cases ***
CRM-2729 SMS Blast_Perform SMS Blast
    [Tags]    ST    AT    RT
    [Teardown]    Close Browser
    Given User navigate to Marketing > SMS Blast
    When User click Create New SMS Blast icon
    And Populate SMS Blast Details    1
    And Approver approve SMS Blast
    And User start the SMS Blast
    Then SMS Blast status should be Completed
    #And Recipient receive the SMS  
    

    

    
    
    