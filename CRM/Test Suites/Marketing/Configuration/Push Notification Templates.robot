*** Settings ***
Resource    ../../../Resources/Keywords/Global/GlobalSettings.robot
Suite Setup    Valid Login
*** Test Cases ***
CRM-2554:Push Noti Template-Search filter verification
    [Tags]    ST    AT    RT
    [Teardown]    Close Browser
    Given User Navigates To Marketing > Configurations > Push Notification Templates
    When User click Filter icon
    And Filter Push Noti Template By Name    1
    Then User able to search for Push Noti Templates using the search filters available

CRM-2564:Push Noti Template-Add new receipt template verification
    [Tags]    ST    AT    RT
    [Teardown]    Close Browser
    Given User Navigates To Marketing > Configurations > Push Notification Templates
    When User click Create New Template icon
    And Populate Push Notification Template    2
    Then Verify that Push Notification Template is available in Rewards Campaign    2
    
CRM-2563:Push Noti Template-Delete confirmation window verification
    [Tags]    ST    AT    RT
    [Teardown]    Close Browser
    Given User Navigates To Marketing > Configurations > Push Notification Templates
    When User validate close in delete window and cancel in delete window    3
    Then Push Notification Template should be deleted in table    3    
    
CRM-2567:Push Noti Template-Edit icon verification
    [Tags]    ST    AT    RT
    [Teardown]    Close Browser
    Given User Navigates To Marketing > Configurations > Push Notification Templates
    When User edit information in Push Notification Template    4
    Then The Push Notification Template Listing status should be The push notification template has been successfully updated.

