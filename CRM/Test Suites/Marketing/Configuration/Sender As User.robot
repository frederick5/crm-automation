*** Settings ***
Resource    ../../../Resources/Keywords/Global/GlobalSettings.robot
Suite Setup    Launch CRM Application


*** Test Cases ***

CRM-1689:User-Display EDM Creator Username- login name Verification
    [Tags]    ST    AT    RT   
    [Teardown]    Close Browser
    Given Login as Admin    
    And User Navigates To Marketing > Configuration > Sender
    And User click Create New Sender icon
    And Display EDM Creator Username is On
    And User Navigates To Marketing > Communications > Email Blast
    And User click Create New Email Blast icon
    And Create recurrence email blast
    Then Validate Login in Received the Email
    
CRM-1691:User-Display EDM Creator Username- sender name Verification
    [Tags]    ST    AT    RT   
    [Teardown]    Close Browser
    Given Login as Admin
    When User Navigates To Marketing > Configuration > Sender
    And User click Create New Sender icon
    And Display EDM Creator Username is Off
    And User Navigates To Marketing > Communications > Email Blast
    And User click Create New Email Blast icon
    And Create recurrence email blast
    Then Validate Sender in Received the Email
    