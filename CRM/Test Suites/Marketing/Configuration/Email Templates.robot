*** Settings ***
Resource    ../../../Resources/Keywords/Global/GlobalSettings.robot
Suite Setup    Valid Login
*** Test Cases ***

CRM-1693:Template-Search filters verification
    [Tags]    ST    AT    RT
    [Teardown]    Close Browser
    Given User Navigates To Marketing > Configurations > Email Templates
    When User click Filter icon
    And Filter Email Template By Name
    Then User able to search for Email Template using the search filters available
    
    When User click Filter icon
    And Filter Email Template By Sender Name
    Then User able to search for Email Template using the search filters available
    
    When User click Filter icon
    And Filter Email Template By Sender Email
    Then User able to search for Email Template using the search filters available

CRM-1714:Add new Template verification
    [Tags]    ST    AT    RT
    [Teardown]    Close Browser
    Given User Navigates To Marketing > Configurations > Email Templates
    When User click Create New Template icon
    And Populate Template Information    1
    Then New Email Template is added on the table    1
    
CRM-1717:Template-Delete confirmation window verification
    [Tags]    ST    AT    RT
    [Teardown]    Close Browser
    Given User Navigates To Marketing > Configurations > Email Templates
    When User click Filter icon
    And Filter Email Template By Name
    And User click Delete icon on Automation Sample on Template Listing 
    Then User close the window
    
    When User click Delete icon on Automation Sample on Template Listing
    Then User cancel delete
    And User should be able to delete Automation Sample
    
CRM-1718:Template-Edit icon verification
    [Tags]    ST    AT    RT
    [Teardown]    Close Browser
    Given User Navigates To Marketing > Configurations > Email Templates
    When User click Filter icon
    And Filter Email Template By Name
    And User click Edit icon on Automation Sample on Template Listing 
    Then Email Template should be editable    2
    