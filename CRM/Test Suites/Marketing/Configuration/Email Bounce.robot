*** Settings ***
Resource    ../../../Resources/Keywords/Global/GlobalSettings.robot
Suite Setup    Valid Login
*** Test Cases ***

CRM-1837:Email Bounce-Search filter verification
    [Tags]    ST    AT    RT
    [Teardown]    Close Browser
    Given User Navigates To Marketing > Configurations > Email Bounce Management
    When User click Filter icon
    And Filter Email Bounce By Hostname
    Then User able to search for Email Bounce using the search filters available
    
    When User click Filter icon
    And Filter Email Bounce By LoginID
    Then User able to search for Email Bounce using the search filters available
    
CRM-1844:Email Bounce-Create new account verification
    [Tags]    ST    AT    RT
    [Teardown]    Close Browser
    Given User Navigates To Marketing > Configurations > Email Bounce Management
    When User click Create New Account icon
    Then Populate Email Bounce Account Management    1
    
    When User Navigates To Marketing > Communications > Email Blast
    And User click Create New Email Blast icon
    And Create Email Blast with invalid recipients
    Then Validate Email Blast Reports
    
CRM-1852:Email Bounce-Edit icon verification
    [Tags]    ST    AT    RT
    [Teardown]    Close Browser
    Given User Navigates To Marketing > Configurations > Email Bounce Management
    When User click Edit Field icon on Sample@test.com on Email Bounce Management
    Then User can edit information
    
CRM-1855:Email Bounce-Delete confirmation window verification
    [Tags]    ST    AT    RT
    [Teardown]    Close Browser
    Given User Navigates To Marketing > Configurations > Email Bounce Management
    When User click Delete Item icon on Sample@test.com on Email Bounce Management
    Then User can close and cancel Delete window in Email Bounce
    
    When User Select Login ID Sample@test.com in table
    Then Email Bounce should be deleted
    And The Email Bounce Management status should be The Account has been successfully deleted