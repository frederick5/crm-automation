*** Settings ***
Resource    ../../../Resources/Keywords/Global/GlobalSettings.robot
Suite Setup    Valid Login
*** Test Cases ***

CRM-1674 Sender-Search filters verification
    [Tags]    ST    AT    RT
    [Teardown]    Close Browser
    Given User Navigates To Marketing > Configurations > Sender
    When User click Filter icon
    And Filter By Name
    Then User able to search for a Sender using the search filters available
    
    When User click Filter icon
    And Filter By Email
    Then User able to search for a Sender using the search filters available
    
    When User click Filter icon
    And Filter By SMS Masked Name
    Then User able to search for a Sender using the search filters available
    
    When User click Filter icon
    And Filter By Mailing List Name
    Then User able to search for a Sender using the search filters available
    

CRM-1681:Sender-Create new sender verification
    [Tags]    ST    AT    RT
    [Teardown]    Close Browser
    Given User Navigates To Marketing > Configurations > Sender 
    When User click Create New Sender icon
    And Populate Required Fields    1
    Then The Sender Listing status should be The sender has been successfully added
    And Check Sender Name on table    1
    
CRM-1686:Sender-Delete confirmation window verification
    [Tags]    ST    AT    RT
    [Teardown]    Close Browser
    Given User Navigates To Marketing > Configurations > Sender
    When User search for sender to delete
    Then User can close and cancel Delete window   
    When User select an item    2
    And User click Delete Checked icon
    Then Delete confirmation window prompted    2
    And The Sender Listing status should be The sender has been successfully deleted
       
CRM-1687:Sender-Edit icon verification
    [Tags]    ST    AT    RT
    [Teardown]    Close Browser
    Given User Navigates To Marketing > Configurations > Sender
    When User search for sender to edit
    Then The Sender Management is Displayed
    
    When Update Sender Redirect Url field
    Then The Sender Listing status should be The sender has been successfully saved
    
CRM-1688:Sender-Edit verification
    [Tags]    ST    AT    RT
    [Teardown]    Close Browser
    Given User Navigates To Marketing > Configurations > Sender
    When User search for sender to edit
    Then Information can be editted
    
    When Update Sender SMS Masked Name field
    Then The Sender Listing status should be The sender has been successfully saved
    
CRM-1690:Sysadmin-Display EDM Creator Username- login name Verification
    [Tags]    ST    AT    RT
    [Teardown]    Close Browser
    Given User Navigates To Marketing > Configurations > Sender
    When User click Create New Sender icon
    And Display EDM Creator Username is On
    And User Navigates To Marketing > Communications > Email Blast
    And User click Create New Email Blast icon
    And Create recurrence email blast
    Then Validate Login in Received the Email
    
CRM-1692:Sysadmin-Display EDM Creator Username- sender name Verification
    [Tags]    ST    AT    RT   
    [Teardown]    Close Browser
    Given User Navigates To Marketing > Configurations > Sender
    When User click Create New Sender icon
    And Display EDM Creator Username is Off
    And User Navigates To Marketing > Communications > Email Blast
    And User click Create New Email Blast icon
    And Create recurrence email blast
    Then Validate Sender in Received the Email
    


    