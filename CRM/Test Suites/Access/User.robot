*** Settings ***
Resource    ../../Resources/Keywords/Global/GlobalSettings.robot

Suite Setup    Valid Login

*** Test Cases ***
CRM-2250 : User - Add New User Verification
   [Tags]    AT    RT
   [Teardown]    Close Browser
   Given User navigate to Access > Users 
   When User add new user
   And User fill up the fields
   And User click Save icon
   Then A New User Should be Inserted    ${loc_txtAddNewUserSuccessMsg}    ${var_AddNewUserSuccess}
   
CRM-2251 : User - Edit Icon Verification
   [Tags]    AT    RT
   [Teardown]    Close Browser
   Given User navigate to Access > Users 
   When User edit user
   And Search Filter by Email
   Then Input value for Email 
   And Select a user to edit
   And User update the fields
   And User click Reset icon
   And User update the fields
   And User click Save icon
   Then Edited User Should be Updated    ${loc_txtEditUserSuccess}    ${var_EditUserSuccess}
   
CRM-2242 : User-Search filter verification
   [Tags]    AT    RT
   [Teardown]    Close Browser
   Given User navigate to Access > Users 
   When User search filter of user
   And Search Filter by User ID
   Then Input value for User ID
   And Search Filter by Email
   Then Input value for Email 
   And Search Filter by User Group
   Then Input value for User Group 
   

CRM-2249 : User-Delete confirmation window verification
   [Tags]    AT    RT
   [Teardown]    Close Browser
   Given User navigate to Access > Users 
   When User clicks Delete Check without selecting from the list
   And User delete user
   And Search Filter by Email
   Then Input value for Email
   And User proceed on deleting
   Then User Should be Deleted    ${loc_txtDeleteUserSuccess}    ${var_DeleteUserSuccess}