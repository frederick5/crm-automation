*** Settings ***
Resource    ../../Resources/Keywords/Global/GlobalSettings.robot

Suite Setup    Valid Login

*** Test Cases ***

CRM-2197 : User Group-Add new user group verification
   [Tags]    AT    RT
   [Teardown]    Close Browser
   Given User navigate to Access > User Groups 
   When User add new user group
   And User complete the fields
   And User click Save icon
   Then The New Group is Added    ${loc_txtAssignUserSuccessMsg}    ${var_AddGroupSuccess} 

CRM-2187 : User Group-Search filter verification
   [Tags]    AT    RT
   [Teardown]    Close Browser
   Given User navigate to Access > User Groups
   When User search filter by user group
   And Search Filter by User Group
   Then Input value by User Group
   And Search Filter by Description
   Then Input value by Description
   

CRM-2198 : User Group-Edit icon verification
   [Tags]    AT    RT
   [Teardown]    Close Browser
   Given User navigate to Access > User Groups
   When User edit user group
   And Search Filter by User Group
   Then Input value by User Group
   And Select user group to edit
   And User edit fields
   And User click Reset icon
   And User edit fields
   And User click Save icon
   Then Edited UserGroup Should be Updated    ${loc_txtAssignUserSuccessMsg}    ${var_AssignUserSuccess} 

CRM-2200 : User Group-Assign User to Group verification
   [Tags]    AT    RT
   [Teardown]    Close Browser
   Given User navigate to Access > User Groups
   When User assign to user group
   And Search Filter by User Group
   Then Input value by User Group
   And Select user group to edit
   And User add user to the group
   And User click Save icon
   Then The User is assigned to a Group    ${loc_txtAssignUserSuccessMsg}    ${var_AssignUserSuccess}    
   
CRM-2195 : User Group-Duplicate User Group verification
   [Tags]    AT    RT
   [Teardown]    Close Browser
   Given User navigate to Access > User Groups
   When User duplicate user group
   And Search Filter by User Group
   Then Input value by User Group
   And Check User Group
   And User select to duplicate
   And User proceed on duplicating the Group
   And User click Save icon
   Then The Duplicated Group is Added    ${loc_txtDuplicateGroupSuccessMsg}    ${var_DuplicateGroupSuccess}  
   
CRM-0000 : User Group Fields Customizing
   [Tags]    AT    RT
   [Teardown]    Close Browser
   Given User navigate to Access > User Groups
   When User customize fields of user group
   And Search Filter by User Group
   Then Input value by User Group
   And Check User Group
   And User proceed on duplicate field customizing
   And User click Submit icon
   Then The Duplicated Field is Added    ${loc_txtDuplicateFieldSuccessMsg}    ${var_DupFieldCustomSuccess}

CRM-2214 : User Group-Edit Field Customizing verification
   [Tags]    AT    RT
   [Teardown]    Close Browser
   Given User navigate to Access > User Groups
   When User edit customize fields of user group
   And Search Filter by User Group
   Then Input value by User Group
   And Check User Group
   And User clicks on edit field customizing
   And User uncheck all
   And User click Reset icon
   And User click Submit icon
   Then The User Group has been updated    ${loc_txtDuplicateGroupSuccessMsg}    ${var_UpdateGroupSuccess}  

CRM-2196 : User Group-Delete confirmation window verification
   [Tags]    AT    RT
   #[Teardown]    Close Browser
   Given User navigate to Access > User Groups
   When User delete user group
   And User clicks Delete Check User Group Listing
   And Search Filter by User Group
   Then Input value by User Group
   And Check User Group
   And User selects group to delete
   #Then User Group Should be Deleted    ${loc_txtDeleteUserGroupSuccess}    ${var_DeleteGroupSuccess}
   
CRM-2231 : User Group- Field Customization v2 Add new fields customizing verification
   [Tags]    AT    RT
   [Teardown]    Close Browser
   Given User navigate to Access > User Groups 
   When User add new field customizing user group
   And User proceed on field customizing
   And User click Submit icon
   Then The New Field Custom is Added    ${loc_txtUpdateFieldCustomSuccessMsg}    ${var_NewFieldCustomSuccess}
