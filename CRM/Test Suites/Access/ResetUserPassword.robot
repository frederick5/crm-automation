*** Settings ***
Resource    ../../Resources/Keywords/Global/GlobalSettings.robot

*** Test Cases ***
CRM-2256 : Reset User Password-New & confirm password verification
    [Tags]    AT    RT
    [Teardown]    Close Browser
    Given Launch CRM application
    When User sign in using valid account
    And User navigate to Access > Reset User Password
    And User reset password
    And User click Save icon
    Then The user new password has been reset    ${loc_txtResetPasswordSuccessMsg}    ${var_ResetPasswordSuccess} 
    
CRM-2259 : User Reset Password-User Login verification
    [Tags]    AT    RT
    [Teardown]    Close Browser
    Given Launch CRM application
    When User sign in using new password
    Then User should be able to login using new password
    
CRM-2263 : User Reset Password-Send Reset password link verification
    [Tags]    AT    RT
    [Setup]    Valid Login
    [Teardown]    Close Browser
    Given User navigate to Membership > Membership Search 
    When User search for the member
    And User click proceed to send reset password link
    And User Receive Reset Password Link Confirmation
    