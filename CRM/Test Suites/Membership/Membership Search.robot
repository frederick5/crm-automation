*** Settings ***
Resource    ../../Resources/Keywords/Global/GlobalSettings.robot

*** Test Cases ***
CRM-2724 : Member Search
    [Tags]    ST    AT    RT
    [Setup]    Valid Login
    [Teardown]    Close Browser
    Given User navigate to Membership > Membership Search 
    When User clicks Member Search
    Then Member Search Listing page is displayed
   
CRM-2725 : Card Search
    [Tags]    ST    AT    RT
    [Setup]    Valid Login
    [Teardown]    Close Browser
    Given User navigate to Membership > Membership Search 
    When User clicks Card Search
    Then Member Search Listing page is displayed
   
CRM-2726 : Update Profile
    [Tags]    ST    AT    RT
    [Setup]    Valid Login
    [Teardown]    Close Browser
    Given User accesses Member Profile
    When User updates Mobile No
    Then Update success message is displayed    ${loc_textProfileUpdateSuccessMessage}    ${var_msgUpdateProfileSuccess}    
     
CRM-2727 : Update Card Info
    [Tags]    ST    AT    RT
    [Setup]    Valid Login
    [Teardown]    Close Browser
    Given User accesses Member Cards
    When User updates Card Info
    Then Update success message is displayed    ${loc_textCardUpdateSuccessMessage}    ${var_msgUpdateCardSuccess}