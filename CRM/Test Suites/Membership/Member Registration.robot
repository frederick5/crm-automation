*** Settings ***
Resource    ../../Resources/Keywords/Global/GlobalSettings.robot
Suite Setup    Valid Login

*** Test Cases ***
CRM-2730 : Register member
   [Tags]    ST    AT    RT
   [Teardown]    Close Browser
   Given User navigate to Membership > Member Registration 
   When User register new member using valid info
   Then Success message is displayed    ${loc_NewUserSuccessMessage}    ${var_NewMemberAddedSuccess}
   #And New member is added to Member Listing