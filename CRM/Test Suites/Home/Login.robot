*** Settings ***
Resource    ../../Resources/Keywords/Global/GlobalSettings.robot
Suite Setup    Launch CRM Application
Suite Teardown    Close Browser

*** Test Cases ***
CRM-2568 Login-correct password verification-User
    [Tags]    ST    AT    RT
    [Teardown]     User Logout   
    When User login using valid User account
    Then User should be able to login to CRM
    
CRM-2569 Login-correct password verification-Admin
    [Tags]    ST    AT    RT
    [Teardown]     User Logout
    When User login using valid Admin account
    Then User should be able to login to CRM
        
CRM-2570 Login-incorrect password verification-User
    [Tags]    ST    AT    RT
    When User login using invalid User account
    Then User should not be able to login to CRM
    And Error message is displayed    ${var_msgInvalidAccount}        

CRM-2571 Login-incorrect password verification-Admin
    [Tags]    ST    AT    RT
    When User login using invalid Admin account
    Then User should not be able to login to CRM
    And Error message is displayed    ${var_msgInvalidAccount}
    
CRM-2574 Login-lockout Duration-User
    [Tags]    AT    RT
    When User login using incorrect password 3 times
    Then Error message is displayed    ${var_msgUserLockout}
    When User login using valid User account
    Then User should not be able to login to CRM

CRM-2575 Login-mandatory username & password verification
    [Tags]    ST    AT    RT
    When User login using blank User account
    Then User should not be able to login to CRM
    And Login fields will be highlighted in red
    When User login using with space username
    Then Error message is displayed    ${var_msgLoginSpaces}        
    
CRM-2577 Login-forgot password link verification
    [Tags]    AT    RT
    When User click Forgot Password link
    And Submit valid User ID
    And Click Reset Password link from Email
    And Submit valid new password
    Then Reset Password success message is displayed
    And User can login with new password
    


    