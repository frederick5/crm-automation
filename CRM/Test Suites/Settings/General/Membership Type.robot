*** Settings ***
Resource    ../../../Resources/Keywords/Global/GlobalSettings.robot
Suite Setup    Valid Login
*** Test Cases ***

CRM-2341:Membership Type-Search filter verification
    [Tags]    ST    AT    RT
    [Teardown]    Close Browser
    Given User Navigates To Settings > General > Membership Type
    When User click Filter icon
    And Filter Membership Type By Code
    Then User able to search Membership Type using the search filters available
    
    When User click Filter icon
    And Filter Membership Type By Name
    Then User able to search Membership Type using the search filters available
     
CRM-3069:Membership Type-Add new membership type verification
    [Tags]    ST    AT    RT
    [Teardown]    Close Browser
    Given User Navigates To Settings > General > Membership Type
    When User click Add New Membership Type icon
    And Populate Membership Type Fields    1
    Then Verify Membership Type Listing status should be The new membership type has been successfully saved.	
    
CRM-2348:Membership Type-Edit icon verification
    [Tags]    ST    AT    RT
    [Teardown]    Close Browser
    Given User Navigates To Settings > General > Membership Type
    When User select membership type code and edit    Automation
    Then Verify Membership Type Listing status should be The membership type has been successfully updated.
    
CRM-2349:Membership Type-Delete confirmation window verification
    [Tags]    ST    AT    RT
    [Teardown]    Close Browser
    Given User Navigates To Settings > General > Membership Type
    When User click Delete Checked icon
    Then Verify Alert message if no checked record
    
    When User select membership type code and delete    Automation
    Then Verify Membership Type Listing status should be The selected item has been successfully deleted.