*** Settings ***
Resource    ../../../Resources/Keywords/Global/GlobalSettings.robot
Suite Setup    Valid Login
*** Test Cases ***
CRM-1561:Add new system code verification
    [Tags]    ST    AT    RT
    [Teardown]    Close Browser
    Given User Navigates To Settings > General > System Code
    When User click Add New Code icon
    And Populate System Code information    1
    Then Verify System Code Listing status should be The new system code has been successfully saved.

CRM-1542:System Code-Parent system code search filter verification
    [Tags]    ST    AT    RT
    [Teardown]    Close Browser
    Given User Navigates To Settings > General > System Code
    When User click Filter icon
    And Filter Parent System Code By System Code
    Then User able to search for a System Code using the search filters available
    
    When User click Filter icon
    And Filter Parent System Code By System Code Name
    Then User able to search for a System Code using the search filters available
    
    When User click Filter icon
    And Filter Parent System Code By Description
    Then User able to search for a System Code using the search filters available
    
    When User click Filter icon
    And Filter Parent System Code By Status
    Then User able to search for a System Code using the search filters available
    
CRM-1547:System Code-Edit System Code Popup window verification
    [Tags]    ST    AT    RT
    [Teardown]    Close Browser
    Given User Navigates To Settings > General > System Code
    When User search for system code    Automation1 
    And User update system code description of    Automation1
    Then Verify System Code Listing status should be The system code has been successfully updated.
    
CRM-1563:Delete confirmation window verification
    [Tags]    ST    AT    RT
    [Teardown]    Close Browser
    Given User Navigates To Settings > General > System Code
    When User search for system code    Automation1 
    And User click Delete Checked icon
    Then Verify Alert message if no checked record
    
    When User select system code to delete    Automation1
    Then User can perform Delete on System Code
    And Verify System Code Listing status should be The system code has been successfully deleted.
    
CRM-1543:System Code-Child system code search filter verification
    [Tags]    ST    AT    RT
    [Teardown]    Close Browser
    Given User Navigates To Settings > General > System Code
    When User Select Child Item of system code    Automation123
    And User click Filter icon
    And Filter Child System Code By System Code
    Then User able to search to Child System Code using the search filters available
    
    When User click Filter icon
    And Filter Child System Code By System Code Name
    Then User able to search to Child System Code using the search filters available
    
    When User click Filter icon
    And Filter Child System Code By Description
    Then User able to search to Child System Code using the search filters available
    
    When User click Filter icon
    And Filter Child System Code By Status
    Then User able to search to Child System Code using the search filters available 

CRM-1557:Add new system child code popup window verification
    [Tags]    ST    AT    RT
    [Teardown]    Close Browser
    Given User Navigates To Settings > General > System Code
    When User Select Child Item of system code    Automation123
    And User click Add New Code icon
    And Populate existing child code    sample123
    Then Validate that existing data will populate then save    2
    And Validate that existing child system code name will be update    Automation Test