*** Settings ***
Resource    ../../../Resources/Keywords/Global/GlobalSettings.robot
Suite Setup    Valid Login
*** Test Cases ***
 
CRM-1573:Custom Code-Search Filters Parent code verification
    [Tags]    ST    AT    RT
    [Teardown]    Close Browser
    Given User Navigates To Settings > General > Custom Code
    When User click Filter icon
    And Filter Parent Custom Code By Custom Code
    Then User able to search for custom code using the search filters available
       
    When User click Filter icon
    And Filter Parent Custom Code By Custom Code Name
    Then User able to search for custom code using the search filters available
     
    When User click Filter icon
    And Filter Parent Custom Code By Description
    Then User able to search for custom code using the search filters available
     
    When User click Filter icon
    And Filter Parent Custom Code By Status
    Then User able to search for custom code using the search filters available
    
CRM-1574:Custom Code-Search Filters Child item verification
    [Tags]    ST    AT    RT
    [Teardown]    Close Browser
    Given User Navigates To Settings > General > Custom Code
    When User Select Child Item of custom code    Automation1
    And User click Filter icon
    And Filter Child Custom Code By Custom Code
    Then User able to search to Child Custom Code using the search filters available
    
    When User click Filter icon
    And Filter Child Custom Code By Custom Code Name
    Then User able to search to Child Custom Code using the search filters available
    
    When User click Filter icon
    And Filter Child Custom Code By Description
    Then User able to search to Child Custom Code using the search filters available
    
    When User click Filter icon
    And Filter Child Custom Code By Status
    Then User able to search to Child Custom Code using the search filters available
    
CRM-1594:Add new custom code verification
    [Tags]    ST    AT    RT
    [Teardown]    Close Browser
    Given User Navigates To Settings > General > Custom Code
    When User click Add New Code icon
    And Populate Custom Code information    1
    Then Verify Custom Code Listing status should be The new custom code has been successfully saved.
    
CRM-1579:Custom Code-Edit Custom Code Popup window verification
    [Tags]    ST    AT    RT
    [Teardown]    Close Browser
    Given User Navigates To Settings > General > Custom Code
    When User search for custom code    Automation1
    And User update custom code description of    Automation1
    Then Verify Custom Code Listing status should be The custom code has been successfully updated.