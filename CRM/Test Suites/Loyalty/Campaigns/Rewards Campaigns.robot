*** Settings ***
Resource    ../../../Resources/Keywords/Global/GlobalSettings.robot
Suite Setup    Login for Loyalty
*** Test Cases ***

CRM-1857:Reward Campaign-User Group accessright
    [Tags]    ST    AT    RT   
    [Teardown]    Close Browser
    Given User Navigate to Access > User Groups
    When User Search for User Group and Edit ROBOTAUTOMATION
    And User UnCheck Rewards Campaign Access Right
    Then Verify Rewards Campaigns is Hidden
    
    When User Navigate to Access > User Groups
    And User Search for User Group and Edit ROBOTAUTOMATION
    And User Check Rewards Campaign Access Right
    Then Verify Rewards Campaigns is Shown
    
CRM-1936:Reward Campaign-User Group Add new button accessright
    [Tags]    ST    AT    RT   
    [Teardown]    Close Browser
    Given User Navigate to Access > User Groups
    When User Search for User Group and Edit ROBOTAUTOMATION
    And User UnCheck Rewards Campaign Field Add Access Right
    Then Verify Rewards Campaigns field Add New is Hidden
    
    When User Navigate to Access > User Groups
    And User Search for User Group and Edit ROBOTAUTOMATION
    And User Check Rewards Campaign Field Add Access Right
    Then Verify Rewards Campaigns field Add New is Shown
    
CRM-1937:Reward Campaign-User Group Delete icon and Delete Checked button accessright
    [Tags]    ST    AT    RT   
    [Teardown]    Close Browser
    Given User Navigate to Access > User Groups
    When User Search for User Group and Edit ROBOTAUTOMATION
    And User UnCheck Rewards Campaign Field Delete Access Right
    Then Verify Rewards Campaigns field Delete Item is Hidden
    And Verify Rewards Campaigns field Delete Checked is Hidden
    
    When User Navigate to Access > User Groups
    And User Search for User Group and Edit ROBOTAUTOMATION
    And User Check Rewards Campaign Field Delete Access Right
    Then Verify Rewards Campaigns field Delete Item is Shown
    And Verify Rewards Campaigns field Delete Checked is Shown
    
CRM-1938:Reward Campaign-User Group Edit icon accessright
    [Tags]    ST    AT    RT   
    [Teardown]    Close Browser
    Given User Navigate to Access > User Groups
    When User Search for User Group and Edit ROBOTAUTOMATION
    And User UnCheck Rewards Campaign Field Edit Access Right
    Then Verify Rewards Campaigns field Edit Item is Hidden
    
    When User Navigate to Access > User Groups
    And User Search for User Group and Edit ROBOTAUTOMATION
    And User Check Rewards Campaign Field Edit Access Right
    Then Verify Rewards Campaigns field Edit Item is Shown
    
CRM-1939:Reward Campaign-User Group Add new with designer accessright
    [Tags]    ST    AT    RT   
    [Teardown]    Close Browser
    Given User Navigate to Access > User Groups
    When User Search for User Group and Edit ROBOTAUTOMATION
    And User UnCheck Rewards Campaign Field Designer Access Right
    Then Verify Rewards Campaigns field Add New With Designer is Hidden
    
    When User Navigate to Access > User Groups
    And User Search for User Group and Edit ROBOTAUTOMATION
    And User Check Rewards Campaign Field Designer Access Right
    Then Verify Rewards Campaigns field Add New With Designer is Shown
    
CRM-1940:Reward Campaign-User Group Priority Adjustment accessright
    [Tags]    ST    AT    RT   
    [Teardown]    Close Browser
    Given User Navigate to Access > User Groups
    When User Search for User Group and Edit ROBOTAUTOMATION
    And User UnCheck Rewards Campaign Field Priority Adjustment Access Right
    Then Verify Rewards Campaigns column Priority Adjustment is Hidden
    
    Given User Navigate to Access > User Groups
    When User Search for User Group and Edit ROBOTAUTOMATION
    And User Check Rewards Campaign Field Priority Adjustment Access Right
    Then Verify Rewards Campaigns column Priority Adjustment is Shown
    
CRM-1941:Reward Campaign-Add New Reward Campaign
    [Tags]    ST    AT    RT   
    [Teardown]    Close Browser
    Given User Navigates To Loyalty > Campaigns > Rewards Campaigns
    When User click Add New icon
    And Populate Reward Campaign Management    1
    Then The Reward Campaign Management status should be A new campaign has been successfully inserted.
    
CRM-1946:Campaign Designer-Add New Campaign Designer
    [Tags]    ST    AT    RT   
    [Teardown]    Close Browser
    Given User Navigates To Loyalty > Campaigns > Rewards Campaigns
    When User click Add New With Designer icon
    Then Verify alert message when Campaign Settings is not populated upon clicking Qualifiers - Rewards - Notification tab
    
    When Populate Campaign Settings tab    2
    And Populate Qualifiers - Rewards - Notifications tab
    Then Verify alert message when successfully saved
    And Verify that campaign designer is present in Group Name dropdown
    
CRM-1942:Reward Campaign-Edit Icon verification
    [Tags]    ST    AT    RT   
    [Teardown]    Close Browser
    Given User Navigates To Loyalty > Campaigns > Rewards Campaigns
    When User try to update Reward Campaign    Automation321
    Then The Reward Campaign Listing status should be The campaign has been successfully updated.
    
CRM-1947:Campaign Designer-Edit Campaign Designer
    [Tags]    ST    AT    RT   
    [Teardown]    Close Browser
    Given User Navigates To Loyalty > Campaigns > Rewards Campaigns
    When User try to update Campaign Designer    Auto321
    Then Verify alert message when successfully saved
    
CRM-1943:Reward Campaign-Delete icon verification
    [Tags]    ST    AT    RT   
    [Teardown]    Close Browser
    Given User Navigates To Loyalty > Campaigns > Rewards Campaigns
    When User try to use Delete icon to delete Reward Campaign    Automation321
    Then Verify if delete confirmation popup window is prompted
    
CRM-1944:Reward Campaign-Delete confirmation window verification
    [Tags]    ST    AT    RT   
    [Teardown]    Close Browser
    Given User Navigates To Loyalty > Campaigns > Rewards Campaigns
    When User try to use Delete Checked icon to delete Reward Campaign    Automation321
    Then Verify Alert message if no checked record
    
    When User check a record and use Delete Checked icon to delete    Automation321
    Then User can close and cancel Delete window in Reward Campaign Listing
    
    When User click Delete Checked icon
    Then User can successfully delete and item
    
CRM-1948:Campaign Designer-Delete Campaign Designer
    [Tags]    ST    AT    RT   
    [Teardown]    Close Browser
    Given User Navigates To Loyalty > Campaigns > Rewards Campaigns
    When User search for Reward Campaign    Sample1
    And User check a record and use Delete Checked icon to delete    Sample1
    Then User can successfully delete and item
    
CRM-1945:Reward Campaign-Duplicate Reward Campaign
    [Tags]    ST    AT    RT   
    [Teardown]    Close Browser
    Given User Navigates To Loyalty > Campaigns > Rewards Campaigns
    When User search for Reward Campaign    1120
    And User duplicate existing campaign    1120    3
    Then The Reward Campaign Listing status should be A new campaign has been successfully inserted.
    
CRM-1949:Campaign Designer-Duplicate Reward Campaign
    [Tags]    ST    AT    RT   
    [Teardown]    Close Browser
    Given User Navigates To Loyalty > Campaigns > Rewards Campaigns
    When User search for Reward Campaign    113
    And User duplicate existing campaign    113    4
    Then The Reward Campaign Listing status should be A new campaign has been successfully inserted.