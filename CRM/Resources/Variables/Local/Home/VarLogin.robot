*** Variables ***
${var_dataLogin}    Test Data//Data Files//Home.xls
${var_msgInvalidAccount}    Sorry, you have entered an invalid Username or Password. Please click "Forgot Your Password" below.
${var_msgWithSpaceAccount}    Spaces are not allowed!
${var_msgResetPasswordSuccess}    Your password has been reset successfully!
${var_emailResetPasswordSender}    noreply@qa4.com
${var_msgUserLockout}    Your account has been locked, please contact Administrator.
${var_msgLoginSpaces}    Spaces are not allowed!
${var_usernameWithSpace}    qatest @email.com