*** Variables ***
${var_dataUserGroup}    Test Data${/}UsersGroup.xls
${var_AssignUserSuccess}    The user group has been successfully updated.
${var_AddGroupSuccess}    The new user group has been successfully inserted.
${var_DuplicateGroupSuccess}    The new user group has been successfully duplicated.
${var_DupFieldCustomSuccess}    The field customizing has been successfully duplicated.
${var_DeleteGroupSuccess}    The user groups have been successfully deleted.
${var_UpdateGroupSuccess}    The user group has been successfully updated.
${var_NewFieldCustomSuccess}    The new fields customizing has been successfully inserted.