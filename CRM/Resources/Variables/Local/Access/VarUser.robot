*** Variables ***
${var_dataUsers}    Test Data${/}UsersGroup.xls
${var_AddNewUserSuccess}    The new user has been successfully inserted.
${var_EditUserSuccess}    The user has been successfully updated.
${var_DeleteUserSuccess}    The users have been successfully deleted.