*** Settings ***
Resource    ../../../Global/GlobalSettings.robot

*** Variables ***
${REWARDSCAMPAIGNS}    ${EMPTY}

*** Keywords ***
Set Loyalty Login Details
    [Arguments]    ${datacol}          
    &{REWARDSCAMPAIGNS}    Create Dictionary
    &{REWARDSCAMPAIGNS}    Get Data From Excel    ${var_dataLoyalty}    LOGIN    ${datacol} 
    Set Global Variable    &{REWARDSCAMPAIGNS}
    
Set Rewards Campaign Details
    [Arguments]    ${datacol}          
    &{REWARDSCAMPAIGNS}    Create Dictionary
    &{REWARDSCAMPAIGNS}    Get Data From Excel    ${var_dataLoyalty}    RewardsCampaigns    ${datacol} 
    Set Global Variable    &{REWARDSCAMPAIGNS}

#User Group Keywords
User Search for ${searchby} and Edit ${itemToSearch}
    Search Filter by ${searchby}
    Input ${itemToSearch} value by ${searchby}
    Select item ${itemToSearch} to edit
    
Input ${itemToSearch} value by ${filtervalue}
        Run Keyword If    '${filtervalue}' == 'User Group'    Input Text    ${loc_inputFilterValue}    ${itemToSearch}
        Run Keyword If    '${filtervalue}' == 'Description'    Input Text    ${loc_inputFilterValue}    ${itemToSearch}
        Click Element    ${loc_btnFilterSearch}
        
Select item ${itemToSearch} to edit
    Click Element    //td[4][contains(., '${itemToSearch}')]/..//td[2]//span[@class='ui-icon ui-icon-pencil']

User ${option} Rewards Campaign Field ${fieldSetValue} Access Right
    Wait Until Element Is Visible    ${loc_userGroupLoyalty}
    Click Element    ${loc_userGroupLoyalty}
    ${fieldSet}    Set Variable    //fieldset[@class='fst_Rewards_Campaigns']//label[contains(.,'${fieldSetValue}')]
    ${unchecked}    Run Keyword And Return Status    Checkbox Should Not Be Selected    ${fieldSet}/../input
    Run Keyword If    '${option}' == 'Check' and '${unchecked}'=='True'      Click Element    ${fieldSet}
    ...    ELSE IF    '${option}' == 'UnCheck' and '${unchecked}'=='False'    Click Element    ${fieldSet}
    User click Save icon
    Edited UserGroup Should be Updated    ${loc_txtAssignUserSuccessMsg}    ${var_AssignUserSuccess}
    
User ${option} Rewards Campaign Access Right
    Wait Until Element Is Visible    ${loc_userGroupLoyalty}
    Click Element    ${loc_userGroupLoyalty}
    ${unchecked}    Run Keyword And Return Status    Checkbox Should Not Be Selected    ${loc_userGroupRewardsCampaignChckBox}
    Run Keyword If    '${option}' == 'Check' and '${unchecked}'=='True'      Click Element    ${loc_userGroupRewardsCampaignChckBox}
    ...    ELSE IF    '${option}' == 'UnCheck' and '${unchecked}'=='False'    Click Element    ${loc_userGroupRewardsCampaignChckBox}
    User click Save icon
    Edited UserGroup Should be Updated    ${loc_txtAssignUserSuccessMsg}    ${var_AssignUserSuccess}
    
#Loyalty
Verify Rewards Campaigns is ${option}
    User Navigate To Loyalty > Campaigns
    Wait Until Element Is Visible     //*[@id="app"]/aside/div/nav/ul/li[4]/ul/li[1]/ul/li[1]/a/span
    ${rewardsCampaigns}    Get Text   //*[@id="app"]/aside/div/nav/ul/li[4]/ul/li[1]/ul/li[1]/a/span   
    #${statusRewardCampaign}=    Run Keyword And Return Status    Click Element    ${rewardsCampaigns}
    Run Keyword If    '${option}' == 'Hidden'    Should Be Equal    ${rewardsCampaigns}    Rewards Campaigns Approval   
    Run Keyword If    '${option}' == 'Shown'     Should Be Equal    ${rewardsCampaigns}    Rewards Campaigns
    
Verify Rewards Campaigns field ${iconName} is ${option}  
    User Navigates To Loyalty > Campaigns > Rewards Campaigns   
    ${status}=    Run Keyword And Return Status    Click Element    //i[@data-original-title='${iconName}']   
    Run Keyword If    '${iconName}' == 'Delete Item' and '${option}' == 'Shown'    User close the window
    Run Keyword If    '${option}' == 'Hidden'    Should Be Equal    '${status}'    'False'
    Run Keyword If    '${option}' == 'Shown'     Should Be Equal    '${status}'    'True'

Verify Rewards Campaigns column Priority Adjustment is ${option}
    User Navigates To Loyalty > Campaigns > Rewards Campaigns
    ${status} =    Run Keyword And Return Status    Click Element    //input[contains(@title,'Enter a priority no. to swap')]
    Run Keyword If    '${option}' == 'Hidden'    Should Be Equal    '${status}'    'False'
    Run Keyword If    '${option}' == 'Shown'     Should Be Equal    '${status}'    'True'
    
#Rewards Campaign
Populate Reward Campaign Management
    [Arguments]    ${datacol}
    Set Rewards Campaign Details    ${datacol}
    Wait Until Element Is Visible    ${loc_inputGeneralCode}
    #Code
    Input Text    ${loc_inputGeneralCode}    ${REWARDSCAMPAIGNS.GeneralCode}  
    #Name 
    Input Text    ${loc_inputGeneralName}    ${REWARDSCAMPAIGNS.GeneralName}   
    #Type
    Select From List By Label    ${loc_cboGeneralType}    ${REWARDSCAMPAIGNS.GeneralType}
    #Entity
    Select From List By Label    ${loc_cboGeneralEntity}    ${REWARDSCAMPAIGNS.GeneralEntity}  
    #Description 
    Input Text    ${loc_inputGeneralDescription}    ${REWARDSCAMPAIGNS.GeneralDescription}
    #Start Date
    Input Text    ${loc_inputAvailabilityStartDate}    ${REWARDSCAMPAIGNS.StartDate}
    #End Date
    Input Text    ${loc_inputAvailabilityEndDate}    ${REWARDSCAMPAIGNS.EndDate}
    #Save
    User click Save And Publish icon
    #Proceed
    Wait Until Element Is Visible    ${loc_btnCampaignReviewProceed}
    Click Element    ${loc_btnCampaignReviewProceed}
    
The Reward Campaign Management status should be ${status}
    
    Wait Until Element Is Visible    ${loc_rewardCampaignNotification}
    ${rewardCampaignStatus}    Get Text    ${loc_rewardCampaignNotification}
    Should Be Equal    ${status}    ${rewardCampaignStatus}  
    
Verify alert message when Campaign Settings is not populated upon clicking Qualifiers - Rewards - Notification tab
    Wait Until Element Is Visible    ${loc_qualifierRewardsNotiTab}
    Click Element    ${loc_qualifierRewardsNotiTab}
    Wait Until Element Is Visible    ${loc_alertErrorRequiredFields}
    ${alertMessage}    Get Text    ${loc_alertErrorRequiredFields}
    Should Be Equal    Please complete the required Campaign Settings field before proceeding.    ${alertMessage}
    Click Element    ${loc_btnAlertOk}  
    
Populate Campaign Settings tab 
    [Arguments]    ${datacol}
    Set Rewards Campaign Details    ${datacol}     
    Wait Until Element Is Visible    ${loc_inputDesignerCode}
    #Code
    Input Text    ${loc_inputDesignerCode}    ${REWARDSCAMPAIGNS.GeneralCode}  
    #Name 
    Input Text    ${loc_inputDesignerName}    ${REWARDSCAMPAIGNS.GeneralName}   
    #Type
    Select From List By Label    ${loc_cboDesignerType}    ${REWARDSCAMPAIGNS.GeneralType}
    #Entity
    Select From List By Label    ${loc_cboDesignerEntity}    ${REWARDSCAMPAIGNS.GeneralEntity}  
    #Description 
    Input Text    ${loc_inputDesignerDesc}    ${REWARDSCAMPAIGNS.GeneralDescription}
    #Start Date
    Input Text    ${loc_inputDesignerStartDate}    ${REWARDSCAMPAIGNS.StartDate}
    #End Date
    Input Text    ${loc_inputDesignerEndDate}    ${REWARDSCAMPAIGNS.EndDate}
    
Populate Qualifiers - Rewards - Notifications tab
    Click Element    ${loc_qualifierRewardsNotiTab}
    Drag And Drop    ${loc_designerAddQualifier}    ${loc_designerContainer} 
    Click Element    ${loc_designerQualifier}
    #Code
    Wait Until Element Is Visible    ${loc_inputQualifierCode}
    Input Text    ${loc_inputQualifierCode}    ${REWARDSCAMPAIGNS.QualifiersCode}
    #Name 
    Input Text    ${loc_inputQualifierName}    ${REWARDSCAMPAIGNS.QualifiersName}
    #Description 
    Input Text    ${loc_inputQualifierDesc}    ${REWARDSCAMPAIGNS.QualifersDescription}
    #Save
    Click Element    ${loc_btnQualifierSave}    
    
Verify alert message when successfully saved
    Wait Until Element Is Visible    ${loc_alertSaveSuccess}
    ${alertMessage}    Get Text    ${loc_alertSaveSuccess}
    Should Be Equal    Campaign has been saved successfully    ${alertMessage}
    Click Element    ${loc_btnAlertSuccessOk}
    User click Back icon
    
Verify that campaign designer is present in Group Name dropdown
    Set Rewards Campaign Details    2
    Wait Until Element Is Visible    ${loc_cboRewardListingGroupName}
    Select From List By Label    ${loc_cboRewardListingGroupName}    ${REWARDSCAMPAIGNS.GeneralName}
    Element Should Be Visible    //*[@id="ctl00_cphBody_uiddlGroupName"]/option[contains(.,'${REWARDSCAMPAIGNS.GeneralName}')]
    
User try to update Reward Campaign
    [Arguments]    ${code}
    #Code
    Input Text    ${loc_inputRewardListingCode}    ${code}
    #Search
    Click Element    ${loc_btnRewardListingSearch}
    
    User click Edit Item of code ${code}
    #Update Description
    Input Text    ${loc_inputGeneralDescription}    Update the value for testing only
    User click Save And Publish icon
    #Proceed
    Wait Until Element Is Visible    ${loc_btnCampaignReviewProceed}
    Click Element    ${loc_btnCampaignReviewProceed}
    
User click ${icon} of code ${code}
    ${iconLocation}    Set Variable   //td[7][contains(., '${code}')]/../td[2]//i[@data-original-title='${icon}']
    Wait Until Element Is Visible    ${iconLocation}
    Click Element    ${iconLocation}
    
The Reward Campaign Listing status should be ${status}        
    Wait Until Element Is Visible    ${loc_rewardListingMessage}
    ${pushNotificationStatus}    Get Text    ${loc_rewardListingMessage}
    Should Be Equal    ${status}    ${pushNotificationStatus}
    
User try to update Campaign Designer
    [Arguments]    ${code}
    #Code
    Input Text    ${loc_inputRewardListingCode}    ${code}
    #Search
    Click Element    ${loc_btnRewardListingSearch}
    
    User click Edit Item of code ${code}
    #Update Campaign Settings Description
    Input Text    ${loc_inputDesignerDesc}    Update the value for testing only
    #Save
    Click Element    ${loc_btnQualifierSave}
    
User try to use Delete icon to delete Reward Campaign
    [Arguments]    ${code}
    #Code
    Input Text    ${loc_inputRewardListingCode}    ${code}
    #Search
    Click Element    ${loc_btnRewardListingSearch}
    User click Delete Item of code ${code}
    
Verify if delete confirmation popup window is prompted
    Content Header Should Be Equal    Delete Confirmation
    #Reason for delete
    Input Text    ${loc_inputRewardListingDeleteReason}    For Testing Only
    #Proceed
    Click Element    ${loc_btnRewardListingDeleteProceed}
    
User try to use Delete Checked icon to delete Reward Campaign
    [Arguments]    ${code}
    #Code
    Input Text    ${loc_inputRewardListingCode}    ${code}
    #Search
    Click Element    ${loc_btnRewardListingSearch}
    User click Delete Checked icon
    
Verify Alert message if no checked record
    Alert Should Be Present    text=Please choose a record
    
User check a record and use Delete Checked icon to delete
    [Arguments]    ${code}
    Wait Until Element Is Visible    //td[6][contains(., '${code}')]/../td[1]/span/input
    Click Element    //td[7][contains(., '${code}')]/../td[1]/span/input
    User click Delete Checked icon
    
User can close and cancel Delete window in Reward Campaign Listing   
    User close the window
    User click Delete Checked icon
    Wait Until Element Is Visible    ${loc_btnRewardListingDeleteCancel}
    Click Element    ${loc_btnRewardListingDeleteCancel}    
    
User can successfully delete and item
    Wait Until Element Is Visible    ${loc_btnRewardListingDeleteProceed}
    #Proceed
    Click Element    ${loc_btnRewardListingDeleteProceed}
    Wait Until Element Is Visible    ${loc_textAreaError}
    Page Should Contain Element    ${loc_textAreaError}
    #Reason for deletion
    Input Text   ${loc_inputRewardListingDeleteReason}    For Testing Only
    Click Element    ${loc_btnRewardListingDeleteProceed}
    The Reward Campaign Listing status should be The campaign(s) have been successfully deleted.
    
User search for Reward Campaign
    [Arguments]    ${code}
    #Code
    Input Text    ${loc_inputRewardListingCode}    ${code}
    #Search
    Click Element    ${loc_btnRewardListingSearch}
    
User duplicate existing campaign
    [Arguments]    ${code}    ${datacol}
    User click Duplicate Reward Campaign of code ${code}
    Set Rewards Campaign Details    ${datacol}
    Wait Until Element Is Visible    ${loc_inputGeneralCode}
    #Code
    Input Text    ${loc_inputGeneralCode}    ${REWARDSCAMPAIGNS.GeneralCode}  
    #Name 
    Input Text    ${loc_inputGeneralName}    ${REWARDSCAMPAIGNS.GeneralName}
    #Save
    User click Save And Publish icon
    #Proceed
    Wait Until Element Is Visible    ${loc_btnCampaignReviewProceed}
    Click Element    ${loc_btnCampaignReviewProceed}