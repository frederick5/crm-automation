*** Settings ***
Resource    ../../Global/GlobalSettings.robot

*** Variables ***
${SMSBLAST}    ${EMPTY}
${recipientCount}    ${EMPTY}        
*** Keywords ***
Set SMS Blast Details
    [Arguments]    ${datacol}          
    &{SMSBLAST}    Create Dictionary
    &{SMSBLAST}    Get Data From Excel    ${var_dataBlast}    SMS Blast    ${datacol} 
    ${SMSBLAST.BlastStartDateDay}    Convert To String    ${SMSBLAST.BlastStartDateDay}
    ${SMSBLAST.StopRecurringOn}    Convert To String    ${SMSBLAST.StopRecurringOn}
    ${toCount}    Split String    ${SMSBLAST.Recipients}    ;    
    ${recipientCount}    Get Length    ${toCount}
    ${recipientCount}    Convert To String    ${recipientCount}   
    Set Global Variable    &{SMSBLAST}
    Set Global Variable    ${recipientCount}
                            
Populate SMS Blast Details
    [Arguments]    ${datacol}
    Set SMS Blast Details    ${datacol}
    Wait Until Element Is Visible    ${loc_inputBlastName}
    #Blast Name
    Input Text    ${loc_inputBlastName}    ${SMSBLAST.BlastName}
    #Click Next
    Click Element    ${loc_btnNext}
    #Configure SMS Blast Schedule
    Set SMS Blast Schedule
    #Configure SMS Message
    Configure SMS Message
    #Configure SMS Recipients
    Configure SMS Recipients
    #Configure SMS Approvers
    Configure SMS Approvers
    #Approver Approve SMS Blast
    
Set SMS Blast Schedule
    Sleep    2s
    #Click Set SMS Schedule
    User click Set SMS Schedule icon
    #Schedule Option
    Run Keyword If    '${SMSBLAST.ScheduleOption}' == 'Once'
	...    Execute Javascript    document.getElementById('${loc_rdoSchedOptionOnce}').click()
	...    ELSE IF    '${SMSBLAST.ScheduleOption}' == 'Daily'
	...    Execute Javascript    document.getElementById('${loc_rdoSchedOptionDaily}').click()
	...    ELSE IF    '${SMSBLAST.ScheduleOption}' == 'Weekly'
	...    Execute Javascript    document.getElementById('${loc_rdoSchedOptionWeekly}').click()
	...    ELSE IF    '${SMSBLAST.ScheduleOption}' == 'Monthly'
	...    Execute Javascript    document.getElementById('${loc_rdoSchedOptionMonthly}').click()
    #Blast/Start Date - Month
	:FOR    ${i}    IN RANGE    12
	\    ${currentMonth}    Get Text    //table[@class='cal_title']//td[2]
	\    Wait Until Element Is Visible    //table[@class='cal_title']//td[3]        
	\    Run Keyword Unless    '${SMSBLAST.BlastStartDateMonth}' in '${currentMonth}'    Click Element    //table[@class='cal_title']//td[3] 
	\    Exit For Loop If    '${SMSBLAST.BlastStartDateMonth}' in '${currentMonth}'
	#Blast/Start Date - Day
    Wait Until Element Is Visible    //td/a[@title='${SMSBLAST.BlastStartDateMonth} ${SMSBLAST.BlastStartDateDay}']
    Run Keyword If    '${SMSBLAST.BlastStartDateDay}' != '{$EMPTY}'    Click Element    //td/a[@title='${SMSBLAST.BlastStartDateMonth} ${SMSBLAST.BlastStartDateDay}']
    #Stop Recurring On
    Run Keyword If    '${SMSBLAST.ScheduleOption}' != 'Once'    Input Text    ${loc_dateStopRecurring}    ${SMSBLAST.StopRecurringOn} 
    #Submit
    Sleep    1s
    Wait Until Element Is Visible    ${loc_btnSMSSubmit}    10
    Click Element    ${loc_btnSMSSubmit}
    
Configure SMS Message
    Sleep    1s
    #Click Configure SMS Message
    User click Configure SMS Message icon
    #Select Sender
    Select From List By Label    ${loc_listSMSSender}    ${SMSBLAST.SelectSender}
    #SMS Message
    Sleep    1s
    Wait Until Element Is Visible    ${loc_inputSMSMessage}
    Input Text    ${loc_inputSMSMessage}    ${SMSBLAST.SMSMessage}
    #Click Save
    Click Element    ${loc_btnSMSMessageSave}
    
Configure SMS Recipients
    Sleep    1s
    #Click Configure SMS Recipients
    User click Configure SMS Recipients icon
    #Key in your own SMS list
    Click Element    ${loc_btnKeyInSMSList}
    ${recipients}    Replace String    ${SMSBLAST.Recipients}    ;    \n       
    Wait Until Element Is Visible    ${loc_inputKeyInSMSRecipients}        
    Input Text    ${loc_inputKeyInSMSRecipients}    ${recipients}
    #Generate SMS List
    Click Element    ${loc_btnGenerateSMSList}
    #Click Return
    User click Return icon
    
Configure SMS Approvers
    Sleep    1s
    #Click Configure Approvers
    User click Select Approval Recipients icon
    #Add User
    User click Add User icon
    #Filter
    Sleep    1s
    User click Filter icon
    #Filter > Search By
    Wait Until Element is Visible    ${loc_listApproverSearchBy}
    Select From List By Label    ${loc_listApproverSearchBy}    UserId
    #Filter > Value
    Input Text    ${loc_listApproverSearchValue}    ${SMSBLAST.Approvers} 
    #Search
    Click Element    ${loc_btnApproverSearch}
    #Select from Approvers list
    Wait Until Element Is Visible    ${loc_tblApprovers}   
    Click Element    ${loc_tblApprovers}//td[2][@title='${SMSBLAST.Approvers}']/..//input
    User click Add Selected icon
    #Click Return
    Sleep    1s
    User click Send Approval Message icon
    
Approver Approve SMS Blast
    Sleep    1s
    Open Mailbox    host=imap.gmail.com    user=marlon@ascentis.com.sg   password=manuaya@1207
    ${LATEST}    Wait For Email    sender=noreply@ascentis.com.sg    status=UNSEEN    timeout=300    
    ${HTML}    Get Links from Email    ${LATEST}
    Switch Window    MAIN    None    Current
    Go To    ${HTML}[1]
    Wait Until Element Is Visible    ${loc_btnApproveEmailBlast}
    Click Element    ${loc_btnApproveEmailBlast}
    Close Mailbox
    
User Start the SMS Blast
    User Navigate To Marketing > SMS Blast
    #Edit the email blast
    Wait Until Element Is Visible    ${loc_tblSMSBlast}    
    Click Element    //td[@title='${SMSBLAST.BlastName}']/../td[2]//div[@id='ctl00_cphBody_ucgvSmsBlast_uigvItemList_ctl02_uipnlbtnEditSMSBlast']
    #Click Start
    Wait Until Element Is Visible    ${loc_btnStartSMSBlast}
    Click Element    ${loc_btnStartSMSBlast}
    #Enter the reason
    Input Text    ${loc_inputStartReason}    for testing only
    #Click Proceed
    Click Element    ${loc_btnProceedStart}
    
SMS Blast Status Should Be ${status}
    User Navigate To Marketing > SMS Blast
    #Verify status
    ${blastStatus}    Get Text    //td[@title='${SMSBLAST.BlastName}']/../td[6]
    Should Be Equal    ${status}    ${blastStatus}
    #Verify Total Sent and Error
    Wait Until Element Is Visible    //td[@title='${SMSBLAST.BlastName}']
    Click Element    //td[@title='${SMSBLAST.BlastName}']/../td[2]//div[@id='ctl00_cphBody_ucgvSmsBlast_uigvItemList_ctl02_uipnlbtnEditSMSBlast']
    #Verify Total Sent
    ${totalSent}    Get Text    ${loc_txtTotalSent}
    Should Be Equal    ${totalSent}    ${recipientCount}        
    #Verify Total Error
    ${totalError}    Get Text    ${loc_txtTotalError}
    Should Be True    '${totalError}'=='0'    
     