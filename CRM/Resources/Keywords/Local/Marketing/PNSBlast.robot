*** Settings ***
Resource    ../../Global/GlobalSettings.robot

*** Variables ***
${PNSBLAST}    ${EMPTY}
${recCount}    ${EMPTY}
    
*** Keywords ***
Set PNS Blast Details
    [Arguments]    ${datacol}          
    &{PNSBLAST}    Create Dictionary
    &{PNSBLAST}    Get Data From Excel    ${var_dataBlast}    PNS Blast    ${datacol}
    Set Global Variable    &{PNSBLAST}
    
Populate Push Notification Blast Details
    [Arguments]    ${datacol}
    Set PNS Blast Details    ${datacol}
    Wait Until Element Is Visible    ${loc_inputBlastName}
    #Blast Name
    Input Text    ${loc_inputBlastName}    ${PNSBLAST.BlastName} 
    #Blast Type
    Run Keyword If    '${PNSBLAST.BlastType}' != '${EMPTY}'    Select From List By Label    ${loc_listBlastType}    ${PNSBLAST.BlastType}
    #Member Filter By
    Run Keyword If    '${PNSBLAST.MemberFilterBy}' != '${EMPTY}'    Click Element    //table[@id='ctl00_cphBody_urdlMemberFilterBy']//input[@value='${PNSBLAST.MemberFilterBy}']
    #Status
    Run Keyword If    '${PNSBLAST.Status}' != '${EMPTY}'    Click Element    //table[@id='ctl00_cphBody_uirdlStatus']//input[@value='${PNSBLAST.Status}']
    #Application Name
    Run Keyword If    '${PNSBLAST.ApplicationName}' != '${EMPTY}'    Select From List By Value    ${loc_listApplicationName}    ${PNSBLAST.ApplicationName}
    #Device Model
    Run Keyword If    '${PNSBLAST.DeviceModel}' != '${EMPTY}'    Select From List By Value    ${loc_listDeviceModel}    ${PNSBLAST.DeviceModel}
    #Is Development
    ${isChecked}    Run Keyword And Return Status    Checkbox Should Be Selected    ${loc_chkIsDevelopment}      
    Run Keyword If    '${PNSBLAST.IsDevelopment}' == 'Yes' and '${isChecked}'=='False'    Select Checkbox    ${loc_chkIsDevelopment} 
    ...    ELSE IF    '${PNSBLAST.IsDevelopment}' == 'No' and '${isChecked}'=='True'    Click Element    ${loc_chkIsDevelopment} 
    #Push Badge
    ${isChecked}    Run Keyword And Return Status    Checkbox Should Be Selected    ${loc_chkPushBadge}   
    Log To Console    ${isChecked}    
    Run Keyword If    '${PNSBLAST.PushBadge}' == 'Yes' and '${isChecked}'=='False'    Select Checkbox    ${loc_chkPushBadge} 
    ...    ELSE IF    '${PNSBLAST.PushBadge}' == 'No' and '${isChecked}'=='True'    Click Element    ${loc_chkPushBadge} 
    #Push Alert
    ${isChecked}    Run Keyword And Return Status    Checkbox Should Be Selected    ${loc_chkPushAlert}      
    Log To Console    ${isChecked}
    Run Keyword If    '${PNSBLAST.PushAlert}' == 'Yes' and '${isChecked}'=='False'    Select Checkbox    ${loc_chkPushAlert}
    ...    ELSE IF    '${PNSBLAST.PushAlert}' == 'No' and '${isChecked}'=='True'    Click Element    ${loc_chkPushAlert}
    #Push Sound
    ${isChecked}    Run Keyword And Return Status    Checkbox Should Be Selected    ${loc_chkPushSound}
    Run Keyword If    '${PNSBLAST.PushSound}' == 'Yes' and '${isChecked}'=='False'    Select Checkbox    ${loc_chkPushSound}
    ...    ELSE IF    '${PNSBLAST.PushSound}' == 'No' and '${isChecked}'=='True'    Click Element    ${loc_chkPushSound}
    #Customized Attributes
    ${isChecked}    Run Keyword And Return Status    Checkbox Should Be Selected    ${loc_chkCustomizedAttributes}
    Run Keyword If    '${PNSBLAST.CustomizedAttributes}' == 'Yes' and '${isChecked}'=='False'    Select Checkbox    ${loc_chkCustomizedAttributes}
    ...    ELSE IF    '${PNSBLAST.CustomizedAttributes}' == 'No' and '${isChecked}'=='True'    Click Element    ${loc_chkCustomizedAttributes}
    #Retrieve Attributes
    Sleep    1s
    ${isVisible}    Run Keyword And Return Status    Element Should Be Visible    ${loc_listRetrieveAttributes}     
    Run Keyword If    '${PNSBLAST.CustomizedAttributes}' == 'Yes' and '${isVisible}' == 'True'     Select From List By Label    ${loc_listRetrieveAttributes}
    #Click Next
    Click Element    ${loc_btnNext}
    #Configure PNS Blast Schedule
    Configure PNS Blast Schedule
    #Configure PNS Message
    Configure PNS Message
    #Configure PNS Recipients
    Configure PNS Recipients
    #Configure PNS Approvers
    Configure PNS Approvers
    
    
Configure PNS Blast Schedule
    Sleep    2s
    #Click Configure Blast Schedule
    User click Configure Blast Schedule icon
    #Schedule Option
    Run Keyword If    '${PNSBLAST.ScheduleOption}' == 'Once'
	...    Execute Javascript    document.getElementById('${loc_rdoSchedOptionOnce}').click()
	...    ELSE IF    '${PNSBLAST.ScheduleOption}' == 'Daily'
	...    Execute Javascript    document.getElementById('${loc_rdoSchedOptionDaily}').click()
	...    ELSE IF    '${PNSBLAST.ScheduleOption}' == 'Weekly'
	...    Execute Javascript    document.getElementById('${loc_rdoSchedOptionWeekly}').click()
	...    ELSE IF    '${PNSBLAST.ScheduleOption}' == 'Monthly'
	...    Execute Javascript    document.getElementById('${loc_rdoSchedOptionMonthly}').click()
	Wait Until Element Is Visible    //table[@class='cal_title']//td[2]
	#Blast/Start Date - Month
	:FOR    ${i}    IN RANGE    12
	\    ${currentMonth}    Get Text    //table[@class='cal_title']//td[2]
	\    Wait Until Element Is Visible    //table[@class='cal_title']//td[3]        
	\    Run Keyword Unless    '${PNSBLAST.BlastStartDateMonth}' in '${currentMonth}'    Click Element    //table[@class='cal_title']//td[3] 
	\    Exit For Loop If    '${PNSBLAST.BlastStartDateMonth}' in '${currentMonth}'
	#Blast/Start Date - Day
    Wait Until Element Is Visible    //td/a[@title='${PNSBLAST.BlastStartDateMonth} ${PNSBLAST.BlastStartDateDay}']
    Run Keyword If    '${PNSBLAST.BlastStartDateDay}' != '{$EMPTY}'    Click Element    //td/a[@title='${PNSBLAST.BlastStartDateMonth} ${PNSBLAST.BlastStartDateDay}']
    #Stop Recurring On
    Run Keyword If    '${PNSBLAST.ScheduleOption}' != 'Once'    Input Text    ${loc_dateStopRecurring}    ${PNSBLAST.StopRecurringOn} 
    #Submit
    Sleep    1s
    Wait Until Element Is Visible    ${loc_btnEmailSubmit}    10
    Click Element    ${loc_btnEmailSubmit}
    
Configure PNS Message
    Sleep    1s
    User click Configure Push Notification icon
    #Use Existing Template
    ${isChecked}    Run Keyword And Return Status    Checkbox Should Be Selected    ${loc_chkUseExistingTemplate}
    Run Keyword If    '${PNSBLAST.UseExistingTemplate}' == 'Yes' and '${isChecked}'=='False'    Select Checkbox    ${loc_chkUseExistingTemplate}
    ...    ELSE IF    '${PNSBLAST.UseExistingTemplate}' == 'No' and '${isChecked}'=='True'    Click Element    ${loc_chkUseExistingTemplate}
    #Push Notification Template
    Run Keyword If    '${PNSBLAST.UseExistingTemplate}' == 'Yes'    Select From List By Label    ${loc_listPNSTemplate}    ${PNSBLAST.PushNotificationTemplate}
    #Push Notification Message
    Wait Until Element Is Visible    ${loc_inputPushNotifMsg}
    Run Keyword If    '${PNSBLAST.UseExistingTemplate}' != 'Yes'    
    ...    Run Keywords
    #Push Notification Message
    ...    Input Text    ${loc_inputPushNotifMsg}    ${PNSBLAST.PushNotificationMessage}
    #In-App Subject
    ...    AND    Input Text    ${loc_inputInAppSubject}    ${PNSBLAST.InAppSubject}
    #In-App Description
    ...    AND    Input Text    ${loc_inputInAppDescription}    ${PNSBLAST.InAppDescription}
    #In-App Image
    #...    AND    Choose File    ${loc_uploadInAppImage}    ${var_PNSInAppImage}
    #In-App Message
    Run Keyword If    '${PNSBLAST.UseExistingTemplate}' != 'Yes'
    ...    Run Keywords       
    ...    Wait Until Element Is Visible    //iframe[@class='cke_wysiwyg_frame cke_reset']   
    ...    AND    Select Frame    //iframe[@class='cke_wysiwyg_frame cke_reset']
    ...    AND    Input Text    ${loc_inputEmailMessage}    ${PNSBLAST.InAppMessage}
    ...    AND    Unselect Frame
    #Deep Link
    Run Keyword If    '${PNSBLAST.UseExistingTemplate}' != 'Yes' and '${PNSBLAST.DeepLink}' != '${EMPTY}'    Select From List By Label    ${loc_listDeepLink}    ${PNSBLAST.DeepLink}
    #Submit
    Click Element    ${loc_btnPNSMsgSubmit}
    #Return
    Sleep    2s
    Wait Until Element Is Visible    ${loc_btnPNSMsgReturn}    
    Click Element    ${loc_btnPNSMsgReturn}
    
Configure PNS Recipients
    Sleep    1s
    #Click Configure Recipients
    User click Configure Recipients icon
    #Inclusion Add From Query
    Wait Until Element Is Visible    ${loc_btnInclusionAddFromQuery}
    Click Element    ${loc_btnInclusionAddFromQuery}
    #Query Name
    Wait Until Element Is Visible    ${loc_inputIncQueryName}
    Input Text    ${loc_inputIncQueryName}    ${PNSBLAST.RecipientsInclusionQueryName}
    #Query Description
    Input Text    ${loc_inputIncDescription}   ${PNSBLAST.RecipientsInclusionDescription}
    #Search
    Click Element    ${loc_btnQuerySeach}
    #Select query from table
    Sleep    2s
    Wait Until Element Is Visible    ctl00_cphBody_ucgvExistingStoredQueryList_uigvItemList
    Click Element    //td[2][contains(., '${PNSBLAST.RecipientsInclusionQueryName}')]/..//td[3][contains(., '${PNSBLAST.RecipientsInclusionDescription}')]/../td[1]//li[@id='uibtnSelectStoredQuery']//span    
    #Add Selected
    Sleep    1s
    Click Element    ${loc_btnQueryAddSelected}
    #Generate Recipient List
    Sleep    1s
    Wait Until Element Is Visible    ${loc_btnGenerateRecipientList}    
    Click Element    ${loc_btnGenerateRecipientList}
    Wait Until Element Is Visible    ${loc_tblGeneratedRecipients}
    ${recipientsCount}    Get Element Count    //table[@id='ctl00_cphBody_ucgvPushNotificationBlastMailingList_uigvItemList']//tr[@class='advGridViewCellWrap']   
    ${recCount}    Set Global Variable    ${recipientsCount}
    #Return
    User Click Return Icon
    
Configure PNS Approvers
    Sleep    1s
    #Click Configure Approvers
    User click Configure Approvers icon
    #Add User
    User click Add User icon
    #Filter
    Sleep    1s
    User click Filter icon
    #Filter > Search By
    Wait Until Element is Visible    ${loc_listApproverSearchBy}
    Select From List By Label    ${loc_listApproverSearchBy}    UserId
    #Filter > Value
    Input Text    ${loc_listApproverSearchValue}    ${PNSBLAST.Approvers} 
    #Search
    Click Element    ${loc_btnApproverSearch}
    #Select from Approvers list
    Wait Until Element Is Visible    ${loc_tblApprovers}   
    Click Element    ${loc_tblApprovers}//td[2][@title='${PNSBLAST.Approvers}']/..//input
    User click Add Selected icon
    #Click Return
    Sleep    1s
    User click Send Approval Message icon
    
Approver Approve PNS Blast
    Sleep    1s
    Open Mailbox    host=${var_emailHost}    user=${var_emailApprover}   password=${var_emailApproverPwd}
    ${LATEST}    Wait For Email    sender=${var_emailPNSApprovalSender}    status=UNSEEN    timeout=300    
    ${HTML}    Get Links from Email    ${LATEST}
    Switch Window    MAIN    None    Current
    Go To    ${HTML}[1]
    Wait Until Element Is Visible    ${loc_btnApproveEmailBlast}
    Click Element    ${loc_btnApproveEmailBlast}
    Close Mailbox
    
User Start the PNS Blast
    User Navigate To Marketing > Push Notifications
    #Edit the pns blast
    Sleep    1s
    Wait Until Element Is Visible    ${loc_tblPNSBlast}  
    Click Element    //td[@title='QA Auto PNS Blast']/../td[2]//*[@id='ctl00_cphBody_ucgvPushNotificationBlastList_uigvItemList_ctl02_uipnlbtnEditPushNotificationBlast']
    #Click Start
    Wait Until Element Is Visible    ${loc_btnStartPNSBlast}
    Click Element    ${loc_btnStartPNSBlast}
    #Enter the reason
    #Input Text    ${loc_inputStartReason}    for testing only
    #Click Proceed
    #Click Element    ${loc_btnProceedStart}
    
PNS Blast Status Should Be ${status}
    User Navigate To Marketing > Push Notifications
    #Verify status
    :FOR    ${i}    IN RANGE    20
    \    Reload Page
    \    Wait Until Element Is Visible    //td[@title='${PNSBLAST.BlastName}']/../td[7]
    \    ${blastStatus}    Get Text    //td[@title='${PNSBLAST.BlastName}']/../td[7]
    \    Exit For Loop If    '${status}' == '${blastStatus}'
    Should Be Equal    ${status}    ${blastStatus}    
    #Verify Total Sent and Error
    Wait Until Element Is Visible    //td[@title='${PNSBLAST.BlastName}']
    Click Element    //td[@title='${PNSBLAST.BlastName}']/../td[2]//div[@id='ctl00_cphBody_ucgvPushNotificationBlastList_uigvItemList_ctl02_uipnlbtnEditPushNotificationBlast']
    #Verify Total Sent
    ${totalSent}    Get Text    ctl00_cphBody_uilblEmailBlast_TotalSent
    Should Be Equal    ${totalSent}    ${recCount}        
    #Verify Total Error
    ${totalError}    Get Text    ctl00_cphBody_uilblEmailBlast_TotalErrors
    Should Be True    '${totalError}'=='0'
                    
Push Notification Blast Listing Is Displayed
    Wait Until Element Is Visible    ${loc_tblPNSBlast}
    #Verify Page Header
    Content Header Should Be Equal    Push Notification Blast Listing
    #Verify Page contains PNS Blast table 
    Page Should Contain Element    ${loc_tblPNSBlast}
    #Verify PNS Blast table is not empty  
    Element Should Not Contain    ${loc_tblPNSBlast}    No record(s) found.  
    
User Edit the UserGroup
    Wait Until Element Is Visible    ctl00_cphBody_ucgvList_uigvItemList
    Click Element    //td[4][@title='QAAUTOADMIN']/../td[2]//li
    
Grant ${module} > ${function} Access
    Wait Until Element Is Visible    v-pills-access
    Click Element    //a[contains(text(), '${module}')]
    #Grant ${function} Access
    ${loc_chkFunction}    Set Variable    //td//label[contains(text(), '${function}')]/../input
    ${isChecked}    Run Keyword And Return Status    Checkbox Should Be Selected    ${loc_chkFunction}
    Run Keyword If    '${isChecked}'=='False'    Select Checkbox    ${loc_chkFunction}
    User click Save icon        

Push Notifications Management Is Available
    Wait Until Element Is Visible    //span[contains(., 'Marketing')]
    Mouse Over    //span[contains(., 'Marketing')]
    Sleep    1s    
    Element Should Be Visible    //span[contains(., 'Push Notifications')]
    Click Link    Push Notifications
    Push Notification Blast Listing Is Displayed              