*** Settings ***
Resource    ../../../Global/GlobalSettings.robot

*** Variables ***
${EMAILTEMPLATES}    ${EMPTY}
*** Keywords ***
Set Email Template
    [Arguments]    ${datacol}          
    ${EMAILTEMPLATES}    Create Dictionary
    ${EMAILTEMPLATES}    Get Data From Excel    ${var_dataBlast}    EmailTemplates    ${datacol}   
    Set Global Variable    ${EMAILTEMPLATES}
    
Filter Email Template By ${option}
    Wait Until Element Is Visible    ${loc_cboTemplateSearchBy}
     #Search By and Value
    ${datacol}    Run Keyword if    '${option}' == 'Name'    Set Variable    1
    ...    ELSE IF    '${option}' == 'Sender Name'    Set Variable    2
    ...    ELSE IF    '${option}' == 'Sender Email'    Set Variable    3
    Set Email Template     ${datacol}
    Select From List By Label   ${loc_cboTemplateSearchBy}    ${EMAILTEMPLATES.Search}
    Input Text    ${loc_inputTemplateValue}    ${EMAILTEMPLATES.Value}    
    Click Element                ${loc_btnTemplateSearch}
    
User able to search for Email Template using the search filters available
    Page Should Contain Element    ${loc_tableEmailTemplateList}
    Page Should Contain Element    ${loc_emailTemplateRowsCount}
    
Populate Template Information
    [Arguments]    ${datacol}
    Set Email Template    ${datacol}
    Wait Until Element Is Visible    ${loc_cboTemplateSelectSender}
    Select From List By Label    ${loc_cboTemplateSelectSender}    ${EMAILTEMPLATES.SelectSender}
    Wait Until Element Is Visible    ${loc_cboTemplateType}
    Select From List By Label    ${loc_cboTemplateType}    ${EMAILTEMPLATES.TemplateType}
    Wait Until Element Is Visible    ${loc_inputTemplateName}
    Input Text    ${loc_inputTemplateName}    ${EMAILTEMPLATES.TemplateName}
    Input Text    ${loc_inputTemplateEmailSubject}    ${EMAILTEMPLATES.EmailSubject}
    Input Text    ${loc_inputTemplateDescription}    ${EMAILTEMPLATES.Description}
    Wait Until Element Is Visible    //iframe[@class='cke_wysiwyg_frame cke_reset']   
    Select Frame    //iframe[@class='cke_wysiwyg_frame cke_reset']
    Input Text    ${loc_templateBody}    ${EMAILTEMPLATES.TemplateBody}
    Unselect Frame
    
    User click Save icon
    sleep    2
    User click Back icon
    
    User click Filter icon
    
    Filter Email Template By Name
    
New Email Template is added on the table
    [Arguments]    ${datacol}
    Set Email Template    ${datacol}
    ${tableName}    Get Text    //table[@id="ctl00_cphBody_ucgvEmailTemplates_uigvItemList"]//td[4][contains(.,'${EMAILTEMPLATES.TemplateName}')]
    Should Be Equal    ${tableName}    ${EMAILTEMPLATES.TemplateName}
    
User click ${icon} icon on ${TemplateName} on Template Listing
    ${iconLocation}    Set Variable   //td[4][contains(., '${TemplateName}')]/../td[2]//i[@data-original-title='${icon}']
    Wait Until Element Is Visible    ${iconLocation}
    Click Element    ${iconLocation}
    
User should be able to delete ${selectTemplate}
    Select template in table ${selectTemplate}
    User click Delete Checked icon
    Template delete confirmation window prompted    1
    The Template Listing status should be The selected items were successfully deleted.
    
Select template in table ${selectedItem}
    Wait Until Element Is Visible    //td[4][contains(., '${selectedItem}')]/../td[1]/span/input
    Click Element    //td[4][contains(., '${selectedItem}')]/../td[1]/span/input
    
The Template Listing status should be ${status}        
    Wait Until Element Is Visible    ${loc_senderNotificationMessage}
    ${senderStatus}    Get Text    ${loc_senderNotificationMessage}
    Should Be Equal    ${status}    ${senderStatus}  
    
Template delete confirmation window prompted
    [Arguments]    ${datacol}
    Set Email Template     ${datacol}
    Content Header Should Be Equal    Delete Confirmation
    Wait Until Element Is Visible    ${loc_btnConfirmationDelete}
    #Proceed
    Click Element    ${loc_btnConfirmationDelete}
    Wait Until Element Is Visible    ${loc_textAreaError}
    Page Should Contain Element    ${loc_textAreaError}
    #Reason for deletion
    Run Keyword If   '${EMAILTEMPLATES.ReasonForDelete}' != '${EMPTY}'  Input Text    ${loc_inputTemplateDeleteReason}    ${EMAILTEMPLATES.ReasonForDelete}
    Click Element    ${loc_btnConfirmationDelete}
    
Email Template should be editable
    [Arguments]    ${datacol}
    Set Email Template     ${datacol}
    Wait Until Element Is Visible   ${loc_cboTemplateSelectSender}
    Element Should Be Enabled    ${loc_cboTemplateSelectSender}
    Element Should Be Enabled    ${loc_cboTemplateType}
    Element Should Be Enabled    ${loc_inputTemplateName}
    Element Should Be Enabled    ${loc_inputTemplateEmailSubject}
    Element Should Be Enabled    ${loc_inputTemplateDescription}
    Input Text    ${loc_inputTemplateName}    ${EMPTY}
    Input Text    ${loc_inputTemplateName}    ${EMAILTEMPLATES.TemplateName}
    User click Save icon
    User click Save icon
    Template Management status should be The email template has been successfully updated.
    
Template Management status should be ${status}
    Wait Until Element Is Visible    ${loc_templateManagementStatus}
    ${senderStatus}    Get Text    ${loc_templateManagementStatus}
    Should Be Equal    ${status}    ${senderStatus} 
    
    
    
    
        
    
    