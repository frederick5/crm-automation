*** Settings ***
Resource    ../../../Global/GlobalSettings.robot

*** Variables ***
${SENDER}    ${EMPTY}
*** Keywords ***
Set Sender
    [Arguments]    ${datacol}          
    &{SENDER}    Create Dictionary
    &{SENDER}    Get Data From Excel    ${var_dataBlast}    Sender    ${datacol}   
    Set Global Variable    &{SENDER}
    
Filter By ${option}
    Wait Until Element Is Visible    ${loc_cboSearchBy}
     #Search By and Value
    ${datacol}    Run Keyword if    '${option}' == 'Name'    Set Variable    1
    ...    ELSE IF    '${option}' == 'Email'    Set Variable    2
    ...    ELSE IF    '${option}' == 'SMS Masked Name'    Set Variable    3
    ...    ELSE IF    '${option}' == 'Mailing List Name'    Set Variable    4
    Set Sender     ${datacol}
    Select From List By Label   ${loc_cboSearchBy}    ${SENDER.Search}
    Input Text    ${loc_inputSenderValue}    ${SENDER.Value}   
    #Search Button 
    Click Element                ${loc_btnSenderSearch}
   
User able to search for a Sender using the search filters available
    Page Should Contain Element    ${loc_senderRowsCount}
    Page Should Contain Element    ${loc_tableSenderList}
    
Populate Required Fields
    [Arguments]    ${datacol}
    Set Sender     ${datacol}
    Wait Until Element Is Visible    ${loc_inputSenderName}
    Wait Until Element Is Visible    ${loc_inputSenderEmailFrom}
    Wait Until Element Is Visible    ${loc_inputRedirectUrl}
    #Sender Name
    Run Keyword If   '${SENDER.SenderName}' != '${EMPTY}'  Input Text    ${loc_inputSenderName}    ${SENDER.SenderName}
    #Sender Email
    Run Keyword If   '${SENDER.SenderEmail}' != '${EMPTY}'  Input Text    ${loc_inputSenderEmailFrom}    ${SENDER.SenderEmail}
    #Redirect Url
    Run Keyword If   '${SENDER.RedirectUrl}' != '${EMPTY}'  Input Text    ${loc_inputRedirectUrl}    ${SENDER.RedirectUrl}
    #Submit
    Click Element    ${loc_btnSenderSubmit}
    
The Sender Listing status should be ${status}
    
    Wait Until Element Is Visible    ${loc_senderNotificationMessage}
    ${senderStatus}    Get Text    ${loc_senderNotificationMessage}
    Should Be Equal    ${status}    ${senderStatus}   
    
Check Sender Name on table
     [Arguments]    ${datacol}
     Set Sender     ${datacol}     
     ${tableName}    Get Text    //table[@id="ctl00_cphBody_ucgvSender_uigvItemList"]//td[3][contains(.,'${SENDER.SenderName}')]
     Should Be Equal    ${tableName}     ${SENDER.SenderName}
        
User select an item
    [Arguments]    ${datacol}
    Set Sender     ${datacol}
    Wait Until Element Is Visible    //*[@id="ctl00_cphBody_ucgvSender_uigvItemList_ctl${SENDER.SelectedItem}_uichkSelected"]
    Click Element    //*[@id="ctl00_cphBody_ucgvSender_uigvItemList_ctl${SENDER.SelectedItem}_uichkSelected"]
    
Delete confirmation window prompted
    [Arguments]    ${datacol}
     Set Sender     ${datacol}
    Content Header Should Be Equal    Delete Confirmation
    Wait Until Element Is Visible    ${loc_btnConfirmationDelete}
    #Proceed
    Click Element    ${loc_btnConfirmationDelete}
    Wait Until Element Is Visible    ${loc_textAreaError}
    Page Should Contain Element    ${loc_textAreaError}
    #Reason for deletion
    Run Keyword If   '${SENDER.ReasonForDelete}' != '${EMPTY}'  Input Text   ${loc_inputReasonForDeletion}    ${SENDER.ReasonForDelete}
    Click Element    ${loc_btnConfirmationDelete}
 
User delete an item
    [Arguments]    ${datacol}
    Set Sender     ${datacol}
    Wait Until Element Is Visible    //*[@id="ctl00_cphBody_ucgvSender_uigvItemList_ctl${SENDER.SelectedItem}_uipnlbtnDeleteSender"]
    Click Element     //*[@id="ctl00_cphBody_ucgvSender_uigvItemList_ctl${SENDER.SelectedItem}_uipnlbtnDeleteSender"]
    
User close the window
    Wait Until Element Is Visible    ${loc_closeWindow}
    Click Element     ${loc_closeWindow}
    
User cancel delete
    Wait Until Element Is Visible    ${loc_btnConfirmationCancel}
    Click Element     ${loc_btnConfirmationCancel}
    
User click ${icon} icon of sender ${senderName}
    ${iconLocation}    Set Variable   //td[3][contains(., '${senderName}')]/../td[2]//i[@data-original-title='${icon}']
    Wait Until Element Is Visible    ${iconLocation}
    Click Element    ${iconLocation}    
  
Update Sender ${valueToUpdate} field
    Set Sender    3
    Wait Until Element Is Visible    ${loc_inputSenderName}
    #Run Keyword if    '${valueToUpdate}' == 'Name'    Input Text    ${loc_inputSenderName}    ${EMPTY}
    Run Keyword if    '${valueToUpdate}' == 'Email'    Input Text    ${loc_inputSenderEmailFrom}    ${SENDER.SenderEmail}
    ...    ELSE IF    '${valueToUpdate}' == 'SMS Masked Name'    Input Text    ${loc_inputSmsMaskedName}     ${SENDER.SMSMaskedName}
    ...    ELSE IF    '${valueToUpdate}' == 'Mailing List Name'    Input Text    ${loc_inputMailingListName}    ${EMPTY}
    ...    ELSE IF    '${valueToUpdate}' == 'Redirect Url'    Input Text    ${loc_inputRedirectUrl}    ${SENDER.RedirectUrl}
    
    Wait Until Element Is Enabled    ${loc_inputSenderEmailFrom}
    Wait Until Element Is Enabled    ${loc_inputSmsMaskedName}
    Wait Until Element Is Enabled    ${loc_inputRedirectUrl}
    sleep    2
    #Run Keyword if    '${valueToUpdate}' == 'Name'    Press Keys        ${loc_inputSenderEmailFrom}    TAB
    #Run Keyword if    '${valueToUpdate}' == 'Name'    Input Text    ${loc_inputSenderName}    ${SENDER.SenderName}
    #Run Keyword if    '${valueToUpdate}' == 'Name'    Click Element    ${loc_btnSenderSubmit}
    #Wait Until Element Is Visible     ${loc_btnSenderSubmit}
    #Run Keyword if    '${valueToUpdate}' == 'Name'    Click Element    ${loc_btnSenderSubmit}
    
    Run Keyword if    '${valueToUpdate}' == 'Mailing List Name'    Press Keys        ${loc_inputSenderEmailFrom}    TAB
    Run Keyword if    '${valueToUpdate}' == 'Mailing List Name'    Input Text    ${loc_inputMailingListName}    ${SENDER.SenderMailingListName}
    Run Keyword if    '${valueToUpdate}' == 'Mailing List Name'    Click Element    ${loc_btnSenderSubmit}
    Run Keyword if    '${valueToUpdate}' == 'Mailing List Name'    Wait Until Element Is Visible     ${loc_btnSenderSubmit}
    Run Keyword if    '${valueToUpdate}' == 'Mailing List Name'    Click Element    ${loc_btnSenderSubmit}
    
    Run Keyword if    '${valueToUpdate}' == 'Email'    Click Element    ${loc_btnSenderSubmit}
    Run Keyword if    '${valueToUpdate}' == 'SMS Masked Name'    Click Element    ${loc_btnSenderSubmit}
    Run Keyword if    '${valueToUpdate}' == 'Redirect Url'    Click Element    ${loc_btnSenderSubmit}
 
    Run Keyword if    '${valueToUpdate}' == 'Email'    Click Element    ${loc_btnSenderSubmit}
    Run Keyword if    '${valueToUpdate}' == 'Redirect Url'    Click Element    ${loc_btnSenderSubmit}
    

The ${contentPageHeadeValue} is displayed
    Wait Until Element Is Visible    ${loc_contentPageHeaderSender}
    ${contentPageHeader}    Get Text    ${loc_contentPageHeaderSender}
    Should Be Equal    ${contentPageHeader}    ${contentPageHeadeValue}    
    
Information can be editted
    Wait Until Element Is Visible    ${loc_inputSenderName}
    ${statusName}=    Run Keyword And Return Status    Element Should Be Enabled    ${loc_inputSenderName}
    ${statusEmail}=    Run Keyword And Return Status    Element Should Be Enabled    ${loc_inputSenderEmailFrom}
    ${statusSMSMaskedName}=    Run Keyword And Return Status    Element Should Be Enabled    ${loc_inputSmsMaskedName}
    ${statusMailingListName}=    Run Keyword And Return Status    Element Should Be Enabled    ${loc_inputMailingListName}
    ${statusRedirectUrl}=    Run Keyword And Return Status    Element Should Be Enabled    ${loc_inputRedirectUrl}
    
    Should Be True   ${statusName} 
    Should Be True   ${statusEmail} 
    Should Be True   ${statusSMSMaskedName} 
    Should Be True   ${statusMailingListName}
    Should Be True   ${statusRedirectUrl}
    
Display EDM Creator Username is ${edmOption}    
    Wait Until Element Is Visible    ${loc_radioEDMCreatorUsername}
    ${unchecked}    Run Keyword And Return Status    Checkbox Should Not Be Selected    ${loc_radioEDMCreatorUsername}
    Run Keyword if    '${edmOption}' == 'On' and '${unchecked}'=='True'    Click Element    ${loc_radioEDMCreatorUsername}
    ...    ELSE IF    '${edmOption}' == 'Off' and '${unchecked}'=='False'    Click Element    ${loc_radioEDMCreatorUsername}   
    sleep    2
    Populate Required Fields    1
    
Create recurrence email blast  
     Populate Email Blast Details    2
     Approver Approve Email Blast for Sender
     User Start the Email Blast for Sender
     
User search for sender to ${option}
    User click Filter icon 
    ${datacol}    Set Variable    1
    Set Sender     ${datacol}
    Select From List By Label   ${loc_cboSearchBy}    ${SENDER.Search}
    ${datacol}    Run Keyword if    '${option}' == 'delete'    Set Variable    2
    ...    ELSE IF    '${option}' == 'edit'    Set Variable    3
    Set Sender     ${datacol}
    Input Text    ${loc_inputSenderValue}    ${SENDER.SenderName}    
    Click Element                ${loc_btnSenderSearch}
    Run Keyword If    '${option}' == 'edit'    User click Edit icon of sender ${SENDER.SenderName}
    
User can close and cancel Delete window
    Set Sender     2
    User click Delete icon of sender ${SENDER.SenderName}
    Then User close the window
    User click Delete icon of sender ${SENDER.SenderName}
    Then User cancel delete

    
        