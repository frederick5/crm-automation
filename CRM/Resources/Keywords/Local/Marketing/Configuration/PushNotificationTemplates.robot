*** Settings ***
Resource    ../../../Global/GlobalSettings.robot

*** Variables ***
${PUSHNOTITEMPLATES}    ${EMPTY}
*** Keywords ***
Set Push Noti Template
    [Arguments]    ${datacol}          
    ${PUSHNOTITEMPLATES}    Create Dictionary
    ${PUSHNOTITEMPLATES}    Get Data From Excel    ${var_dataBlast}    PushNotiTemplates    ${datacol}   
    Set Global Variable    ${PUSHNOTITEMPLATES}

Filter Push Noti Template By Name
    [Arguments]    ${datacol}
    Set Push Noti Template    ${datacol}
    Select From List By Label   ${loc_searchByPushNotiTemp}    ${PUSHNOTITEMPLATES.Search}
    Input Text    ${loc_inputValuePushNotiTemp}    ${PUSHNOTITEMPLATES.Value}    
    Click Element                ${loc_btnSearchPushNotiTemp}
    
User able to search for Push Noti Templates using the search filters available
    Page Should Contain Element    ${loc_pushNotiTempTable}
    Page Should Contain Element    ${loc_pushNotiTempRowsCount}
    
Populate Push Notification Template
    [Arguments]    ${datacol}
    Set Push Noti Template    ${datacol}
    Wait Until Element Is Visible    ${loc_inputPushNotiTempName}
    #Template Name
    Input Text    ${loc_inputPushNotiTempName}    ${PUSHNOTITEMPLATES.Name}
    #Template Description
    Input Text    ${loc_inputPushNotiTempDesc}    ${PUSHNOTITEMPLATES.Description}
    #Template Type
    Select From List By Label    ${loc_cboPushNotiTempType}      ${PUSHNOTITEMPLATES.Type}
    Wait Until Element Is Enabled    ${loc_inputPushNotiTempMessage}
    #Template Push Notifcation Message
    Input Text    ${loc_inputPushNotiTempMessage}     ${PUSHNOTITEMPLATES.PushNotiMessage}
    #Template In App Subject
    Input Text    ${loc_inputPushNotiInAppSubject}     ${PUSHNOTITEMPLATES.InAppSubject}
    #Template In App Description
    Input Text    ${loc_inputPushNotiInAppDescr}     ${PUSHNOTITEMPLATES.InAppDescription}
    #Template In App Message
    Select Frame    //iframe[@class='cke_wysiwyg_frame cke_reset']
    Input Text    ${loc_inputPushNotiInAppMessage}     ${PUSHNOTITEMPLATES.InAppMessage}
    Unselect Frame
    
    User click Save icon
    sleep    2
    User click Filter icon
    Filter Push Noti Template By Name    ${datacol}
    New Push Notification Template is added on the table    ${datacol}
    
New Push Notification Template is added on the table
    [Arguments]    ${datacol}
    Set Push Noti Template    ${datacol}
    Wait Until Element Is Visible    //table[@id="ctl00_cphBody_uigvList_uigvItemList"]//td[3][contains(.,'${PUSHNOTITEMPLATES.Name}')]
    ${tableName}    Get Text    //table[@id="ctl00_cphBody_uigvList_uigvItemList"]//td[3][contains(.,'${PUSHNOTITEMPLATES.Name}')]
    Should Be Equal    ${tableName}    ${PUSHNOTITEMPLATES.Name}
    
Verify that Push Notification Template is available in Rewards Campaign
    [Arguments]    ${datacol}
    Set Push Noti Template    ${datacol}
    User navigates To Loyalty > Campaigns > Rewards Campaigns
    User click Add New icon
    Wait Until Element Is Visible    //*[@id="ctl00_cphBody_uiddlPushTemplate"]
    Select From List By Label    //*[@id="ctl00_cphBody_uiddlPushTemplate"]    ${PUSHNOTITEMPLATES.Name}
    Element Should Be Visible    //*[@id="ctl00_cphBody_uiddlPushTemplate"]/option[contains(.,'${PUSHNOTITEMPLATES.Name}')] 

User validate close in delete window and cancel in delete window
    [Arguments]    ${datacol}
    User click Filter icon
    Filter Push Noti Template By Name    ${datacol}
    User click Delete Item icon on Push Notification Table ${datacol}
    Wait Until Element Is Visible    ${loc_btnConfirmationCancelPushNoti}
    Click Element     ${loc_btnConfirmationCancelPushNoti} 
    User click Delete Item icon on Push Notification Table ${datacol}
    Wait Until Element Is Visible    ${loc_closeWindowPushNoti}
    Click Element     ${loc_closeWindowPushNoti} 
    
User click ${option} icon on Push Notification Table ${datacol}
    Set Push Noti Template    ${datacol}
    ${iconLocation}    Set Variable   //td[3][contains(., '${PUSHNOTITEMPLATES.Value}')]/../td[2]//i[@data-original-title='${option}']
    Wait Until Element Is Visible    ${iconLocation}
    Click Element    ${iconLocation}
    
Push Notification template delete confirmation window prompted
    Content Header Should Be Equal    Delete Confirmation
    Wait Until Element Is Visible    ${loc_btnDeleteConfirmPushNoti}
    #Proceed
    Click Element    ${loc_btnDeleteConfirmPushNoti}
    Wait Until Element Is Visible    ${loc_textAreaErrorPushNoti}
    Page Should Contain Element    ${loc_textAreaErrorPushNoti}
    #Reason for deletion
    Input Text    ${loc_inputReasonForDeletionPushNoti}    Testing
    Click Element    ${loc_btnDeleteConfirmPushNoti}
    
Push Notification Template should be deleted in table
    [Arguments]    ${datacol}
    Select push notification template in table    ${datacol}
    User click Delete Checked icon
    Push Notification template delete confirmation window prompted
    The Push Notification Template Listing status should be The selected items were successfully deleted.
    
The Push Notification Template Listing status should be ${status}        
    Wait Until Element Is Visible    ${loc_PushNotiTempNotificationMessage}
    ${pushNotificationStatus}    Get Text    ${loc_PushNotiTempNotificationMessage}
    Should Be Equal    ${status}    ${pushNotificationStatus}
    
Select push notification template in table
    [Arguments]    ${datacol}    
    Set Push Noti Template    ${datacol}
    Wait Until Element Is Visible    //td[3][contains(., '${PUSHNOTITEMPLATES.Value}')]/../td[1]/span/input
    Click Element    //td[3][contains(., '${PUSHNOTITEMPLATES.Value}')]/../td[1]/span/input
   
User edit information in Push Notification Template
    [Arguments]    ${datacol} 
    User click Filter icon
    Filter Push Noti Template By Name    ${datacol}
    User click Edit Field icon on Push Notification Table ${datacol}
    Set Push Noti Template    ${datacol}
    Then Template Management fields should be edited
    #Template Push Notifcation Message
    Input Text    ${loc_inputPushNotiTempMessage}     ${EMPTY}
    Input Text    ${loc_inputPushNotiTempMessage}     ${PUSHNOTITEMPLATES.PushNotiMessage}
    User click Save icon
    
Template Management fields should be edited
    Element Should Be Enabled    ${loc_inputPushNotiTempDesc}
    Element Should Be Enabled    ${loc_cboPushNotiTempType}
    Element Should Be Enabled    ${loc_inputPushNotiTempMessage}
    Element Should Be Enabled    ${loc_inputPushNotiInAppSubject}
    Element Should Be Enabled    ${loc_inputPushNotiInAppDescr}
    
    