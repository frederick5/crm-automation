*** Settings ***
Resource    ../../../Global/GlobalSettings.robot

*** Variables ***
${EMAILBOUNCE}    ${EMPTY}
*** Keywords ***
Set Email Bounce
    [Arguments]    ${datacol}          
    ${EMAILBOUNCE}    Create Dictionary
    ${EMAILBOUNCE}    Get Data From Excel    ${var_dataBlast}    EmailBounce    ${datacol}   
    Set Global Variable    ${EMAILBOUNCE}
    
Filter Email Bounce By ${option}
    Wait Until Element Is Visible    ${loc_cboEmailBounceSearchBy}
     #Search By and Value
    ${datacol}    Run Keyword if    '${option}' == 'Hostname'    Set Variable    1
    ...    ELSE IF    '${option}' == 'LoginID'    Set Variable    2
    Set Email Bounce     ${datacol}
    Select From List By Label   ${loc_cboEmailBounceSearchBy}    ${EMAILBOUNCE.Search}
    Input Text    ${loc_inputEmailBounceValue}    ${EMAILBOUNCE.Value}    
    Click Element                ${loc_btnEmailBounceSearch}

User able to search for Email Bounce using the search filters available
    Page Should Contain Element    ${loc_tableEmailBounceList}
    Page Should Contain Element    ${loc_emailBounceRowsCount}
    
Populate Email Bounce Account Management
    [Arguments]    ${datacol}  
    Set Email Bounce    ${datacol}
    Content Header Should Be Equal    Email Bounce Account Management
    Fields Required
    Wait Until Element Is Visible    ${loc_inputEmailBounceLoginId}
    #Login ID
    Input Text    ${loc_inputEmailBounceLoginId}    ${EMAILBOUNCE.LoginID}
    #Password
    Input Text    ${loc_inputEmailBouncePassword}    ${EMAILBOUNCE.Password}
    #Pop3 Hostname
    Input Text    ${loc_inputEmailBouncePop3HostName}    ${EMAILBOUNCE.Pop3Hostname}
    #Port
    Input Text    ${loc_inputEmailBouncePort}    ${EMAILBOUNCE.Port}
    #Bounce Back Path
    Input Text    ${loc_inputEmailBounceBounceBackPath}    ${EMAILBOUNCE.BounceBackPath}
    #Description
    Input Text    ${loc_inputEmailBounceDescription}    ${EMAILBOUNCE.Description}
    #Unsub After Soft Bounce Times
    Input Text    ${loc_inputEmailBounceSoftBounce}    ${EMAILBOUNCE.UnsubAfterSoftBounce}
    #Customer Code
    Select From List By Label   ${loc_cboEmailBounceCustomerCode}    ${EMAILBOUNCE.CustomerCode}
    
    User click Save icon
    The Email Bounce Management status should be The account has been successfully added
    User click Back icon
    
The Email Bounce Management status should be ${status}
    Wait Until Element Is Visible    ${loc_emailBounceNotification}
    ${emailBounceStatus}    Get Text    ${loc_emailBounceNotification}
    Should Be Equal    ${status}    ${emailBounceStatus}
    
Create Email Blast with invalid recipients
     Populate Email Blast Details    3
     Approver Approve Email Blast for Email Bounce
     User Start the Email Blast for Email Bounce
     
Fields Required
    User click Save icon
    Page Should Contain Element    ${loc_requiredLoginId}
    Page Should Contain Element    ${loc_requiredPassword}
    Page Should Contain Element    ${loc_requiredHostname}
    Page Should Contain Element    ${loc_requiredPort}
    Page Should Contain Element    ${loc_requiredBounceBack}
    Page Should Contain Element    ${loc_requiredCounter}
    
Validate Email Blast Reports
    User Navigates To Marketing > Communications > Email Blast
    Set Email Blast Details    3
    #Edit the email blast
    Wait Until Element Is Visible    ${loc_tblEmailBlast}    
    Click Element    //td[contains(.,'${EMAILBLAST.BlastName}')]/../td[2]//i[@data-original-title='Edit Item']
    Wait Until Element Is Visible    //*[@id="v-pills-report"]//div[@class='card-body p-b-20']//div[@id='divEmailSentChart']
    ${sample}    Get Text    //*[@id="v-pills-report"]//div[@class='card-body p-b-20']//div[@id='divEmailSentChart']  
    Should Contain    ${sample}    'Not Sent'   
    
Approver Approve Email Blast for Email Bounce
    Sleep    1s
    Set Email Blast Details    3
    Open Mailbox    host=imap.gmail.com    user=${EMAILBLAST.ApproverEmail}   password=${EMAILBLAST.ApproverPassword}
    ${LATEST}    Wait For Email    sender=${EMAILBLAST.SenderEmail}	    status=UNSEEN    timeout=300    
    ${HTML}    Get Links from Email    ${LATEST}
    Switch Window    MAIN    None    Current
    Go To    ${HTML}[1]
    Wait Until Element Is Visible    ${loc_btnApproveEmailBlast}
    Click Element    ${loc_btnApproveEmailBlast}
    Close Mailbox
    
User Start the Email Blast for Email Bounce
    User Navigates To Marketing > Communications > Email Blast
    Set Email Blast Details    3
    #Edit the email blast
    Wait Until Element Is Visible    ${loc_tblEmailBlast}    
    Click Element    //td[contains(.,'${EMAILBLAST.BlastName}')]/../td[2]//i[@data-original-title='Edit Item']
    #Click Start
    Wait Until Element Is Visible    ${loc_btnStartEmailBlast}
    Click Element    ${loc_btnStartEmailBlast}
    #Enter the reason
    Input Text    ${loc_inputStartReason}    for testing only
    #Click Proceed
    Click Element    ${loc_btnProceedStart}
    
User click ${icon} icon on ${emailBounceLogin} on Email Bounce Management
    ${iconLocation}    Set Variable   //td[4][contains(., '${emailBounceLogin}')]/../td[2]//i[@data-original-title='${icon}']
    Wait Until Element Is Visible    ${iconLocation}
    Click Element    ${iconLocation}
    
User can edit information
    Field are editable
    Input Text    ${loc_inputEmailBounceDescription}    ${EMPTY}
    Input Text    ${loc_inputEmailBounceDescription}    Testing Field for Edit
    User click Save icon
    The Email Bounce Management status should be The account has been successfully updated
    User click Back icon
    
Field are editable
    Element Should Be Enabled    ${loc_inputEmailBounceLoginId}
    Element Should Be Enabled    ${loc_inputEmailBouncePassword}
    Element Should Be Enabled    ${loc_inputEmailBouncePop3HostName}
    Element Should Be Enabled    ${loc_inputEmailBouncePort}
    Element Should Be Enabled    ${loc_inputEmailBounceBounceBackPath}
    Element Should Be Enabled    ${loc_inputEmailBounceDescription}
    Element Should Be Enabled    ${loc_inputEmailBounceSoftBounce}
    Element Should Be Enabled    ${loc_cboEmailBounceCustomerCode}
    
User can close and cancel Delete window in Email Bounce
    User close the window
    User click Delete Item icon on Sample@test.com on Email Bounce Management
    Click Element    //*[@id="ctl00_cphBody_uibtnCancel"]
    
User Select Login ID ${selectedItem} in table
    Wait Until Element Is Visible    //td[4][contains(., '${selectedItem}')]/../td[1]/span/input
    Click Element    //td[4][contains(., '${selectedItem}')]/../td[1]/span/input
    User click Delete Checked icon
    
Email Bounce should be deleted
    Content Header Should Be Equal    Delete Confirmation
    Wait Until Element Is Visible    ${loc_btnDeleteEmailBounce}
    #Proceed
    Click Element    ${loc_btnDeleteEmailBounce}
    Wait Until Element Is Visible    ${loc_textAreaError}
    Page Should Contain Element    ${loc_textAreaError}
    #Reason for deletion
    Input Text    ${loc_inputEmailBounceDeleteReason}    For Testing
    Click Element    ${loc_btnDeleteEmailBounce}