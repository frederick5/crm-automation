*** Settings ***
Resource    ../../Global/GlobalSettings.robot

*** Variables ***
${MEMBER}    ${EMPTY}

*** Keywords ***
Set MemberRegister Details
    [Arguments]    ${userinfo}
    ${user}    Run Keyword if    '${userinfo}' == 'valid'    Set Variable    1       
    &{MEMBER}    Create Dictionary
    &{MEMBER}    Get Data From Excel    ${var_dataMember}    MEMBER REGISTRATION    ${user}
    ${MEMBER.DOBDay}    Convert To String    ${MEMBER.DOBDay}
    ${MEMBER.DOBMonth}    Convert To String    ${MEMBER.DOBMonth}
    ${MEMBER.DOBYear}    Convert To String    ${MEMBER.DOBYear}
    ${MEMBER.VehicleIUNo}    Convert To String    ${MEMBER.VehicleIUNo}
    Set Global Variable    &{MEMBER}    
      
User Register New Member Using ${infoType} Info
    Set MemberRegister Details    ${infoType}
    #Enter MemberID
    Input Text    ${loc_MemberID}    ${MEMBER.MemberID}    
    #Select Salutation
    Select From List by Value    ${loc_Salutation}    ${MEMBER.Salutation}
    #Enter Name
    Input Text    ${loc_Name}    ${MEMBER.Name}
    #Enter NRIC 
    Run Keyword If   '${MEMBER.NRIC}' == '${EMPTY}'    Click Button   ${loc_GetTempNRIC}  
    ...    ELSE IF    '${MEMBER.NRIC}' != '${EMPTY}'    Input Text    ${loc_NRIC}    ${MEMBER.NRIC}
    #Enter Passport
    Input Text    ${loc_Passport}    ${MEMBER.Passport}
    #Enter Email
    Input Text    ${loc_Email}    ${MEMBER.Email}
    #Select DOBDay
    Select From List by Value    ${loc_DOBDay}    ${MEMBER.DOBDay}
    #Select DOBMonth
    Select From List by Value    ${loc_DOBMonth}    ${MEMBER.DOBMonth}
    #Select DOBYear
    Select From List by Value    ${loc_DOBYear}    ${MEMBER.DOBYear}
    #ChooseGender
    Run Keyword If    '${MEMBER.Gender}' == 'M'    Click Element    ${loc_GenderM}
    ...    ELSE IF    '${MEMBER.Gender}' == 'F'    Click Element    ${loc_GenderF}
    #Select MaritalStatus
    Select From List by Value    ${loc_MaritalStatus}    ${MEMBER.MaritalStatus}
    #Select Race
    Select From List by Value    ${loc_Race}    ${MEMBER.Race}
    #Select Nationality
    Select From List by Label    ${loc_Nationality}    ${MEMBER.Nationality}
    #Enter ContactNo
    Input Text    ${loc_ContactNo}    ${MEMBER.ContactNo}
    #Enter MobileNoCountryCode
    Run Keyword If    '${loc_MobileNoCountryCode}' == '${EMPTY}'    Input Text    ${loc_MobileNoCountryCode}    ${MEMBER.MobileNoCountryCode}
    #Enter MobileNoAreaCode
    Input Text    ${loc_MobileNoAreaCode}    ${MEMBER.MobileNoAreaCode}
    #Enter MobileNo
    Input Text    ${loc_MobileNo}    ${MEMBER.MobileNo}
    #Enter FaxNo
    Input Text    ${loc_FaxNo}    ${MEMBER.FaxNo}
    #Enter VehicleUNo
    Input Text    ${loc_VehicleIUNo}    ${MEMBER.VehicleIUNo}
    #Enter Vehicle
    Input Text    ${loc_Vehicle}    ${MEMBER.Vehicle}
    #Enter MemberTag
    Input Text    ${loc_MemberTag}    ${MEMBER.MemberTag}
    #Pause
    
    ${unchecked}    Run Keyword And Return Status    Checkbox Should Not Be Selected    ${loc_Cardinformation}
    #Run Keyword If    '${unchecked}'=='True'    Click Element    ${loc_Cardinformation}
    Run Keyword If    '${MEMBER.CardInfo}' == 'Yes' and '${unchecked}'=='True'    Execute Javascript    document.getElementById('${loc_Cardinformation}').click()
    Run Keyword If    '${MEMBER.CardInfo}' == 'Yes'    Card Information
    
    
    

Card Information
    #Select Membership Type
    Select From List by Value    ${loc_MembershipType}    ${MEMBER.MembershipType} 
    #Select Membership Tier
    Set Selenium Implicit Wait    15 
    Select From List by Value    ${loc_MembershipTier}    ${MEMBER.MembershipTier}  
    #Enter Printed Name
    Input Text    ${loc_PrintedName}    ${MEMBER.PrintedName} 
    #Select Membership Status
    Select From List by Value    ${loc_MembershipStatus}    ${MEMBER.MembershipStatus} 
    #Select SignUp Location
    Select From List by Value    ${loc_SignUpLocation}    ${MEMBER.SignUpLocation}      
    #Select PickUp Location
    Select From List by Value    ${loc_PickUpLocation}    ${MEMBER.PickUpLocation}
    #Enter PickUp By
    Input Text    ${loc_PickUpBy}    ${MEMBER.PickUpBy}    
    #Save New User   
    Click Element    ${loc_Save}

A New Member Should be Added
    [Arguments]    ${NewUserSuccessMessage}    ${NewMemberAddedSuccess}
    Wait Until Element is Visible    ${NewUserSuccessMessage}
    ${SuccessMessage}    Get Text    ${NewUserSuccessMessage}
    Should be equal   ${SuccessMessage}    ${NewMemberAddedSuccess}   
    
        
    
    