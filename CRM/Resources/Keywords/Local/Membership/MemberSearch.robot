*** Settings ***
Resource    ../../Global/GlobalSettings.robot

*** Variables ***
${PROFILE}    ${EMPTY}
${CARDINFO}    ${EMPTY}

*** Keywords ***
Set Member Search Details
    [Arguments]    ${datacol}
    &{MEMBER}    Create Dictionary
    &{MEMBER}    Get Data From Excel    ${var_dataMemberSearch}    MEMBERSHIP SEARCH   ${datacol}
    ${MEMBER.CountryCode}    Convert To String    ${MEMBER.CountryCode}
    ${MEMBER.MobileNo}    Convert To String    ${MEMBER.MobileNo}
    Set Global Variable    &{MEMBER}
    
Set Card Info
    [Arguments]    ${datacol}
    &{CARDINFO}    Create Dictionary
    &{CARDINFO}    Get Data From Excel    ${var_datamembersearch}    Card Info   ${datacol}
    Set Global Variable    &{CARDINFO}

Member Search Listing page is displayed
    Content Header Should Be Equal    Member Search Listing
    Page Should Contain Element    ${loc_tableMemberList}      

User clicks Member Search
    Click Element    ${loc_btnSearch}    
    
User clicks Card Search
    Click Element    ${loc_listToggleDropdown}
    Click Element    ${loc_linkCardSearch}
    
User accesses Member Profile
    Sleep    5s
    User navigate to Membership > Membership Search
    Click Element    ${loc_btnSearch}
    Wait Until Element is Visible    ${loc_btnShowActionsMenu}
    Click Link    Mobile No.
    Sleep    10s
    Click Link    Mobile No.
    Sleep    10s
    Click Element    ${loc_btnShowActionsMenu}
    Click Element    ${loc_linkEditMember}
    Sleep    10s
    Click Element    ${loc_linkProfileTab}
    
User updates Gender
    Click Element    ${loc_rdoGenderMale}
    Click Element    ${loc_btnUpdateProfile}
    
User updates Mobile No
    Set Member Search Details    1
    Input Text    ${loc_fieldCountryCode}    ${MEMBER.CountryCode}
    Input Text    ${loc_fieldContactNo}    ${MEMBER.MobileNo}    
    Click Element    ${loc_btnUpdateProfile}
    #Wait Until Page Contains Element    ${loc_textSuccessMessage}   

User accesses Member Cards
    Sleep    5s
    User navigate to Membership > Membership Search
    Click Element    ${loc_btnSearch}
    Wait Until Element is Visible    ${loc_btnShowActionsMenu}
    Click Element    ${loc_btnShowActionsMenu}
    Click Element    ${loc_linkEditMember}
    Sleep    10s
    Click Element    ${loc_linkCardsTab}
    
User updates Card Info
    #Wait Until Page Contains Element    ${loc_btnEditCardInfo}
    Set Member Search Details    2
    Select Frame    ${loc_iframeMembership}
    #Click Element    ${loc_btnEditCardInfo}
    User click Edit Card Info icon    
    Sleep    5s
    Input Text    ${loc_fieldCardPrintedName}    ${MEMBER.PrintedName}    
    Input Text    ${loc_fieldCardPickupBy}    ${MEMBER.PickupBy}
    Click Element    ${loc_btnUpdateCard}   
    
Update success message is displayed
    [Arguments]    ${elementLocator}    ${message}
    Wait Until Element is Visible    ${elementLocator}    10
    ${elementText}    Get Text    ${elementLocator}
    Should be equal   ${elementText}    ${message}