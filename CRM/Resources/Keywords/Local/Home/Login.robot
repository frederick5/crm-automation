*** Settings ***
Resource    ../../Global/GlobalSettings.robot

*** Variables ***
${LOGIN}    ${EMPTY}
*** Keywords ***
Set Login Details
    [Arguments]    ${account}
    ${acctType}    Run Keyword If    '${account}'=='User'    Set Variable    1
    ...    ELSE IF    '${account}'=='Admin'    Set Variable    2
    ...    ELSE    Set Variable    ${account}                  
    &{LOGIN}    Create Dictionary
    &{LOGIN}    Get Data From Excel    ${var_dataLogin}    LOGIN    ${acctType}
    Set Global Variable    &{LOGIN}    
    
User login using ${action} ${accountType} account
    Set Login Details    ${accountType}
    ${LOGIN.Username}    Run Keyword If    '${action}' == 'blank'    Set Variable    ${EMPTY}
    ...    ELSE IF    '${action}' == 'with space'    Set Variable    ${var_usernameWithSpace}
    ...    ELSE    Set Variable    ${LOGIN.Username}        
    ${LOGIN.Password}    Run Keyword If    '${action}' == 'invalid'    Set Variable    ${LOGIN.Password}123
    ...    ELSE IF    '${action}' == 'blank'    Set Variable    ${EMPTY}
    ...    ELSE    Set Variable    ${LOGIN.Password}                    
    #Enter Username
    Input Text    ${loc_inputUsername}    ${LOGIN.Username}    
    #Enter Password
    Input Password    ${loc_inputPassword}    ${LOGIN.Password}     
    #Captcha
    #Pause Execution    Press OK once captcha is manually completed.
    #Click Login    
    Click Element    ${loc_btnLogin}
    
User login using with space username
    Input Text    ${loc_inputUsername}    ${var_usernameWithSpace}
    
User should be able to login to CRM
    #Verify default landing page.
    ${pageTitle}    Get Title
    Should Contain    ${pageTitle}    Dashlets
    Content Header Should Be Equal    Dashlets    
    
    #Verify Username
    ${username}    Get Text    ${loc_txtUserMenuName}
    Click User Avatar
    Should Contain    ${LOGIN.Username}    ${username}
    #Close User Actions
    Click User Avatar
    
User should not be able to login to CRM
    #Verify that user did not proceed to default landing page.
    ${pageTitle}    Get Title
    Should Not Contain    ${pageTitle}    Dashlets
    
Login fields will be highlighted in red
    #Username
    Element Attribute Value Should Be    ${loc_inputUsername}    style    background-color: rgb(255, 199, 199);
    #Password    
    Element Attribute Value Should Be    ${loc_inputPassword}    style    background-color: rgb(255, 199, 199);
    
User Click Forgot Password Link
    Wait Until Element Is Visible    ${loc_linkForgotPassword}
    Click Link    ${loc_linkForgotPassword}
    
Submit Valid User ID
    Set Login Details    7
    Input Text    ${loc_inputUserID}    ${LOGIN.Username}
    Click Element    ${loc_btnSendResetPassword}  

Click Reset Password Link From Email    
    Sleep    1s
    Open Mailbox    host=${var_emailHost}    user=${var_emailApprover}   password=${var_emailApproverPwd}
    ${LATEST}    Wait For Email    sender=${var_emailResetPasswordSender}    status=UNSEEN    timeout=300    
    ${HTML}    Get Matches From Email   ${LATEST}    https://sitcrm.ascentis.com.sg/MatrixCRM/UserResetPassword/.*        
    ${resetPwdURL}    Replace String    ${HTML}[0]    </p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>\r    ${EMPTY}
    Switch Window    MAIN    None    Current
    Go To    ${resetPwdURL}
    
Submit Valid New Password
    Wait Until Element Is Visible    ${loc_inputNewPassword}
    #New Password
    Input Text    ${loc_inputNewPassword}    ${LOGIN.Password}           
    #Confirm Password
    Input Text    ${loc_inputNewConfirmPassword}    ${LOGIN.Password}
    #Submit
    Click Element    ${loc_btnSubmitResetPassword}
    
Reset Password Success Message Is Displayed
    Sleep    2s    
    Page Should Contain Element    //span[contains(., '${var_msgResetPasswordSuccess}')]
        
User can login with new password
    Click Link    here
    Wait Until Element Is Visible    ${loc_inputUsername}
    #Enter Username
    Input Text    ${loc_inputUsername}    ${LOGIN.Username}    
    #Enter Password
    Input Password    ${loc_inputPassword}    ${LOGIN.Password}
    #Captcha
    #Pause Execution    Press OK once captcha is manually completed.
    #Click Login    
    Click Element    ${loc_btnLogin}
    User should be able to login to CRM
    
User login using incorrect password ${number} times
    Set Login Details    1    
    Input Text    ${loc_inputUsername}    ${LOGIN.Username}
    :FOR    ${i}    IN RANGE    ${number}    
    \    Input Password    ${loc_inputPassword}    ${LOGIN.Password}123
    \    Click Element    ${loc_btnLogin}

User account should be locked out
    User login using valid user account  
    