*** Settings ***
Resource    ../../../Global/GlobalSettings.robot

*** Variables ***
${CUSTOMCODE}    ${EMPTY}

*** Keywords ***
Set Custom Code Details
    [Arguments]    ${datacol}          
    &{CUSTOMCODE}    Create Dictionary
    &{CUSTOMCODE}    Get Data From Excel    ${var_dataSettings}    CustomCode    ${datacol} 
    Set Global Variable    &{CUSTOMCODE}
    
Filter Parent Custom Code By ${option}
    Wait Until Element Is Visible    ${loc_cboParentCustomSearchBy}
     #Search By and Value
    ${datacol}    Run Keyword if    '${option}' == 'Custom Code'    Set Variable    1
    ...    ELSE IF    '${option}' == 'Custom Code Name'    Set Variable    2
    ...    ELSE IF    '${option}' == 'Description'    Set Variable    3
    ...    ELSE IF    '${option}' == 'Status'    Set Variable    4
    Set Custom Code Details     ${datacol}
    Select From List By Label   ${loc_cboParentCustomSearchBy}    ${CUSTOMCODE.SearchBy}
    Input Text    ${loc_inputParentCustomValue}    ${CUSTOMCODE.Value}   
    #Search Button 
    Click Element                ${loc_btnParentCustomSearch}
    
Filter Child Custom Code By ${option}
    Wait Until Element Is Visible    ${loc_cboChildCustomSearchBy}
     #Search By and Value
    ${datacol}    Run Keyword if    '${option}' == 'Custom Code'    Set Variable    1
    ...    ELSE IF    '${option}' == 'Custom Code Name'    Set Variable    2
    ...    ELSE IF    '${option}' == 'Description'    Set Variable    3
    ...    ELSE IF    '${option}' == 'Status'    Set Variable    4
    Set Custom Code Details     ${datacol}
    Select From List By Label   ${loc_cboChildCustomSearchBy}    ${CUSTOMCODE.SearchBy}
    Input Text    ${loc_inputChildCustomValue}    ${CUSTOMCODE.ChildValue}   
    #Search Button 
    Click Element                ${loc_btnChildCustomSearch}
    
Populate Custom Code information
    [Arguments]    ${datacol}
    Set Custom Code Details    ${datacol}
    Wait Until Element Is Visible    ${loc_inputCustomCode} 
    #System Code
    Input Text    ${loc_inputCustomCode}    ${CUSTOMCODE.CustomCode}
    #System Code Name
    Input Text    ${loc_inputCustomCodeName}    ${CUSTOMCODE.CustomCodeName}
    #Description
    Input Text     ${loc_inputCustomCodeDesc}    ${CUSTOMCODE.CustomDescription}
    #Order Priority
    Input Text     ${loc_inputCustomCodePriority}    ${CUSTOMCODE.OrderPriority}
    #Save
    Click Element    ${loc_btnCustomCodeSubmit}
    
User able to search for custom code using the search filters available
    Page Should Contain Element    ${loc_customCodeRowsCount}
    Page Should Contain Element    ${loc_tableCustomCodeList}
    
User able to search to Child Custom Code using the search filters available
    Page Should Contain Element    ${loc_childCustomCodeRowsCount}
    Page Should Contain Element    ${loc_tableChildCustomCodeList}
    
Verify Custom Code Listing status should be ${status}
    Wait Until Element Is Visible    ${loc_customCodeMessage}
    ${customCodeStatus}    Get Text   ${loc_customCodeMessage}
    Should Be Equal    ${status}    ${customCodeStatus}  
    
User Select Child Item of custom code
    [Arguments]    ${code}
    User search for custom code    ${code}
    User click Child Item icon of custom code ${code}
    
User search for custom code
    [Arguments]    ${value}
    User click Filter icon
    Wait Until Element Is Visible    ${loc_cboParentCustomSearchBy}
    Select From List By Label   ${loc_cboParentCustomSearchBy}    Custom Code
    Input Text    ${loc_inputParentCustomValue}    ${value}
    #Search Button 
    Click Element                ${loc_btnParentCustomSearch}
    
User click ${icon} icon of custom code ${customCode}
    ${iconLocation}    Set Variable   //td[3][contains(., '${customCode}')]/../td[2]//i[@data-original-title='${icon}']
    Wait Until Element Is Visible    ${iconLocation}
    Click Element    ${iconLocation}
    
User update custom code description of
    [Arguments]    ${value}
    User click Edit Item icon of custom code ${value}
    Then Verify if custom code fields are editable
    #Description
    Input Text     ${loc_inputCustomCodeDesc}     Updating the field for testing purposes
    Click Element                ${loc_btnCustomCodeSubmit}
    
Verify if custom code fields are editable
    Wait Until Element Is Visible    ${loc_inputCustomCodeName}
    Element Should Be Enabled    ${loc_inputCustomCodeName}
    Element Should Be Enabled    ${loc_inputCustomCodeDesc}
    Element Should Be Enabled    ${loc_inputCustomCodePriority}
    Element Should Be Enabled    ${loc_chckBoxCustomActive}