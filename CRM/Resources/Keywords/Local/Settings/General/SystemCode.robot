*** Settings ***
Resource    ../../../Global/GlobalSettings.robot

*** Variables ***
${SYSTEMCODE}    ${EMPTY}

*** Keywords ***
Set System Code Details
    [Arguments]    ${datacol}          
    &{SYSTEMCODE}    Create Dictionary
    &{SYSTEMCODE}    Get Data From Excel    ${var_dataSettings}    SystemCode    ${datacol} 
    Set Global Variable    &{SYSTEMCODE}
    
Populate System Code information
    [Arguments]    ${datacol}
    Set System Code Details    ${datacol}
    Wait Until Element Is Visible    ${loc_inputSystemCode} 
    #System Code
    Input Text    ${loc_inputSystemCode}    ${SYSTEMCODE.SystemCode}
    #System Code Name
    Input Text    ${loc_inputSystemCodeName}    ${SYSTEMCODE.SystemCodeName}
    #Description
    Input Text     ${loc_inputSysteCodeDesc}    ${SYSTEMCODE.SystemDescription}
    #Order Priority
    Input Text     ${loc_inputSystemCodePriority}    ${SYSTEMCODE.OrderPriority}
    #Save
    Click Element    ${loc_btnSystemCodeSubmit}
    
Verify System Code Listing status should be ${status}
    Wait Until Element Is Visible    ${loc_systemCodeMessage}
    ${systemCodeStatus}    Get Text    ${loc_systemCodeMessage}
    Should Be Equal    ${status}    ${systemCodeStatus}   
    
Filter Parent System Code By ${option}
    Wait Until Element Is Visible    ${loc_cboParentSystemSearchBy}
     #Search By and Value
    ${datacol}    Run Keyword if    '${option}' == 'System Code'    Set Variable    1
    ...    ELSE IF    '${option}' == 'System Code Name'    Set Variable    2
    ...    ELSE IF    '${option}' == 'Description'    Set Variable    3
    ...    ELSE IF    '${option}' == 'Status'    Set Variable    4
    Set System Code Details     ${datacol}
    Select From List By Label   ${loc_cboParentSystemSearchBy}    ${SYSTEMCODE.SearchBy}
    Input Text    ${loc_inputParentSystemValue}    ${SYSTEMCODE.Value}   
    #Search Button 
    Click Element                ${loc_btnParentSystemSearch}
    
Filter Child System Code By ${option}
    Wait Until Element Is Visible    ${loc_cboChildSystemSearchBy}
     #Search By and Value
    ${datacol}    Run Keyword if    '${option}' == 'System Code'    Set Variable    1
    ...    ELSE IF    '${option}' == 'System Code Name'    Set Variable    2
    ...    ELSE IF    '${option}' == 'Description'    Set Variable    3
    ...    ELSE IF    '${option}' == 'Status'    Set Variable    4
    Set System Code Details     ${datacol}
    Select From List By Label   ${loc_cboChildSystemSearchBy}    ${SYSTEMCODE.SearchBy}
    Input Text    ${loc_inputChildSystemValue}    ${SYSTEMCODE.ChildValue}   
    #Search Button 
    Click Element                ${loc_btnChildSystemSearch}
    
User able to search for a System Code using the search filters available
    Page Should Contain Element    ${loc_systemCodeRowsCount}
    Page Should Contain Element    ${loc_tableSystemCodeList}
    
User able to search to Child System Code using the search filters available
    Page Should Contain Element    ${loc_childSystemCodeRowsCount}
    Page Should Contain Element    ${loc_tableChildSystemCodeList}
    
User search for system code
    [Arguments]    ${value}
    User click Filter icon
    Wait Until Element Is Visible    ${loc_cboParentSystemSearchBy}
    Select From List By Label   ${loc_cboParentSystemSearchBy}    System Code
    Input Text    ${loc_inputParentSystemValue}    ${value}
    #Search Button 
    Click Element                ${loc_btnParentSystemSearch}
       
User click ${icon} icon of system code ${systemCode}
    ${iconLocation}    Set Variable   //td[3][contains(., '${systemCode}')]/../td[2]//i[@data-original-title='${icon}']
    Wait Until Element Is Visible    ${iconLocation}
    Click Element    ${iconLocation}  
    
Verify if fields are editable
    Wait Until Element Is Visible    ${loc_inputSystemCodeName}
    Element Should Be Enabled    ${loc_inputSystemCodeName}
    Element Should Be Enabled    ${loc_inputSysteCodeDesc}
    Element Should Be Enabled    ${loc_inputSystemCodePriority}
    Element Should Be Enabled    ${loc_chckBoxSystemActive}
    
User update system code description of
    [Arguments]    ${value}
    User click Edit Item icon of system code ${value}
    Then Verify if fields are editable
    #Description
    Input Text     ${loc_inputSysteCodeDesc}     Updating the field for testing purposes
    Click Element                ${loc_btnSystemCodeSubmit}
    
User select system code to delete
    [Arguments]    ${code}
    Wait Until Element Is Visible    //td[3][contains(., '${code}')]/../td[1]/span/input
    Click Element    //td[3][contains(., '${code}')]/../td[1]/span/input
    User click Delete Checked icon
    
User can close and cancel Delete window in System Code Listing   
    User close the window
    User click Delete Checked icon
    Wait Until Element Is Visible    ${loc_btnSystemCodeDeleteCancel}
    Click Element    ${loc_btnSystemCodeDeleteCancel}
    
User can perform Delete on System Code
    User can close and cancel Delete window in System Code Listing
    User click Delete Checked icon
    User can successfully delete an item
    
User can successfully delete an item
    Wait Until Element Is Visible    ${loc_btnSystemCodeDeleteProceed}
    #Proceed
    Click Element    ${loc_btnSystemCodeDeleteProceed}
    Wait Until Element Is Visible    ${loc_textAreaError}
    Page Should Contain Element    ${loc_textAreaError}
    #Reason for deletion
    Input Text   ${loc_inputSystemCodeDeleteReason}    For Testing Only
    Click Element    ${loc_btnSystemCodeDeleteProceed}
    
User Select Child Item of system code
    [Arguments]    ${code}
    User search for system code    ${code}
    User click Child Item icon of system code ${code}
    
Populate existing child code
    [Arguments]    ${existingCode}
    Wait Until Element Is Visible    ${loc_inputSystemCode} 
    #System Code
    Input Text    ${loc_inputSystemCode}    ${existingCode}
    Press Keys        ${loc_inputSystemCode}    TAB
    #Warning Icon
    Wait Until Element Is Enabled    ${loc_inputSystemCodeWarning}
    Click Element    ${loc_inputSystemCodeWarning}
    
Validate that existing data will populate then save
    [Arguments]    ${datacol}
    sleep    5
    ${systemCodeValue}    Get Value    ${loc_inputSystemCode}
    ${systemCodeNameValue}    Get Value    ${loc_inputSystemCodeName}
    ${descriptionValue}    Get Text    ${loc_inputSysteCodeDesc}
    ${orderPriorityValue}    Get Value    ${loc_inputSystemCodePriority}
    
    Set System Code Details    ${datacol} 
    Should Be Equal    ${SYSTEMCODE.SystemCode}    ${systemCodeValue}
    Should Be Equal    ${SYSTEMCODE.SystemCodeName}    ${systemCodeNameValue}
    Should Be Equal    ${SYSTEMCODE.SystemDescription}    ${descriptionValue}
    Should Be Equal    ${SYSTEMCODE.OrderPriority}    ${orderPriorityValue}
    
    
    Click Element    ${loc_btnSystemCodeSubmit}
    
Validate that existing child system code name will be update
    [Arguments]    ${parentSystemCodeName}
    Wait Until Element Is Visible    //*[@id="ctl00_cphMainHeader_uilblsyscodename"]
    ${headerValue}    Get Text    //*[@id="ctl00_cphMainHeader_uilblsyscodename"]
    Should Be Equal    ${parentSystemCodeName}    ${headerValue}
    Verify System Code Listing status should be The system child code has been successfully updated.
    