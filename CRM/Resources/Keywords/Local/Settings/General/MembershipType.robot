*** Settings ***
Resource    ../../../Global/GlobalSettings.robot

*** Variables ***
${MEMBERSHIPTYPE}    ${EMPTY}

*** Keywords ***
Set MembershipType Details
    [Arguments]    ${datacol}          
    &{MEMBERSHIPTYPE}    Create Dictionary
    &{MEMBERSHIPTYPE}    Get Data From Excel    ${var_dataSettings}    MembershipType    ${datacol} 
    Set Global Variable    &{MEMBERSHIPTYPE}
    
Filter Membership Type By ${option}
    Wait Until Element Is Visible    ${loc_cboMembershipTypeSearchBy}
     #Search By and Value
    ${datacol}    Run Keyword if    '${option}' == 'Code'    Set Variable    1
    ...    ELSE IF    '${option}' == 'Name'    Set Variable    2
    Set MembershipType Details     ${datacol}
    Select From List By Label   ${loc_cboMembershipTypeSearchBy}    ${MEMBERSHIPTYPE.SearchBy}
    Input Text    ${loc_inputMembershipTypeValue}    ${MEMBERSHIPTYPE.Value}   
    #Search Button 
    Click Element                ${loc_btnMembershipTypeSearch}
    
User able to search Membership Type using the search filters available
    Page Should Contain Element    ${loc_membershipTypeRowsCount}
    Page Should Contain Element    ${loc_tableMembershipTypeList}
    
Populate Membership Type Fields
    [Arguments]    ${datacol}
    Set MembershipType Details    ${datacol}
    Wait Until Element Is Visible    ${loc_inputMembershipTypeCode}
    #Code
    Input Text    ${loc_inputMembershipTypeCode}    ${MEMBERSHIPTYPE.Code}
    #Name
    Input Text    ${loc_inputMembershipTypeName}    ${MEMBERSHIPTYPE.Name}
    #Format
    Input Text    ${loc_inputMembershipTypeFormat}    ${MEMBERSHIPTYPE.Format}
    #PrintedNameLength
    Input Text    ${loc_inputMembershipTypePrintedNameLength}    ${MEMBERSHIPTYPE.PrintedNameLength}
    #Entity
    Select From List By Label    ${loc_cboMembershipTypeEntity}    ${MEMBERSHIPTYPE.Entity}
    #Program Type
    Select From List By Label    ${loc_cboMembershipTypeProgramType}    ${MEMBERSHIPTYPE.ProgramType}
    #Rounding Options
    Select From List By Label    ${loc_cboMembershipTypeRoundingOptions}    ${MEMBERSHIPTYPE.RoundingOptions}
    #Tier Code
    Input Text    ${loc_inputMembershipTypeCodeTier}    ${MEMBERSHIPTYPE.CodeTier}
    #Tier Name
    Input Text    ${loc_inputMembershipTypeNameTier}    ${MEMBERSHIPTYPE.NameTier}
    #Cycle Duration Type
    Select From List By Label    ${loc_cboMembershipTypeCycleDurationType}    ${MEMBERSHIPTYPE.CycleDurationType}
    #Cycle Duration Length
    Input Text    ${loc_inputMembershipTypeCycleDurationLength}    ${MEMBERSHIPTYPE.CycleDurationLength}   
    #Add Tier
    Click Element    ${loc_btnMembershipTypeAddTier}   
    #Save
    User click Save icon
    
Verify Membership Type Listing status should be ${status}
    Wait Until Element Is Visible    ${loc_membershipTypeMessage}
    ${membershipTypeStatus}    Get Text    ${loc_membershipTypeMessage}
    Should Be Equal    ${status}    ${membershipTypeStatus}  
    
User select Membership Type
    [Arguments]    ${code}
    Wait Until Element Is Visible    //td[3][contains(., '${code}')]/../td[1]/span/input
    Click Element    //td[3][contains(., '${code}')]/../td[1]/span/input
    
User click ${icon} icon of membership type ${membershipType}
    ${iconLocation}    Set Variable   //td[3][contains(., '${membershipType}')]/../td[2]//i[@data-original-title='${icon}']
    Wait Until Element Is Visible    ${iconLocation}
    Click Element    ${iconLocation} 
    
User select membership type code and edit
    [Arguments]    ${membershipType}
    User click Filter icon
    Wait Until Element Is Visible    ${loc_cboMembershipTypeSearchBy}
    Select From List By Label   ${loc_cboMembershipTypeSearchBy}    Code
    Input Text    ${loc_inputMembershipTypeValue}    ${membershipType}
    Click Element                ${loc_btnMembershipTypeSearch}
    
    User click Edit Item icon of membership type ${membershipType}
    Wait Until Element Is Visible    ${loc_inputMembershipTypeName}
    #Name
    Input Text    ${loc_inputMembershipTypeName}    Auto Update Name
    User click Save icon
    
User select membership type code and delete
    [Arguments]    ${membershipType}
    User click Filter icon
    Wait Until Element Is Visible    ${loc_cboMembershipTypeSearchBy}
    Select From List By Label   ${loc_cboMembershipTypeSearchBy}    Code
    Input Text    ${loc_inputMembershipTypeValue}    ${membershipType}
    Click Element                ${loc_btnMembershipTypeSearch}
    User select Membership Type   ${membershipType}
    User click Delete Checked icon
    Then User can close and cancel Delete window in Membership Type Listing
    User click Delete Item icon of membership type ${membershipType}
    User can successfully delete membership type
    
User can close and cancel Delete window in Membership Type Listing   
    User close the window
    User click Delete Checked icon
    Wait Until Element Is Visible    ${loc_btnMembershipTypeDeleteCancel}
    Click Element    ${loc_btnMembershipTypeDeleteCancel}
    
User can successfully delete membership type
    Wait Until Element Is Visible    ${loc_btnoMembershipTypeDeleteProceed}
    #Proceed
    Click Element    ${loc_btnoMembershipTypeDeleteProceed}
    Wait Until Element Is Visible    ${loc_textAreaError}
    Page Should Contain Element    ${loc_textAreaError}
    #Reason for deletion
    Input Text   ${loc_inputMembershipTypeDeleteReason}    For Testing Only
    Click Element   ${loc_btnoMembershipTypeDeleteProceed}
