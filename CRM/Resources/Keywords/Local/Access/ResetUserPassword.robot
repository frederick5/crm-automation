*** Settings ***
Resource    ../../Global/GlobalSettings.robot

*** Variables ***
${ResetUserPassword}    ${EMPTY}

*** Keywords ***

Set User Reset Password
    [Arguments]    ${userpassword}
    ${PassReset}    Run Keyword If    '${userpassword}'=='valid account'    Set Variable    1
    ...    ELSE IF    '${userpassword}'=='new password'    Set Variable    2
    &{ResetUserPassword}    Create Dictionary
    &{ResetUserPassword}    Get Data From Excel    ${var_dataResetUserPassword}    ResetUserPassword    ${PassReset}
    Set Global Variable    &{ResetUserPassword}    
    
Set Member Search Details
    [Arguments]    ${SearchMem}
    ${MemSearch}    Run Keyword If    '${SearchMem}'=='member'    Set Variable    3
    &{ResetUserPassword}    Create Dictionary
    &{ResetUserPassword}    Get Data From Excel    ${var_dataResetUserPassword}    ResetUserPassword   ${MemSearch}
    Set Global Variable    &{ResetUserPassword}
    
User sign in using ${passwordreset}
    Set User Reset Password    ${passwordreset}
    Input Text    ${loc_inputUsername}    ${ResetUserPassword.UserName}    
    Input Text    ${loc_inputPassword}    ${ResetUserPassword.Password}
    Click Element    ${loc_btnLogin}
    
User reset password
    Input Text    ${loc_txtCurrentPassword}    ${ResetUserPassword.Password} 
    Input Text    ${loc_txtNewPassword}    ${ResetUserPassword.NewPassword}
    Input Text    ${loc_txtConfirmPassword}    ${ResetUserPassword.ConfirmPassword}
    
The user new password has been reset
    [Arguments]    ${AddGroupSuccessMsg}    ${AddGroupSuccess}    
    Wait Until Element is Visible    ${AddGroupSuccessMsg}
    ${AddGroupMsg}    Get Text    ${AddGroupSuccessMsg}
    Should be equal   ${AddGroupMsg}    ${AddGroupSuccess}
    
User should be able to login using new password
    ${pageTitle}    Get Title
    Should Contain    ${pageTitle}    Dashlets
    Content Header Should Be Equal    Dashlets    
    ${username}    Get Text    ${loc_txtUserMenuName}
    Click User Avatar
    Should Contain    ${ResetUserPassword.UserName}    ${username}

User search for the ${membername}
    Set Member Search Details    ${membername}
    Set Selenium Implicit Wait    5
    Select From List by Value    ${loc_listSearchMember}    ${ResetUserPassword.SearchList}
    Input Text    ${loc_txtMemberName}    ${ResetUserPassword.MemberName}
    Click Element    ${loc_btnMemSearch}
    User click Show Actions Menu icon
    #Click Element    ${loc_listSendResetPwordEmail}
    Click Element    //td[4][contains(., '${ResetUserPassword.MemberName}')]/..//td[2]//span[@id='ctl00_cphBody_uigvList_uigvItemList_ctl02_uilblSendResetLink']  
    

User click proceed to send reset password link
    Click Element    ${loc_btnSendResetProceed}
    Wait Until Element Is Visible    ${loc_btnConfirmClose}
    Click Element    ${loc_btnConfirmClose}
    

User Receive Reset Password Link Confirmation
    Sleep    1s
    Open Mailbox    host=imap.gmail.com    user=ian@ascentis.com.sg   password=Madi&Chelle01
    ${LATEST}    Wait For Email    sender=noreply@ascentis.com.sg    status=UNSEEN    timeout=300
    ${ALL_LINKS}    Get Links from Email    ${LATEST}
    SeleniumLibrary.Go To    ${ALL_LINKS}[0]


