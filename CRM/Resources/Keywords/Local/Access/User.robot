*** Settings ***
Resource    ../../Global/GlobalSettings.robot

*** Variables ***
${USERS}    ${EMPTY}


*** Keywords ***
Set User Details
    [Arguments]    ${user}
    ${useraccnt}    Run Keyword If    '${user}' == 'add new'    Set Variable    1
    ...    ELSE IF    '${user}' == 'edit'    Set Variable    2
    ...    ELSE IF    '${user}' == 'search filter of'    Set Variable    3            
    ...    ELSE IF    '${user}' == 'delete'    Set Variable    4
    &{USERS}    Create Dictionary
    &{USERS}    Get Data From Excel    ${var_dataUsers}    USERS    ${useraccnt}
    ${USERS.Mobile}    Convert To String    ${USERS.Mobile}
    Set Global Variable    &{USERS}
    
User ${useraction} user
    Set User Details    ${useraction}
    Set Selenium Implicit Wait    5
        
Select a user to edit
    Click Element    //td[5][contains(., '${USERS.Email}')]/..//td[2]//span[@class='ui-icon ui-icon-pencil']
    
    
User fill up the fields
    User click Add New User icon
    Set Selenium Implicit Wait    5
    Input Text    ${loc_inputUserID}    ${USERS.UserID}    
    Input Text    ${loc_inputUserPassword}    ${USERS.Password}
    Input Text    ${loc_inputUserConfirmPassword}    ${USERS.ConfirmPassword}
    Input Text    ${loc_inputUserName}    ${USERS.UserName}
    Input Text    ${loc_inputUserEmail}    ${USERS.Email}
    Input Text    ${loc_inputUserMobile}    ${USERS.Mobile}
    Select From List by Value    ${loc_listUserGroup}    ${USERS.UserGroup}
    Select From List by Label    ${loc_listUserAccessLevel}    ${USERS.Level}  
    Set Selenium Implicit Wait    5
    
A New User Should be Inserted
    [Arguments]    ${AddNewUserSuccessMsg}    ${AddNewUserSuccess}    
    Wait Until Element is Visible    ${AddNewUserSuccessMsg}
    ${UserSuccessMsg}    Get Text    ${AddNewUserSuccessMsg}
    Should be equal   ${UserSuccessMsg}    ${AddNewUserSuccess}   
    
   
User update the fields
    Input Text    ${loc_inputUserName}    ${USERS.UserName}
    Input Text    ${loc_inputUserEmail}    ${USERS.Email}
    Input Text    ${loc_inputUserMobile}    ${USERS.Mobile}
    Select From List by Label    ${loc_listUserAccessLevel}    ${USERS.Level} 
    Set Selenium Implicit Wait    5
    
Edited User Should be Updated
    [Arguments]    ${EditUserSuccessMsg}    ${EditUserSuccess}    
    Wait Until Element is Visible    ${EditUserSuccessMsg}
    ${EditSuccessMsg}    Get Text    ${EditUserSuccessMsg}
    Should be equal   ${EditSuccessMsg}    ${EditUserSuccess} 
   

Input value for ${filtervalue}
        Run Keyword If    '${filtervalue}' == 'User ID'    Input Text    ${loc_inputFilterValue}    ${USERS.UserID}
        Run Keyword If    '${filtervalue}' == 'Email'    Input Text    ${loc_inputFilterValue}    ${USERS.Email}
        Run Keyword If    '${filtervalue}' == 'User Group'    Input Text    ${loc_inputFilterValue}    ${USERS.UserGroup}
        Click Element    ${loc_btnFilterSearch}

    
User clicks Delete Check without selecting from the list
    User click Delete Checked icon
    Handle Alert    Accept

User proceed on deleting
    Set Selenium Implicit Wait    5
    Click Element    ${loc_chkUserList}
    User click Delete Checked icon
    Input Text    ${loc_inputDeleteReason}    ${USERS.Reason}
    Click Element    ${loc_btnDeleteProceed}
    
User Should be Deleted
    [Arguments]    ${DeleteUserSuccessMsg}    ${DeleteUserSuccess}    
    Wait Until Element is Visible    ${DeleteUserSuccessMsg}
    ${DeleteSuccessMsg}    Get Text    ${DeleteUserSuccessMsg}
    Should be equal   ${DeleteSuccessMsg}    ${DeleteUserSuccess}
    
