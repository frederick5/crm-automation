*** Settings ***
Resource    ../../Global/GlobalSettings.robot

*** Variables ***
${USERGROUP}    ${EMPTY}

*** Keywords ***

Set User Group Details
    [Arguments]    ${usergroup}
    ${groupaccnt}    Run Keyword If    '${usergroup}' == 'add new'    Set Variable    1
    ...    ELSE IF    '${usergroup}' == 'search filter by'    Set Variable    2
    ...    ELSE IF    '${usergroup}' == 'edit'    Set Variable    3
    ...    ELSE IF    '${usergroup}' == 'assign to'    Set Variable    4
    ...    ELSE IF    '${usergroup}' == 'duplicate'    Set Variable    5
    ...    ELSE IF    '${usergroup}' == 'customize fields of'    Set Variable    6
    ...    ELSE IF    '${usergroup}' == 'edit customize fields of'    Set Variable    7
    ...    ELSE IF    '${usergroup}' == 'delete'    Set Variable    8
    ...    ELSE IF    '${usergroup}' == 'add new field customizing'    Set Variable    9
    &{USERGROUP}    Create Dictionary
    &{USERGROUP}    Get Data From Excel    ${var_dataUserGroup}    USERGROUP    ${groupaccnt}
    #${USERGROUP.Mobile}    Convert To String    ${USERGROUP.Mobile}
    Set Global Variable    &{USERGROUP}
    

User ${useraction} user group
    Set User Group Details    ${useraction}
    Set Selenium Implicit Wait    5
   
Check User Group
    Click Element    ${loc_chkUserList}
    
Select user group to edit
    Click Element    //td[4][contains(., '${USERGROUP.UserGroup}')]/..//td[2]//span[@class='ui-icon ui-icon-pencil']
       
User clicks on edit field customizing
    Click Element    ${loc_btnEditFieldCustom}
    
User uncheck all
    Click element    ${loc_chkUncheckAll}
    

User complete the fields
    User click Add New User Group icon    
    Set Selenium Implicit Wait    5    
    Input Text    ${loc_txtUserGroup}    ${USERGROUP.UserGroup}
    Input Text    ${loc_txtUserDescription}    ${USERGROUP.Description}
    

The New Group is Added
    [Arguments]    ${AddGroupSuccessMsg}    ${AddGroupSuccess}    
    Wait Until Element is Visible    ${AddGroupSuccessMsg}
    ${AddGroupMsg}    Get Text    ${AddGroupSuccessMsg}
    Should be equal   ${AddGroupMsg}    ${AddGroupSuccess}  
    

Input value by ${filtervalue}
        Run Keyword If    '${filtervalue}' == 'User Group'    Input Text    ${loc_inputFilterValue}    ${USERGROUP.UserGroup}
        Run Keyword If    '${filtervalue}' == 'Description'    Input Text    ${loc_inputFilterValue}    ${USERGROUP.Description}
        Click Element    ${loc_btnFilterSearch}


User edit fields
    Input Text    ${loc_txtUserDescription}    ${USERGROUP.Description}
    Select From List by Value    ${loc_listAssignUser}    ${USERGROUP.AssignUsertoGroup}  
    Set Selenium Implicit Wait    5
     

Edited UserGroup Should be Updated
    [Arguments]    ${EditUserSuccessMsg}    ${EditUserSuccess}    
    Wait Until Element is Visible    ${EditUserSuccessMsg}
    ${EditSuccessMsg}    Get Text    ${EditUserSuccessMsg}
    Should be equal   ${EditSuccessMsg}    ${EditUserSuccess}  


User add user to the group
    Select From List by Value    ${loc_listAssignUser}    ${USERGROUP.AssignUsertoGroup}

The User is assigned to a Group
    [Arguments]    ${AssignUserSuccessMsg}    ${AssignedSuccess}    
    Wait Until Element is Visible    ${AssignUserSuccessMsg}
    ${AssignUserMsg}    Get Text    ${AssignUserSuccessMsg}
    Should be equal   ${AssignUserMsg}    ${AssignedSuccess}   

User select to duplicate
    Click Element    ${loc_btnDuplicateGroup}
    
User proceed on duplicating the Group
    Input Text    ${loc_txtUserGroup}    ${USERGROUP.UserGroupDup}
    Input Text    ${loc_txtUserDescription}    ${USERGROUP.Description}
    
The Duplicated Group is Added
    [Arguments]    ${DuplicateGroupSuccessMsg}    ${DuplicateGroupSuccess}    
    Wait Until Element is Visible    ${DuplicateGroupSuccessMsg}
    ${DuplicateGroupMsg}    Get Text    ${DuplicateGroupSuccessMsg}
    Should be equal   ${DuplicateGroupMsg}    ${DuplicateGroupSuccess} 
    

User proceed on duplicate field customizing
    Click Element    ${loc_btnDuplicateField}
    Select From List by Value    ${loc_listCustomUserGroup}    ${USERGROUP.UserGroupDup}
    Set Selenium Implicit Wait    5    
    Select From List by Label    ${loc_listCustomResgistration}    ${USERGROUP.RegistrationFormType}
    
    
The Duplicated Field is Added
    [Arguments]    ${DupFieldCustomSuccessMsg}    ${DupFieldCustomSuccess}    
    Wait Until Element is Visible    ${DupFieldCustomSuccessMsg}
    ${DupFieldCustomMsg}    Get Text    ${DupFieldCustomSuccessMsg}
    Should be equal   ${DupFieldCustomMsg}    ${DupFieldCustomSuccess} 
    
The User Group has been updated
    [Arguments]    ${EditFieldCustomSuccessMsg}    ${EditFieldCustomSuccess}    
    Wait Until Element is Visible    ${EditFieldCustomSuccessMsg}
    ${EditFieldCustomMsg}    Get Text    ${EditFieldCustomSuccessMsg}
    Should be equal   ${EditFieldCustomMsg}    ${EditFieldCustomSuccess} 


User clicks Delete Check User Group Listing
    User click Delete Checked icon
    Handle Alert    Accept

User selects group to delete
    Set Selenium Implicit Wait    5    
    User click Delete Checked icon
    Input Text    ${loc_inputDeleteReason}    ${USERGROUP.Reason}
    #Click Element    ${loc_btnDeleteCancel}
    Sleep    2s
    Click Element    ${loc_btnDeleteGrpProceed}    
    
User Group Should be Deleted
    [Arguments]    ${DeleteUserGroupSuccessMsg}    ${DeleteUserGroupSuccess}    
    Wait Until Element is Visible    ${DeleteUserGroupSuccessMsg}
    ${DeleteGroupSuccessMsg}    Get Text    ${DeleteUserGroupSuccessMsg}
    Should be equal   ${DeleteGroupSuccessMsg}    ${DeleteUserGroupSuccess}
    
User click add new field customizing
    User click Add New Fields Customizing icon    
    Set Selenium Implicit Wait    5    
    
User proceed on field customizing
    Click Element    ${loc_btnDuplicateField}
    Select From List by Value    ${loc_listCustomUserGroup}    ${USERGROUP.UserGroup}
    Set Selenium Implicit Wait    5    
    Select From List by Label    ${loc_listCustomResgistration}    ${USERGROUP.RegistrationFormType}
    Set Selenium Implicit Wait    5 
    Run Keyword If    '${USERGROUP.JoinDate}' == 'No'    Click Element    ${loc_chkFieldJoinDate}
    #Input Text    ${loc_txtFieldJoinDate}    ${USERGROUP.JoinDateTxt}
    Run Keyword If    '${USERGROUP.MemberID}' == 'No'    Click Element    ${loc_chkFieldMemberID}
    Run Keyword If    '${USERGROUP.MemberIDRequired}' == 'Yes'    Click Element    ${loc_chkFieldMemberIDRequired}
    Handle Alert    Accept
    Run Keyword If    '${USERGROUP.VIP}' == 'No'    Click Element    ${loc_chkFieldVIP}
    Run Keyword If    '${USERGROUP.Salutation}' == 'No'    Click Element    ${loc_chkFieldSalutation}
    Run Keyword If    '${USERGROUP.SalutationRequired}' == 'Yes'    Click Element    ${loc_chkFieldSalutationRequired}
    Run Keyword If    '${USERGROUP.Name}' == 'No'    Click Element    ${loc_chkFieldName}
    Run Keyword If    '${USERGROUP.NameRequired}' == 'Yes'    Click Element    ${loc_chkFieldNameRequired}
    Run Keyword If    '${USERGROUP.NRIC}' == 'No'    Click Element    ${loc_chkFieldNRIC}
    Run Keyword If    '${USERGROUP.NRICRequired}' == 'Yes'    Click Element    ${loc_chkFieldNRICRequired}
    Run Keyword If    '${USERGROUP.Passport}' == 'No'    Click Element    ${loc_chkFieldPassport}
    Run Keyword If    '${USERGROUP.PassportRequired}' == 'Yes'    Click Element    ${loc_chkFieldPassportRequired}
    Run Keyword If    '${USERGROUP.Email}' == 'No'    Click Element    ${loc_chkFieldEmail}
    Run Keyword If    '${USERGROUP.EmailRequired}' == 'Yes'    Click Element    ${loc_chkFieldEmailRequired}
    Run Keyword If    '${USERGROUP.DOB}' == 'No'    Click Element    ${loc_chkFieldDOB}
    Run Keyword If    '${USERGROUP.DOBRequired}' == 'Yes'    Click Element    ${loc_chkFieldDOBRequired}
    Run Keyword If    '${USERGROUP.Gender}' == 'No'    Click Element    ${loc_chkFieldGender}
    Run Keyword If    '${USERGROUP.GenderRequired}' == 'Yes'    Click Element    ${loc_chkFieldGenderRequired}
    Run Keyword If    '${USERGROUP.MaritalStatus}' == 'No'    Click Element    ${loc_chkFieldMaritalStatus}
    Run Keyword If    '${USERGROUP.MaritalStatusRequired}' == 'Yes'    Click Element    ${loc_chkFieldMaritalStatusRequired}
    Run Keyword If    '${USERGROUP.Race}' == 'No'    Click Element    ${loc_chkFieldRace}
    Run Keyword If    '${USERGROUP.RaceRequired}' == 'Yes'    Click Element    ${loc_chkFieldRaceRequired}   
    Run Keyword If    '${USERGROUP.Nationality}' == 'No'    Click Element    ${loc_chkFieldNationality}
    Run Keyword If    '${USERGROUP.NationalityRequired}' == 'Yes'    Click Element    ${loc_chkFieldNationalityRequired} 
    Run Keyword If    '${USERGROUP.Country}' == 'No'    Click Element    ${loc_chkFieldCountry}
    Run Keyword If    '${USERGROUP.CountryRequired}' == 'Yes'    Click Element    ${loc_chkFieldCountryRequired}
    Run Keyword If    '${USERGROUP.Address}' == 'No'    Click Element    ${loc_chkFieldAddress}
    Run Keyword If    '${USERGROUP.AddressRequired}' == 'Yes'    Click Element    ${loc_chkFieldAddressRequired}
    Run Keyword If    '${USERGROUP.Address1}' == 'No'    Click Element    ${loc_chkFieldAddress1}
    Run Keyword If    '${USERGROUP.Address1Required}' == 'Yes'    Click Element    ${loc_chkFieldAddress1Required}    
    Run Keyword If    '${USERGROUP.ContactNo}' == 'No'    Click Element    ${loc_chkFieldContactNo}
    Run Keyword If    '${USERGROUP.ContactNoRequired}' == 'Yes'    Click Element    ${loc_chkFieldContactNoRequired}
    Run Keyword If    '${USERGROUP.MobileNo}' == 'No'    Click Element    ${loc_chkFieldMobileNo}
    Run Keyword If    '${USERGROUP.MobileNoRequired}' == 'Yes'    Click Element    ${loc_chkFieldMobileNoRequired}
    Run Keyword If    '${USERGROUP.FaxNo}' == 'No'    Click Element    ${loc_chkFieldFaxNo}
    Run Keyword If    '${USERGROUP.FaxNoRequired}' == 'Yes'    Click Element    ${loc_chkFieldFaxNoRequired}
    Run Keyword If    '${USERGROUP.VehicleIU}' == 'No'    Click Element    ${loc_chkFieldVehicleIU}
    Run Keyword If    '${USERGROUP.VehicleIURequired}' == 'Yes'    Click Element    ${loc_chkFieldVehicleIURequired}
    Run Keyword If    '${USERGROUP.Vehicle}' == 'No'    Click Element    ${loc_chkFieldVehicle}
    Run Keyword If    '${USERGROUP.VehicleRequired}' == 'Yes'    Click Element    ${loc_chkFieldVehicleRequired}
    Run Keyword If    '${USERGROUP.TAG}' == 'No'    Click Element    ${loc_chkFieldTAG}
    Run Keyword If    '${USERGROUP.TAGRequired}' == 'Yes'    Click Element    ${loc_chkFieldTAGRequired}
    Run Keyword If    '${USERGROUP.Occupation}' == 'No'    Click Element    ${loc_chkFieldOccupation}
    Run Keyword If    '${USERGROUP.OccupationRequired}' == 'Yes'    Click Element    ${loc_chkFieldOccupationRequired}
    Run Keyword If    '${USERGROUP.IncomeGroup}' == 'No'    Click Element    ${loc_chkFieldIncomeGroup}
    Run Keyword If    '${USERGROUP.IncomeGroupRequired}' == 'Yes'    Click Element    ${loc_chkFieldIncomeGroupRequired}
    Run Keyword If    '${USERGROUP.InterestGroup}' == 'No'    Click Element    ${loc_chkFieldInterestGroup}
    Run Keyword If    '${USERGROUP.SMSSubscribe}' == 'No'    Click Element    ${loc_chkFieldSMSSubscribe}
    Run Keyword If    '${USERGROUP.EmailSubscribe}' == 'No'    Click Element    ${loc_chkFieldEmailSubscribe}
    Run Keyword If    '${USERGROUP.MailSubscribe}' == 'No'    Click Element    ${loc_chkFieldMailSubscribe}
    Run Keyword If    '${USERGROUP.Company}' == 'No'    Click Element    ${loc_chkFieldCompany}
    Run Keyword If    '${USERGROUP.CompanyRequired}' == 'Yes'    Click Element    ${loc_chkFieldCompanyRequired}
    Run Keyword If    '${USERGROUP.CompanyCountry}' == 'No'    Click Element    ${loc_chkFieldCompanyCountry}
    Run Keyword If    '${USERGROUP.CompanyCountryRequired}' == 'Yes'    Click Element    ${loc_chkFieldCompanyCountryRequired}

The New Field Custom is Added
    [Arguments]    ${FieldCustomSuccessMsg}    ${FieldCustomSuccess}    
    Wait Until Element is Visible    ${FieldCustomSuccessMsg}
    ${NewFieldCustomSuccessMsg}    Get Text    ${FieldCustomSuccessMsg}
    Should be equal   ${NewFieldCustomSuccessMsg}    ${FieldCustomSuccess}