*** Settings ***
Library           SeleniumLibrary
Library           Collections
Library           OperatingSystem
Library           ExcelLibrary
Library           ImapLibrary2
Library           String
Library           DateTime
Library           Dialogs
Resource          Common.robot
Resource          ../Local/Home/Login.robot
Resource          ../Local/Home/Header.robot
Resource          ../Local/Marketing/EmailBlast.robot
Resource          ../Local/Marketing/SMSBlast.robot
Resource          ../Local/Marketing/PNSBlast.robot
Resource          ../Local/Marketing/Configuration/Sender.robot
Resource          ../Local/Marketing/Configuration/EmailTemplates.robot
Resource          ../Local/Marketing/Configuration/PushNotificationTemplates.robot
Resource          ../Local/Marketing/Configuration/EmailBounce.robot
Resource          ../Local/Membership/MemberRegistration.robot
Resource          ../Local/Membership/MemberSearch.robot
Resource          ../Local/Access/User.robot
Resource          ../Local/Access/UserGroup.robot
Resource          ../Local/Access/ResetUserPassword.robot	
Resource          ../Local/Loyalty/Campaigns/RewardsCampaigns.robot
Resource          ../Local/Settings/General/SystemCode.robot
Resource          ../Local/Settings/General/MembershipType.robot
Resource          ../Local/Settings/General/CustomCode.robot
Resource          ../../Locators/Global/LocCommon.robot
Resource          ../../Locators/Local/Home/LocLogin.robot
Resource          ../../Locators/Local/Home/LocClientSwitcher.robot
Resource          ../../Locators/Local/Home/LocHeader.robot
Resource          ../../Locators/Local/Membership/MemberRegistration.robot
Resource          ../../Locators/Local/Membership/LocMembersearch.robot
Resource          ../../Locators/Local/Marketing/LocCommonBlast.robot
Resource          ../../Locators/Local/Marketing/LocEmailBlast.robot
Resource          ../../Locators/Local/Marketing/LocSMSBlast.robot
Resource          ../../Locators/Local/Marketing/LocPNSBlast.robot
Resource          ../../Locators/Local/Marketing/Configuration/LocSender.robot
Resource          ../../Locators/Local/Marketing/Configuration/LocEmailTemplates.robot
Resource          ../../Locators/Local/Marketing/Configuration/LocPushNotiTemplates.robot
Resource          ../../Locators/Local/Marketing/Configuration/LocEmailBounce.robot
Resource          ../../Locators/Local/Access/LocUser.robot
Resource          ../../Locators/Local/Access/LocUserGroup.robot
Resource          ../../Locators/Local/Access/LocResetUserPassword.robot
Resource          ../../Locators/Local/Loyalty/Campaigns/LocRewardsCampaigns.robot
Resource          ../../Locators/Local/Settings/General/LocSystemCode.robot
Resource          ../../Locators/Local/Settings/General/LocMembershipType.robot
Resource          ../../Locators/Local/Settings/General/LocCustomCode.robot
Resource          ../../Variables/Global/common.robot
Resource          ../../Variables/Local/Home/VarLogin.robot
Resource          ../../Variables/Local/Marketing/VarBlast.robot
Resource          ../../Variables/Local/Membership/VarMember.robot
Resource          ../../Variables/Local/Membership/VarMemberSearch.robot
Resource          ../../Variables/Local/Access/VarUser.robot
Resource          ../../Variables/Local/Access/VarUserGroup.robot
Resource          ../../Variables/Local/Access/VarResetUserPassword.robot	
Resource          ../../Variables/Local/Loyalty/VarLoyalty.robot
Resource          ../../Variables/Local/Settings/VarSettings.robot

*** Variable ***
*** Keyword ***


    
