*** Settings ***
Resource    GlobalSettings.robot

*** Variables ***
${ENVIRONMENT}    ${EMPTY}

*** Keywords ***
Get Data From Excel
    [Arguments]    ${arg_file}    ${arg_sheet}    ${arg_column}
    Open Excel    ${arg_file}
    ${count}    Get Row Count    ${arg_sheet}
    &{content}    Create Dictionary
    :FOR    ${i}    IN RANGE    1    ${count}
    \    ${temp}    Get Row Values    ${arg_sheet}    ${i}
    \    Set To Dictionary    ${content}    ${temp[0][1]}    ${temp[${arg_column}][1]}
    Return From Keyword    &{content}
    
Set Test Environment
    Open Excel    ${var_file} 
    ${environment}    Read Cell Data By Name    Environment Settings    B10        
    ${var_column}    Run Keyword If    '${environment}' == 'SIT'    Set Variable    1
    ...    ELSE IF    '${environment}' == 'UAT'    Set Variable    2
    ...    ELSE IF    '${environment}' == 'STAGING'    Set Variable    3
    ...    ELSE IF    '${environment}' == 'PRODUCTION'    Set Variable    4            
    &{ENVIRONMENT}    Create Dictionary
    &{ENVIRONMENT}    Get Data From Excel    ${var_file}    Environment Settings    ${var_column}    
    Set Global Variable    &{ENVIRONMENT}    

Launch CRM application
    Open Browser    ${ENVIRONMENT.URL}    ${ENVIRONMENT.Browser}
    Maximize Browser Window
    Set Selenium Speed    0.25 seconds
    
Content Header Should Be Equal
    [Arguments]    ${mainHeader}
    ${contentHeader}    Get Text    ${loc_textMainContentHeader}    
    Should Be Equal    ${contentHeader}    ${contentHeader}  
    
Valid Login
    Launch CRM application
    #Enter Username
    Input Text    ${loc_inputUsername}    ${ENVIRONMENT.UserID}
    #Enter Password
    Input Password    ${loc_inputPassword}    ${ENVIRONMENT.Password}
    #Pause Execution    Press OK once captcha is manually completed.
    #Click Login Button
    Click Element    ${loc_btnLogin}
    #Select Client
    Select From List By Value    ${loc_listClients}    ${ENVIRONMENT.Client}
    Click Element    ${loc_btnSwitch}
    
User Navigate To ${module} > ${Function}
    Wait Until Element Is Visible    //span[contains(., '${module}')]
    Mouse Over    //span[contains(., '${module}')]
    Click Link    ${function}
    
Error message is Displayed
    [Arguments]    ${arg_msg}
    Page Should Contain    ${arg_msg}        
    
User click ${iconName} icon
    Wait Until Element Is Visible    //i[@data-original-title='${iconName}']    
    Click Element    //i[@data-original-title='${iconName}']
    
User Logout
    Click User Avatar
    Wait Until Element Is Visible    //a[contains(., 'Log Out')]    
    Click Element    //a[contains(., 'Log Out')]
    
Success message is displayed
    [Arguments]    ${elementLocator}    ${message}
    Wait Until Element is Visible    ${elementLocator}    10
    ${elementText}    Get Text    ${elementLocator}
    Should be equal   ${elementText}    ${message}
    
User Navigates To ${module} > ${Function} > ${SubFunction}
    User Navigate To ${module} > ${Function}
    Wait Until Element Is Visible    //a[contains(., '${SubFunction}')]
    Click Link    ${SubFunction}
    
Login as ${accountType}
    Set Login Details   ${accountType}
    Input Text    ${loc_inputUsername}    ${LOGIN.Username}
    Input Password    ${loc_inputPassword}    ${LOGIN.Password}
    Click Element    ${loc_btnLogin}
    
Search Filter by ${searchby}
    User click Filter icon
    Select From List by Value    ${loc_listFilterSearchBy}    ${searchby}
    
Login for Loyalty
    Launch CRM application
    Set Loyalty Login Details   1
    Input Text    ${loc_inputUsername}    ${REWARDSCAMPAIGNS.Username}
    Input Password    ${loc_inputPassword}    ${REWARDSCAMPAIGNS.Password}
    Click Element    ${loc_btnLogin}
    
     
    