*** Variables ***
${loc_listFilterSearchBy}    ctl00_cphBody_ucgvList_uiddlSearchItem
${loc_inputFilterValue}    ctl00_cphBody_ucgvList_uitxtSearchKeyword
${loc_btnFilterSearch}    ctl00_cphBody_ucgvList_uibtnSearch
${loc_btnEditUser}    //span[@class='ui-icon ui-icon-pencil']    
${loc_listAssignUser}    ctl00_cphBody_uilstUserLists
${loc_txtAssignUserSuccessMsg}    ctl00_cphBody_uiwcNotification_lblMessage
${loc_txtUserGroup}    ctl00_cphBody_uitxtUserGroup
${loc_txtUserDescription}    ctl00_cphBody_uitxtUserGroupName
${loc_btnDuplicateGroup}    //li[@title='Duplicate User Group']
${loc_txtDuplicateGroupSuccessMsg}    ctl00_cphBody_uiwcNotification_lblMessage
${loc_btnDuplicateField}    //li[@title='Duplicate Fields Customizing']
${loc_listCustomUserGroup}    ctl00_cphBody_uiddlUserGroup
${loc_listCustomResgistration}    ctl00_cphBody_uiddlFormType    
${loc_txtDuplicateFieldSuccessMsg}    ctl00_cphBody_uiwcNotification_lblMessage
${loc_btnDeleteGrpProceed}    ctl00_cphBody_uibtnProceed
${loc_btnDeleteCancel}    ctl00_cphBody_uibtnCancel
${loc_txtDeleteUserGroupSuccess}    ctl00_cphBody_uiwcNotification_lblMessage
${loc_btnEditFieldCustom}    //li[@title='Edit Fields Customizing']
${loc_chkUncheckAll}    //*[@title='Uncheck All']

${loc_chkFieldJoinDate}    ctl00_cphBody_uiucMemberInfo_uichkHideJoinDate
${loc_txtFieldJoinDate}    ctl00_cphBody_uiucMemberInfo_uitxtNewlblJoinDate
${loc_chkFieldMemberID}    ctl00_cphBody_uiucMemberInfo_uichkHideMemberID
${loc_chkFieldMemberIDRequired}    ctl00_cphBody_uiucMemberInfo_uichkMandatoryMemberID
${loc_chkFieldVIP}    ctl00_cphBody_uiucMemberInfo_uichkHideMemberIsVIP
${loc_chkFieldSalutation}    ctl00_cphBody_uiucMemberInfo_uichkHideMemberSalutation
${loc_chkFieldSalutationRequired}    ctl00_cphBody_uiucMemberInfo_uichkMandatoryMemberSalutation
${loc_chkFieldName}    ctl00_cphBody_uiucMemberInfo_uichkHideMemberName
${loc_chkFieldNameRequired}    ctl00_cphBody_uiucMemberInfo_uichkMandatoryMemberName
${loc_chkFieldNRIC}    ctl00_cphBody_uiucMemberInfo_uichkHideMemberNRIC
${loc_chkFieldNRICRequired}    ctl00_cphBody_uiucMemberInfo_uichkMandatoryMemberNRIC  
${loc_chkFieldPassport}    ctl00_cphBody_uiucMemberInfo_uichkHideMemberPassport
${loc_chkFieldPassportRequired}    ctl00_cphBody_uiucMemberInfo_uichkMandatoryMemberPassport
${loc_chkFieldEmail}    ctl00_cphBody_uiucMemberInfo_uichkHideMemberEmail
${loc_chkFieldEmailRequired}    ctl00_cphBody_uiucMemberInfo_uichkMandatoryMemberEmail
${loc_chkFieldDOB}    ctl00_cphBody_uiucMemberInfo_uichkHideMemberDOB
${loc_chkFieldDOBRequired}    ctl00_cphBody_uiucMemberInfo_uichkMandatoryMemberDOB
${loc_chkFieldGender}    ctl00_cphBody_uiucMemberInfo_uichkHideMemberGender
${loc_chkFieldGenderRequired}    ctl00_cphBody_uiucMemberInfo_uichkMandatoryMemberGender
${loc_chkFieldMaritalStatus}    ctl00_cphBody_uiucMemberInfo_uichkHideMemberMaritalStatus
${loc_chkFieldMaritalStatusRequired}    ctl00_cphBody_uiucMemberInfo_uichkMandatoryMemberMaritalStatus
${loc_chkFieldRace}    ctl00_cphBody_uiucMemberInfo_uichkHideMemberRace
${loc_chkFieldRaceRequired}    ctl00_cphBody_uiucMemberInfo_uichkMandatoryMemberRace
${loc_chkFieldNationality}    ctl00_cphBody_uiucMemberInfo_uichkHideMemberNationality
${loc_chkFieldNationalityRequired}    ctl00_cphBody_uiucMemberInfo_uichkMandatoryMemberNationality
${loc_chkFieldCountry}    ctl00_cphBody_uiucMemberInfo_uichkHideMemberCountry
${loc_chkFieldCountryRequired}    ctl00_cphBody_uiucMemberInfo_uichkMandatoryMemberCountry
${loc_chkFieldAddress}    ctl00_cphBody_uiucMemberInfo_uichkHideMemberBlock
${loc_chkFieldAddressRequired}    ctl00_cphBody_uiucMemberInfo_uichkMandatoryMemberBlock
${loc_chkFieldAddress1}    ctl00_cphBody_uiucMemberInfo_uichkHideMemberAddress
${loc_chkFieldAddress1Required}    ctl00_cphBody_uiucMemberInfo_uichkMandatoryMemberAddress1
${loc_chkFieldContactNo}    ctl00_cphBody_uiucMemberInfo_uichkHideMemberHomeNo
${loc_chkFieldContactNoRequired}    ctl00_cphBody_uiucMemberInfo_uichkMandatoryMemberHomeNo
${loc_chkFieldMobileNo}    ctl00_cphBody_uiucMemberInfo_uichkHideMemberContactNo
${loc_chkFieldMobileNoRequired}    ctl00_cphBody_uiucMemberInfo_uichkMandatoryMemberContactNo
${loc_chkFieldFaxNo}    ctl00_cphBody_uiucMemberInfo_uichkHideMemberFaxNo
${loc_chkFieldFaxNoRequired}    ctl00_cphBody_uiucMemberInfo_uichkMandatoryMemberFaxNo
${loc_chkFieldVehicleIU}    ctl00_cphBody_uiucMemberInfo_uichkHideIU
${loc_chkFieldVehicleIURequired}    ctl00_cphBody_uiucMemberInfo_uichkMandatoryMemberIU
${loc_chkFieldVehicle}    ctl00_cphBody_uiucMemberInfo_uichkHideVehicle
${loc_chkFieldVehicleRequired}    ctl00_cphBody_uiucMemberInfo_uichkMandatoryMemberVehicle
${loc_chkFieldTag}    ctl00_cphBody_uiucMemberInfo_uichkHideMemberCategory
${loc_chkFieldTagRequired}    ctl00_cphBody_uiucMemberInfo_uichkMandatoryMemberCategory
${loc_chkFieldOccupation}    ctl00_cphBody_uiucMemberInfo_uichkHideMemberOccupation
${loc_chkFieldOccupationRequired}    ctl00_cphBody_uiucMemberInfo_uichkHideMemberOccupation
${loc_chkFieldIncomeGroup}    ctl00_cphBody_uiucMemberInfo_uichkHideMemberIncomeGroup
${loc_chkFieldIncomeGroupRequired}    ctl00_cphBody_uiucMemberInfo_uichkMandatoryMemberIncomeGroup
${loc_chkFieldInterestGroup}    ctl00_cphBody_uiucMemberInfo_uichkHideMemberInterestGroup
${loc_chkFieldSMSSubscribe}    ctl00_cphBody_uiucMemberInfo_uichkHideSMSSubscriptionRemarks
${loc_chkFieldEmailSubscribe}    ctl00_cphBody_uiucMemberInfo_uichkHideMemberEmailSubscriptionBySender
${loc_chkFieldMailSubscribe}    ctl00_cphBody_uiucMemberInfo_uichkHideMailSubscription
${loc_chkFieldCompany}    ctl00_cphBody_uiucMemberInfo_uichkHideMemberCompany
${loc_chkFieldCompanyRequired}    ctl00_cphBody_uiucMemberInfo_uichkMandatoryMemberCompany
${loc_chkFieldCompanyCountry}    ctl00_cphBody_uiucMemberInfo_uichkHideMemberCompanyCountry
${loc_chkFieldCompanyCountryRequired}    ctl00_cphBody_uiucMemberInfo_uichkMandatoryMemberCompanyCountry

${loc_txtUpdateFieldCustomSuccessMsg}    ctl00_cphBody_uiwcNotification_lblMessage 