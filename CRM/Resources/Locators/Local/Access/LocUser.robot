*** Variables ***
${loc_btnAddNew}    //i[@data-original-title='Add New User']
${loc_inputUserID}    ctl00_cphBody_uitxtUserID
${loc_inputUserPassword}    ctl00_cphBody_uitxtPassword
${loc_inputUserConfirmPassword}    ctl00_cphBody_uitxtConfirmPassword
${loc_inputUserName}    ctl00_cphBody_uitxtUserName
${loc_inputUserEmail}    ctl00_cphBody_uitxtEmail
${loc_inputUserMobile}    ctl00_cphBody_uitxtMobileNo
${loc_chkUserEnable}    uichkEnable
${loc_listUserGroup}    ctl00_cphBody_uiddlUserGroup
${loc_listUserAccessLevel}    ctl00_cphBody_uiddlAccessLevelSetting
${loc_txtAddNewUserSuccessMsg}    ctl00_cphBody_uiwcNotification_lblMessage
${loc_listFilterSearchBy}    ctl00_cphBody_ucgvList_uiddlSearchItem
${loc_inputFilterValue}    ctl00_cphBody_ucgvList_uitxtSearchKeyword
${loc_btnFilterSearch}    ctl00_cphBody_ucgvList_uibtnSearch
${loc_btnEditUser}    //span[@class='ui-icon ui-icon-pencil']    
${loc_txtEditUserSuccess}    ctl00_cphBody_uiwcNotification_lblMessage
${loc_chkUserList}    ctl00_cphBody_ucgvList_uigvItemList_ctl02_uichkSelected



${loc_inputDeleteReason}    uitxtDeleteReason
${loc_btnDeleteProceed}    ctl00_cphBody_uibtnProceed
${loc_txtDeleteUserSuccess}    ctl00_cphBody_uiwcNotification_lblMessage