*** Variables ***
${loc_txtCurrentPassword}    ctl00_cphBody_uitxtCurPassword
${loc_txtNewPassword}    ctl00_cphBody_uitxtPassword
${loc_txtConfirmPassword}    ctl00_cphBody_uitxtConfirmPassword
${loc_txtResetPasswordSuccessMsg}    ctl00_cphBody_uiwcNotification_lblMessage
${loc_listSearchMember}    ctl00_cphBody_uiddlSearchField
${loc_txtMemberName}    ctl00_cphBody_uitxtMemberName
${loc_btnMemSearch}    ctl00_cphBody_uilnkbtnSubmit
${loc_listSendResetPwordEmail}    ctl00_cphBody_uigvList_uigvItemList_ctl02_uilblSendResetLink
${loc_btnSendResetProceed}    ctl00_cphBody_uibtnRetrievePassword    
${loc_btnConfirmClose}    ctl00_cphBody_pnlPasswordRetrievalSuccess
