*** Variable ***
${loc_rdoMemberFilterByAll}    ctl00_cphBody_urdlMemberFilterBy_0
${loc_rdoMemberFilterByMember}    ctl00_cphBody_urdlMemberFilterBy_1
${loc_rdoMemberFilterByNonMember}    ctl00_cphBody_urdlMemberFilterBy_2
${loc_listApplicationName}    ctl00_cphBody_uilstAppName
${loc_listDeviceModel}    ctl00_cphBody_uilstDeviceModel
${loc_chkIsDevelopment}    ctl00_cphBody_uichkIsDevelopment
${loc_chkPushBadge}    ctl00_cphBody_uichkPushBadge
${loc_chkPushAlert}    ctl00_cphBody_uichkPushAlert
${loc_chkPushSound}    ctl00_cphBody_uichkPushSound
${loc_chkUseExistingTemplate}    ctl00_cphBody_uichkUseExistingTemplate
${loc_listPNSTemplate}    ctl00$cphBody$uiddlPushExistingTemplate
${loc_inputPushNotifMsg}    ctl00_cphBody_uitxtPushTemplateMsg
${loc_inputInAppSubject}    ctl00_cphBody_uitxtPushTemplateInAppSubject
${loc_inputInAppDescription}    ctl00_cphBody_uitxtPushTemplateInAppDesc
${loc_uploadInAppImage}    ctl00_cphBody_uifuPushImg
${loc_listDeepLink}    ctl00_cphBody_uiddlPushTemplateDeepLink
${loc_btnPNSMsgSubmit}    ctl00_cphBody_uibtnSave
${loc_btnPNSMsgReturn}    ctl00_cphBody_uibtnCancel
${loc_btnInclusionAddFromQuery}    ctl00_cphBody_ucgvMailingList_uirpCommands_ctl00_labelFor_uibtnAddFromQuery
${loc_inputIncQueryName}    ctl00_cphBody_uitxtQueryName
${loc_inputIncDescription}    ctl00_cphBody_uitxtDescription
${loc_btnQuerySeach}    ctl00_cphBody_uibtnShowStoredQueryList
${loc_btnQueryAddSelected}    ctl00_cphBody_uibtnAddStoredQuery
${loc_tblPNSBlast}    ctl00_cphBody_ucgvPushNotificationBlastList_uigvItemList
${loc_btnStartPNSBlast}    ctl00_cphBody_uibtnToggleEmailBlastExecution
${loc_tblGeneratedRecipients}    ctl00_cphBody_ucgvPushNotificationBlastMailingList_uigvItemList

                        