*** Variable ***
${loc_cboSearchBy}    ctl00_cphBody_ucgvSender_uiddlSearchItem
${loc_inputSenderValue}    ctl00_cphBody_ucgvSender_uitxtSearchKeyword
${loc_btnSenderSearch}    ctl00_cphBody_ucgvSender_uibtnSearch
${loc_btnReset}    ctl00_cphBody_ucgvSender_uibtnReset
#Sender Table
${loc_tableSenderList}    ctl00_cphBody_ucgvSender_uigvItemList
${loc_senderRowsCount}    ctl00_cphBody_ucgvSender_uilblRowsVisible_Top
${loc_senderNotificationMessage}    ctl00_cphBody_ucNotification_lblMessage
#Create New Sender
${loc_contentPageHeaderSender}       exampleModalLabel
${loc_inputSenderName}    ctl00_cphBody_uitxtSenderName
${loc_inputSenderEmailFrom}    ctl00_cphBody_uitxtSenderEmail
${loc_inputRedirectUrl}    ctl00_cphBody_uitxtUnsubscribeRedirectUrl
${loc_btnSenderSubmit}    ctl00_cphBody_uibtnSave
${loc_inputMailingListName}    ctl00_cphBody_uitxtMailingListName
${loc_inputSmsMaskedName}    ctl00_cphBody_uitxtSMSMaskedName
${loc_radioEDMCreatorUsername}    ctl00_cphBody_uichkDisplayEDMCreatorUsername
#Delete Confirmation
${loc_btnConfirmationDelete}    //button[contains(text(), 'Proceed')]
${loc_textAreaError}    //textarea[contains(@class, 'form-control ui-state-error')]
${loc_inputReasonForDeletion}    ctl00_cphBody_uitxtDeleteReason
${loc_closeWindow}    //*[@id="gridViewDeleteModal"]/div/div/div[1]/button/span
${loc_btnConfirmationCancel}    //button[contains(text(), 'Cancel')]