*** Variable ***
${loc_cboEmailBounceSearchBy}       ctl00_cphBody_ucgvAccount_uiddlSearchItem                            
${loc_inputEmailBounceValue}        ctl00_cphBody_ucgvAccount_uitxtSearchKeyword
${loc_btnEmailBounceSearch}         ctl00_cphBody_ucgvAccount_uibtnSearch
#Email Bounce Table
${loc_tableEmailBounceList}    ctl00_cphBody_ucgvAccount_uigvItemList
${loc_emailBounceRowsCount}    ctl00_cphBody_ucgvAccount_uilblRowsVisible_Top
#Create New Email Bounce
${loc_inputEmailBounceLoginId}    ctl00_cphBody_uitxtLoginID
${loc_inputEmailBouncePassword}    ctl00_cphBody_uitxtPwd
${loc_inputEmailBouncePop3HostName}    ctl00_cphBody_uitxtHostname
${loc_inputEmailBouncePort}    ctl00_cphBody_uitxtPort
${loc_inputEmailBounceBounceBackPath}    ctl00_cphBody_uitxtBouncedBackPath
${loc_inputEmailBounceDescription}    ctl00_cphBody_uitxtDescription
${loc_inputEmailBounceSoftBounce}       ctl00_cphBody_uitxtMaxSoftBounced
${loc_cboEmailBounceCustomerCode}    ctl00_cphBody_uilstCustomerCode
${loc_emailBounceNotification}       ctl00_cphBody_ucNotification_pnlMessage
#Required Fields
${loc_requiredLoginId}    ctl00_cphBody_uirevtxtLoginID
${loc_requiredPassword}    ctl00_cphBody_uirevtxtPwd
${loc_requiredHostname}    ctl00_cphBody_uirevtxtHostname
${loc_requiredPort}    ctl00_cphBody_uirevtxtPort
${loc_requiredBounceBack}    ctl00_cphBody_uirevtxtBouncedBackPath
${loc_requiredCounter}    ctl00_cphBody_uirfvtxtMaxSoftBounced
#Delete
${loc_btnDeleteEmailBounce}    //input[contains(text(), 'Proceed')]
${loc_inputEmailBounceDeleteReason}    uitxtDeleteReason