*** Variable ***
${loc_cboTemplateSearchBy}       ctl00_cphBody_ucgvEmailTemplates_uiddlSearchItem
${loc_inputTemplateValue}        ctl00_cphBody_ucgvEmailTemplates_uitxtSearchKeyword
${loc_btnTemplateSearch}         ctl00_cphBody_ucgvEmailTemplates_uibtnSearch
#Email Template Table
${loc_tableEmailTemplateList}    ctl00_cphBody_ucgvEmailTemplates_uigvItemList
${loc_emailTemplateRowsCount}    ctl00_cphBody_ucgvEmailTemplates_uilblRowsVisible_Top
#Create Template
${loc_cboTemplateSelectSender}   ctl00_cphBody_uiddlSender
${loc_cboTemplateType}           ctl00_cphBody_uiddlTemplateType
${loc_inputTemplateName}         ctl00_cphBody_uitxtTemplateName
${loc_inputTemplateEmailSubject}    ctl00_cphBody_uitxtEmailSubject
${loc_inputTemplateDescription}     ctl00_cphBody_uitxtTemplateDescription
${loc_templateBody}                 //body[@class='cke_editable cke_editable_themed cke_contents_ltr cke_show_borders']
${loc_inputTemplateDeleteReason}    uitxtDeleteReason
${loc_templateManagementStatus}    ctl00_cphBody_ucNotificationMessage_lblMessage
#//body[contains(@class,'cke_editable cke_editable_themed cke_contents_ltr cke_show_borders')]
