*** Variable ***
${loc_searchByPushNotiTemp}    ctl00_cphBody_uigvList_uiddlSearchItem
${loc_inputValuePushNotiTemp}    ctl00_cphBody_uigvList_uitxtSearchKeyword
${loc_btnSearchPushNotiTemp}    ctl00_cphBody_uigvList_uibtnSearch
#Push Notification Table
${loc_pushNotiTempTable}    ctl00_cphBody_uigvList_uigvItemList
${loc_pushNotiTempRowsCount}    ctl00_cphBody_uigvList_uilblRowsVisible_Top
${loc_PushNotiTempNotificationMessage}    ctl00_cphBody_ucNotification_lblMessage
#Create New Push Notification Template
${loc_inputPushNotiTempName}      ctl00_cphBody_uitxtPushTemplateName
${loc_inputPushNotiTempDesc}      ctl00_cphBody_uitxtPushTemplateDesc
${loc_cboPushNotiTempType}        ctl00_cphBody_uiddlTemplateType
${loc_inputPushNotiTempMessage}   ctl00_cphBody_uitxtPushTemplateMsg
${loc_inputPushNotiInAppSubject}    ctl00_cphBody_uitxtPushTemplateInAppSubject
${loc_inputPushNotiInAppDescr}      ctl00_cphBody_uitxtPushTemplateInAppDesc
${loc_inputPushNotiInAppMessage}    //body[@class='cke_editable cke_editable_themed cke_contents_ltr cke_show_borders']
#Delete Window
${loc_closeWindowPushNoti}    //*[@id="gridViewDeleteModal"]/div/div/div[1]/button/span
${loc_btnConfirmationCancelPushNoti}    ctl00_cphBody_uibtnCancel
${loc_btnDeleteConfirmPushNoti}    ctl00_cphBody_uibtnProceed
${loc_textAreaErrorPushNoti}    //textarea[contains(@class, 'form-control ui-state-error')]
${loc_inputReasonForDeletionPushNoti}    uitxtDeleteReason