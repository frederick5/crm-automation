*** Variable ***
${loc_listSelectSender}    ctl00_cphBody_uiddlSender
${loc_chkIncludeUnscubscription}    uichkIncludeUnsubscriptionOption
${loc_btnEmailSubmit}    ctl00_cphBody_uibtnSaveEmailBlastSchedule
${loc_inputEmailSubject}    ctl00_cphBody_uitxtEmailSubject
${loc_inputEmailMessage}    //body[@class='cke_editable cke_editable_themed cke_contents_ltr cke_show_borders']
${loc_btnKeyInEmailList}    //input[@value='Key in your own mailing list']
${loc_inputKeyInMailingList}    ctl00_cphBody_txtEmailAddress
${loc_btnKeyInGenerateList}    ctl00_cphBody_uibtnGenerateMailingListFromInput
${loc_listApproverSearchBy}    ctl00_cphBody_ucgvApprovalRecipientSearch_uiddlSearchItem
${loc_listApproverSearchValue}    ctl00_cphBody_ucgvApprovalRecipientSearch_uitxtSearchKeyword
${loc_btnApproverSearch}    ctl00_cphBody_ucgvApprovalRecipientSearch_uibtnSearch
${loc_btnApproveEmailBlast}    ctl00_cphBody_uibtnApproveBlast
${loc_tblEmailBlast}    ctl00_cphBody_ucgvEmailBlastList_uigvItemList
${loc_btnStartEmailBlast}    ctl00_cphBody_uibtnToggleEmailBlastExecution

