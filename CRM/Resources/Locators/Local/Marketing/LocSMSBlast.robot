*** Variable ***
${loc_btnSMSSubmit}    ctl00_cphBody_uibtnSaveSMSSchedule
${loc_listSMSSender}    ctl00_cphBody_uiddlSender
${loc_inputSMSMessage}    ctl00_cphBody_uitxtSMSMessage
${loc_btnSMSMessageSave}    ctl00_cphBody_uibtnSaveSMSMessage
${loc_btnKeyInSMSList}    //input[@value='Key in your own SMS list']
${loc_inputKeyInSMSRecipients}    ctl00_cphBody_txtMobileNos
${loc_btnGenerateSMSList}    //input[@value='Generate SMS List']
${loc_btnApproveSMSBlast}    ctl00_cphBody_uibtnApproveBlast
${loc_tblSMSBlast}    ctl00_cphBody_ucgvSmsBlast_uigvItemList
${loc_btnStartSMSBlast}    ctl00_cphBody_uibtnToggleSMSBlastExecution
${loc_txtTotalSent}    ctl00_cphBody_uilblSMSBlast_TotalSent
${loc_txtTotalError}    ctl00_cphBody_uilblSMSBlast_TotalErrors
