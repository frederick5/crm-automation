*** Variable ***
${loc_inputBlastName}    ctl00_cphBody_uitxtName
${loc_listBlastType}    ctl00_cphBody_uiddlBlastType
${loc_chkCustomizedAttributes}    uichkIncludeCustomizedAttributes
${loc_listRetrieveAttributes}    ctl00_cphBody_uiddlCustomizedAttributesSpName
${loc_btnNext}    ctl00_cphBody_uibtnNext
${loc_btnCancel}    //div//input[@value='Cancel']
${loc_rdoSchedOptionOnce}    ctl00_cphBody_uirblScheduleOption_0
${loc_rdoSchedOptionDaily}    ctl00_cphBody_uirblScheduleOption_1
${loc_rdoSchedOptionWeekly}    ctl00_cphBody_uirblScheduleOption_2
${loc_rdoSchedOptionMonthly}    ctl00_cphBody_uirblScheduleOption_3
${loc_dateStopRecurring}    ctl00_cphBody_uitxtStopRecurringOn
${loc_tblApprovers}    //table[@id='ctl00_cphBody_ucgvApprovalRecipientSearch_uigvItemList']
${loc_inputStartReason}    uitxtStartReason
${loc_btnProceedStart}    dialog-start-confirm_btnProceed
${loc_btnGenerateRecipientList}    ctl00_cphBody_uibtnGenerateRecipientList 
              