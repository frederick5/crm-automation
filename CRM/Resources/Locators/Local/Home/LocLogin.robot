*** Variables ***
${loc_inputUsername}    ctl00_cphBody_uitxtUserName
${loc_inputPassword}    ctl00_cphBody_uitxtPassword
${loc_btnLogin}    loginbuttons
${loc_linkForgotPassword}    ctl00_cphBody_uilnkbtnForgetPassword
${loc_inputUserID}    ctl00_cphBody_uitxtUserName
${loc_btnSendResetPassword}    uibtnResetPassword
${loc_inputNewPassword}    uitxtPassword
${loc_inputNewConfirmPassword}    uitxtConfirmPassword       
${loc_btnSubmitResetPassword}    uibtnSubmitUserResetPassword      