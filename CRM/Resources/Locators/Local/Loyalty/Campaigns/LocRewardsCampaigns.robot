*** Variables ***
#User Group Variable
${loc_userGroupLoyalty}    //*[@id="ui-id-3"]/a
${loc_userGroupRewardsCampaignChckBox}    ctl00_cphBody_uiUCAccessRights_cb_Rewards Campaigns_0
#Reward Campaign Management
${loc_inputGeneralCode}    ctl00_cphBody_uitxtCamCode
${loc_inputGeneralName}    ctl00_cphBody_uitxtCamName
${loc_cboGeneralType}      ctl00_cphBody_uiddlCamType
${loc_cboGeneralEntity}    ctl00_cphBody_uiddlCamEntity
${loc_inputGeneralDescription}    ctl00_cphBody_uitxtCamDescription
${loc_inputAvailabilityStartDate}    ctl00_cphBody_uitxtCamDateStart
${loc_inputAvailabilityEndDate}      ctl00_cphBody_uitxtCamDateEnd
${loc_btnCampaignReviewProceed}      ctl00_cphBody_uibtnProceed
${loc_rewardCampaignNotification}    ctl00_cphBody_uiwcNotification_pnlMessage
#Campaign Designer
${loc_inputDesignerCode}    uitxtCamCode
${loc_inputDesignerName}    uitxtCamName
${loc_cboDesignerType}      uiddlCamType
${loc_cboDesignerEntity}    uiddlCamEntity
${loc_inputDesignerDesc}    uitxtCamDescription
${loc_inputDesignerStartDate}    uitxtCamDateStart
${loc_inputDesignerEndDate}        uitxtCamDateEnd
${loc_qualifierRewardsNotiTab}    //*[@id="tab-qra"]/a
${loc_alertErrorRequiredFields}    //*[@id="alertdialog"]/div/div
${loc_btnAlertOk}    //*[@id="alertdialog"]/div/p[3]/span/input
#Qualifiers Rewards Notification
${loc_designerQualifier}    sqid1
${loc_designerAddQualifier}    formtemplatemessage
${loc_designerContainer}    fieldcontainer
${loc_inputQualifierCode}    sc-code
${loc_inputQualifierName}    sc-name
${loc_inputQualifierDesc}    sc-desc
${loc_btnQualifierSave}    saveForm
${loc_alertSaveSuccess}    //*[@id="alertdialog"]/div/p[1]
${loc_btnAlertSuccessOk}    //*[@id="alertdialog"]/div/p[2]/span/input
#Reward Campaign Listing
${loc_cboRewardListingGroupName}    ctl00_cphBody_uiddlGroupName
${loc_inputRewardListingCode}    ctl00_cphBody_uitxtCampCode
${loc_btnRewardListingSearch}    ctl00_cphBody_uibtnSearch
${loc_rewardListingMessage}      ctl00_cphBody_uiwcNotification_lblMessage
${loc_inputRewardListingDeleteReason}    uitxtDeleteReason
${loc_btnRewardListingDeleteProceed}    ctl00_cphBody_uibtnProceed
${loc_btnRewardListingDeleteCancel}     ctl00_cphBody_uibtnCancel