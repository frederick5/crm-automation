*** Variables ***
#Member Search Page
${loc_btnRefresh}    //i[@class='icon la la-refresh']
${loc_listSearch}    ctl00_cphBody_uiddlSearchField
${loc_fieldSearchTextSelect}    ctl00_cphBody_uiTxtDummy
${loc_fieldSearchTextCardNo}    ctl00_cphBody_uitxtCardNo
${loc_fieldSearchTextContactNo}    ctl00_cphBody_uitxtMemberHomeNo
${loc_fieldSearchTextEmail}    ctl00_cphBody_uitxtMemberEmail
${loc_fieldSearchTextMemberID}    ctl00_cphBody_uitxtMemberID
${loc_fieldSearchTextMobileNo}    ctl00_cphBody_uitxtMemberContactNo
${loc_fieldSearchTextName}    ctl00_cphBody_uitxtMemberName
${loc_fieldSearchTextNRIC}    ctl00_cphBody_uitxtMemberNRIC
${loc_fieldSearchTextPassport}    ctl00_cphBody_uitxtMemberPassport
${loc_linkExpand}    expand
${loc_chkCampaign}    chkCampaign
${loc_listCampaign}    ctl00_cphBody_uiddlMemQExistingCampaign
${loc_btnDeleteCampaign}    ctl00_cphBody_uipnlbtnDeleteCampaign
${loc_listQuery}    ctl00_cphBody_uiddlMemQExistingQuery
${loc_btnDeleteQuery}    ctl00_cphBody_uipnlbtnDeleteQuery
${loc_listCustomizedQuery}    ctl00_cphBody_uiddlCustomizedQuery
${loc_chkMember}    chkMember
${loc_listAgeRangeFrom}    ctl00_cphBody_uitxtMemberAgeFrom
${loc_listAgeRangeTo}    ctl00_cphBody_uitxtMemberAgeTo
${loc_listCountry}    ctl00_cphBody_uilstMemberCountry
${loc_listIncomeGroup}    ctl00_cphBody_uilstMemberIncomeGroup
${loc_fieldStreet}    ctl00_cphBody_uitxtMemberStreet
${loc_fieldPostalCode}    ctl00_cphBody_uitxtMemberPostalCode
${loc_listPostalDistrict}    ctl00_cphBody_uilstMemberPostalDistrict
${loc_listDOBDayFrom}    ctl00_cphBody_uiddlMemberDOBDayFrom
${loc_listDOBMonthFrom}    ctl00_cphBody_uiddlMemberDOBMonthFrom
${loc_listDOBYearFrom}    ctl00_cphBody_uiddlMemberDOBYearFrom
${loc_listDOBDayTo}    ctl00_cphBody_uiddlMemberDOBDayTo
${loc_listDOBMonthTo}    ctl00_cphBody_uiddlMemberDOBMonthTo
${loc_listDOBYearTo}    ctl00_cphBody_uiddlMemberDOBYearTo
${loc_listRace}    ctl00_cphBody_uilstMemberRace
${loc_listMaritalStatus}    ctl00_cphBody_uilstMemberMaritalStatus
${loc_dateJoinDateFrom}    ctl00_cphBody_uitxtMemberJoinDateFrom
${loc_dateJoinDateTo}    ctl00_cphBody_uitxtMemberJoinDateTo
${loc_listInterestGroup}    ctl00_cphBody_uilstMemberInterestGroup
${loc_fieldCompany}    ctl00_cphBody_uitxtMemberCompany
${loc_dateLastLoginDateFrom}    ctl00_cphBody_uitxtMemberLastLoginDateFrom
${loc_dateLastLoginDateTo}    ctl00_cphBody_uitxtMemberLastLoginDateTo
${loc_listValid}    ctl00_cphBody_cbValid
${loc_listGender}    ctl00_cphBody_uiddlMemberGender
${loc_listNationality}    ctl00_cphBody_uilstMemberNationality
${loc_listOccupation}    ctl00_cphBody_uilstMemberOccupation
${loc_fieldVehicleIU}    ctl00_cphBody_uitxtMemberQueryIU
${loc_fieldCarPlateNo}    ctl00_cphBody_uitxtMemberQueryVehiclePlateNo
${loc_listSubscription}    ctl00_cphBody_cbSubscription
${loc_listIsActiveCampaignTag}    ctl00_cphBody_uiddlIsActiveCampaignTag
${loc_listCampaignTag}    ctl00_cphBody_uilstActiveCampaignTag
${loc_fieldMemberTag}    ctl00_cphBody_uitxtMemberTag
${loc_chkMemberAdd}    chkMemberAdd
${loc_fieldDynamicTEST}    ctl00_cphBody_uiucDynamicInfo_uitxtTEST1
${loc_listDynamicdropwhole}    ctl00_cphBody_uiucDynamicInfo_uilbldropwhole2
${loc_chkMemberType}    chkTypeField
${loc_fieldPrintedName}    ctl00_cphBody_uitxtCardPrintedName
${loc_listProgramType}    ctl00_cphBody_uiddlMemTypeProgramType
${loc_listMembershipType}    ctl00_cphBody_uilstCardMembershipType
${loc_listMembershipTier}    ctl00_cphBody_uilstCardMembershipTier
${loc_listMembershipStatus}    ctl00_cphBody_uilstCardMembershipStatus
${loc_listCardsFilterBy}    ctl00_cphBody_uilstCardFilter
${loc_fieldCardNoRangeFrom}    ctl00_cphBody_uitxtCardNoRangeFrom
${loc_fieldCardNoRangeTo}    ctl00_cphBody_uitxtCardNoRangeTo
${loc_fieldBalPointsRangeFrom}    ctl00_cphBody_uitxtLoyaltyPointsRangeFrom
${loc_fieldBalPointsRangeTo}     ctl00_cphBody_uitxtLoyaltyPointsRangeTo
${loc_dateIssueDateFrom}    ctl00_cphBody_uitxtCardIssueDateFrom
${loc_dateIssueDateTo}    ctl00_cphBody_uitxtCardIssueDateTo
${loc_dateEffectiveDateFrom}    ctl00_cphBody_uitxtCardEffectiveDateFrom
${loc_dateEffectiveDateTo}    ctl00_cphBody_uitxtCardEffectiveDateTo
${loc_dateExpiryDateFrom}    ctl00_cphBody_uitxtCardExpiryDateFrom
${loc_dateExpiryDateTo}    ctl00_cphBody_uitxtCardExpiryDateTo
${loc_dateCurrentRCGraceExpiryDateFrom}    ctl00_cphBody_uitxtCardPreviousRCGraceEndDateFrom
${loc_dateCurrentRCGraceExpiryDateTo}    ctl00_cphBody_uitxtCardPreviousRCGraceEndDateTo
${loc_datePrintDateFrom}    ctl00_cphBody_uitxtCardPrintedDateFrom
${loc_datePrintDateTo}    ctl00_cphBody_uitxtCardPrintedDateTo
${loc_listMembershipBatch}    ctl00_cphBody_uilstCardMembershipBatch
${loc_listSignUpLocation}    ctl00_cphBody_uilstCardMembershipSignUpLocation
${loc_listPickUpLocation}    ctl00_cphBody_uilstCardMembershipPickUpLocation
${loc_chkVoucherTransaction}    chkvouchertransaction
${loc_dateOriginalTransactionDateFrom}    ctl00_cphBody_uitxtOriTransactionsDateFrom
${loc_dateOriginalTransactionDateTo}    ctl00_cphBody_uitxtOriTransactionsDateTo
${loc_dateTransactionDateFrom}    ctl00_cphBody_uitxtTransactionsDateFrom
${loc_dateTransactionDateTo}    ctl00_cphBody_uitxtTransactionsDateTo
${loc_listOutlet}    ctl00_cphBody_uilstOutlet
${loc_fieldSpendingAmountFrom}    ctl00_cphBody_uitxtSpendingAmountFrom
${loc_fieldSpendingAmountTo}    ctl00_cphBody_uitxtSpendingAmountTo
${loc_chkExportFields}    chkexportfields
${loc_chkSelectAll}    uichkSelectAll
${loc_chkCardCardNo}    ctl00_cphBody_uichklstExportFields_0
${loc_chkCardMemberID}    ctl00_cphBody_uichklstExportFields_1
${loc_chkCardPrintedName}    ctl00_cphBody_uichklstExportFields_2
${loc_chkCardMembershipStatusCode}    ctl00_cphBody_uichklstExportFields_3
${loc_chkCardIssueDate}    ctl00_cphBody_uichklstExportFields_4
${loc_chkCardEffectiveDate}    ctl00_cphBody_uichklstExportFields_5
${loc_chkCardExpiryDate}    ctl00_cphBody_uichklstExportFields_6
${loc_chkCardTierCode}    ctl00_cphBody_uichklstExportFields_7
${loc_chkCardPointsBAL}    ctl00_cphBody_uichklstExportFields_8
${loc_chkCardPasscode}     ctl00_cphBody_uichklstExportFields_9
${loc_chkCardStoredValueBalance}    ctl00_cphBody_uichklstExportFields_10
${loc_chkCCN}    ctl00_cphBody_uichklstExportFields_11
${loc_chkMemberName}    ctl00_cphBody_uichklstExportFields_12
${loc_chkMemberNRIC}    ctl00_cphBody_uichklstExportFields_13
${loc_chkMemberPassport}    ctl00_cphBody_uichklstExportFields_14
${loc_chkMemberMaritalStatus}    ctl00_cphBody_uichklstExportFields_15
${loc_chkMemberDateofBirth}    ctl00_cphBody_uichklstExportFields_16
${loc_chkMemberMobileNo}    ctl00_cphBody_uichklstExportFields_17
${loc_chkMembershipType}    ctl00_cphBody_uichklstExportFields_18
${loc_chkTotalGracePoints}    ctl00_cphBody_uichklstExportFields_19
${loc_chkTEST}    ctl00_cphBody_uichklstDynamicExportFields_0
${loc_chkmand}    ctl00_cphBody_uichklstDynamicExportFields_1
${loc_dropwhole}    ctl00_cphBody_uichklstDynamicExportFields_2
${loc_btnSearch}    ctl00_cphBody_uilnkbtnSubmit
${loc_listToggleDropdown}    //button[@class='btn btn-primary dropdown-toggle dropdown-toggle-split']
${loc_linkCardSearch}    ctl00_cphBody_uilnkbtnBatchUpdate
${loc_linkExportData}    ctl00_cphBody_uilnkbtnExportData
${loc_linkExportTo}    ctl00_cphBody_uilnkbtnExportToExcelData
${loc_linkSendEmailBlast}    ctl00_cphBody_uilnkbtnBlastEmail
${loc_linkSendSMSBlast}    ctl00_cphBody_uilnkbtnBlastSMS
${loc_linkGenerateLetters}    ctl00_cphBody_uilnkGenerateLetters
${loc_linkSaveQuery}    ctl00_cphBody_uilnkbtnSaveQuery
${loc_linkSaveCampaign}    ctl00_cphBody_uilnkbtnSaveCampaign'
${loc_linkSendPushNotification}    ctl00_cphBody_uilnkbtnPushNotification
${loc_linkSendGeoAlerts}    ctl00_cphBody_uilnkbtnGeoTargeted
${loc_linkRewardCampaign}    ctl00_cphBody_uilnkbtnRewardCampaign
${loc_linkCampaignTagging}    ctl00_cphBody_uilnkbtnCampaignTagging
${loc_linkCampaignUntagging}    ctl00_cphBody_uilnkbtnCampaignUntagging
${loc_linkCardActivation}    ctl00_cphBody_uibtnCardActivation
${loc_linkReset}    ctl00_cphBody_uilnkbtnReset
#Member Search Listing Page
${loc_textSearchFilterTitle}    //div[@class='SearchFilterTitle']
${loc_btnDeleteChecked}    ctl00_cphBody_uigvList_uirpCommands_ctl00_uibtnCommand
${loc_textCurrentPageTop}    ctl00_cphBody_uigvList_uilblCurrentPage_Top
${loc_textRowsVisibleTop}    ctl00_cphBody_uigvList_uilblRowsVisible_Top
${loc_textTotalRowsTop}    ctl00_cphBody_uigvList_uilblTotalRows_Top
${loc_tableMemberList}    ctl00_cphBody_uigvList_uigvItemList
${loc_btnCheckAll}    //span[@class='ui-icon ui-icon-check']
${loc_btnUncheckAll}    //span[@class='ui-icon ui-icon-closethick']
${loc_btnShowCardInfo}    ctl00_cphBody_uigvList_uigvItemList_ctl02_uipnlShowCardInfo
${loc_btnShowActionsMenu}    ctl00_cphBody_uigvList_uigvItemList_ctl02_uiPanelShowPopupMenu
${loc_linkEditMember}    ctl00_cphBody_uigvList_uigvItemList_ctl02_uilblEdit
${loc_linkResetPassword}    ctl00_cphBody_uigvList_uigvItemList_ctl02_uilblResetPassword
${loc_linkSendReset}    ctl00_cphBody_uigvList_uigvItemList_ctl02_uilblSendResetLink
${loc_linkSendSMS}    ctl00_cphBody_uigvList_uigvItemList_ctl02_uilblSMSSend
${loc_linkSendEmail}    ctl00_cphBody_uigvList_uigvItemList_ctl02_uilblEmailSend
${loc_linkCreateNewCase}    ctl00_cphBody_uigvList_uigvItemList_ctl02_uilblNewCase
${loc_linkShowCaseHistory}    ctl00_cphBody_uigvList_uigvItemList_ctl02_uilblCaseManagement
${loc_linkLoginToPortal}    ctl00_cphBody_uigvList_uigvItemList_ctl02_uilblLogintoPortal
${loc_linkDeleteMember}    ctl00_cphBody_uigvList_uigvItemList_ctl02_uilblDeleteCard
${loc_linkLockUnlockMember}    ctl00_cphBody_uigvList_uigvItemList_ctl02_uilblMemLockUnlock
#Member Detail Page
${loc_fieldProgressBar}    sui_progressbar_parent
${loc_linkCardsTab}    //a[@href='#tabs_Membership']
${loc_iframeMembership}    uiframeMembership
${loc_modalCardInfo}    cardinfomodal
${loc_btnEditCardInfo}    ctl00_cphIFrameBody_Membership_Info
${loc_linkProfileTab}    //a[@href='#tabs_BIODATA']
${loc_linkCommLogTab}    //a[@href='#tabs_Communication']
${loc_linkAuditTab}    //a[@href='#tabs_System_Activity']
${loc_linkJournalTab}    //a[@href='#tabs_Journal']
${loc_linkWarrantyTab}    //a[@href='#tabs_Warranty']
${loc_fieldJoinDate}    ctl00_cphMembershipViewBody_uiucMemberInfo_uitxtJoinDate
${loc_rdoGenderMale}    ctl00_cphMembershipViewBody_uiucMemberInfo_uirdolstMemberGender_0
${loc_fieldCountryCode}    ctl00_cphMembershipViewBody_uiucMemberInfo_uitxtMemberContactNoCountryCode
${loc_fieldAreaCode}    ctl00_cphMembershipViewBody_uiucMemberInfo_uitxtMemberContactNoAreaCode
${loc_fieldContactNo}    ctl00_cphMembershipViewBody_uiucMemberInfo_uitxtMemberContactNo
${loc_btnUpdateProfile}    ctl00_cphMembershipViewBody_uibtnUpdate
${loc_textProfileUpdateSuccessMessage}    ctl00_cphMembershipViewBody_uiwcNotification_BIODATA_pnlMessage
${loc_textCardUpdateSuccessMessage}    ctl00_cphIFrameBody_uiwcNotification_CardInfo_pnlMessage
#EditCardInfo
${loc_fieldCardPrintedName}    ctl00_cphIFrameBody_uiucCardInfo_uitxtCardPrintedName
${loc_fieldCardPickupBy}    ctl00_cphIFrameBody_uiucCardInfo_uitxtCardPickupBy
${loc_btnUpdateCard}    ctl00_cphIFrameBody_uibtnCardUpdate