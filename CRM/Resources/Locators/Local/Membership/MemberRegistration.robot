*** Variables ***
${loc_joindate}    ctl00_cphBody_uiucMemberInfo_uitxtJoinDate
${loc_MemberID}    ctl00_cphBody_uiucMemberInfo_uitxtMemberID
${loc_IsVIP}       ctl00_cphBody_uiucMemberInfo_uichkMemberIsVIP
${loc_Salutation}    ctl00_cphBody_uiucMemberInfo_uiddlMemberSalutation
${loc_Name}    ctl00_cphBody_uiucMemberInfo_uitxtMemberName
${loc_NRIC}    ctl00_cphBody_uiucMemberInfo_uitxtMemberNRIC
${loc_GetTempNRIC}    ctl00_cphBody_uiucMemberInfo_btnGetTempNric
${loc_Passport}    ctl00_cphBody_uiucMemberInfo_uitxtMemberPassport
${loc_Email}    ctl00_cphBody_uiucMemberInfo_uitxtMemberEmail
${loc_DOBDay}    ctl00_cphBody_uiucMemberInfo_uiddlMemberDOBDay
${loc_DOBMonth}    ctl00_cphBody_uiucMemberInfo_uiddlMemberDOBMonth
${loc_DOBYear}    ctl00_cphBody_uiucMemberInfo_uiddlMemberDOBYear
${loc_GenderM}    ctl00_cphBody_uiucMemberInfo_uirdolstMemberGender_0
${loc_GenderF}    ctl00_cphBody_uiucMemberInfo_uirdolstMemberGender_1
${loc_MaritalStatus}    ctl00_cphBody_uiucMemberInfo_uiddlMemberMaritalStatus
${loc_Race}    ctl00_cphBody_uiucMemberInfo_uiddlMemberRace
${loc_Nationality}    ctl00_cphBody_uiucMemberInfo_uiddlMemberNationality
${loc_ContactNo}    ctl00_cphBody_uiucMemberInfo_uitxtMemberHomeNo
${loc_MobileNoCountryCode}    ctl00_cphBody_uiucMemberInfo_uitxtMemberContactNoCountryCode
${loc_MobileNoAreaCode}    ctl00_cphBody_uiucMemberInfo_uitxtMemberContactNoAreaCode
${loc_MobileNo}    ctl00_cphBody_uiucMemberInfo_uitxtMemberContactNo
${loc_FaxNo}    ctl00_cphBody_uiucMemberInfo_uitxtMemberFaxNo
${loc_VehicleIUNo}    ctl00_cphBody_uiucMemberInfo_uitxtMemberIU
${loc_Vehicle}    ctl00_cphBody_uiucMemberInfo_uitxtMemberVehicle
${loc_MemberTag}    ctl00_cphBody_uiucMemberInfo_uitxtMemberCategory

${loc_Cardinformation}    chkCard
${loc_MembershipType}    ctl00_cphBody_uiucCardInfo_uiddlCardMembershipType
${loc_MembershipTier}    ctl00_cphBody_uiucCardInfo_uiddlCardMembershipTier
${loc_PrintedName}    ctl00_cphBody_uiucCardInfo_uitxtCardPrintedName
${loc_CradNo}    ctl00_cphBody_uiucCardInfo_uitxtCardNo
${loc_GetCardNo}    ctl00_cphBody_uiucCardInfo_uibtnCardMembershipCardNoGet
${loc_MembershipBatch}    ctl00_cphBody_uiucCardInfo_uiddlCardMembershipBatch
${loc_MembershipStatus}    ctl00_cphBody_uiucCardInfo_uiddlCardMembershipStatus
${loc_SignUpLocation}        uiddlCardMembershipSignUpLocation
${loc_PickUpLocation}    ctl00_cphBody_uiucCardInfo_uiddlCardMembershipPickUpLocation
#${loc_PickUpDate}
${loc_PickUpBy}    ctl00_cphBody_uiucCardInfo_uitxtCardPickupBy
#${loc_IssueDate}
${loc_EffectiveDate}    ctl00_cphBody_uiucCardInfo_uitxtCardEffectiveDate
#${loc_RenewalDate}
#${loc_PrintDate}
${loc_Remarks}    ctl00_cphBody_uiucCardInfo_uitxtCardRemarks


${loc_Save}    //i[@data-original-title='Save']
${loc_NewUserSuccessMessage}    ctl00_cphBody_uiwcNotification_lblMessage