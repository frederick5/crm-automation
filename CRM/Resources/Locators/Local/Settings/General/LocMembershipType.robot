*** Variables ***
#Parent System Code Listing
${loc_membershipTypeMessage}      ctl00_cphBody_uiwcNotification_lblMessage
${loc_membershipTypeRowsCount}    ctl00_cphBody_ucgvList_uilblRowsVisible_Top
${loc_tableMembershipTypeList}    ctl00_cphBody_ucgvList_uigvItemList
#Parent System Code Search
${loc_cboMembershipTypeSearchBy}    ctl00_cphBody_ucgvList_uiddlSearchItem                                                           
${loc_inputMembershipTypeValue}     ctl00_cphBody_ucgvList_uitxtSearchKeyword
${loc_btnMembershipTypeSearch}      ctl00_cphBody_ucgvList_uibtnSearch
#Delete
${loc_btnMembershipTypeDeleteCancel}     ctl00_cphBody_uibtnCancel
${loc_inputMembershipTypeDeleteReason}    uitxtDeleteReason
${loc_btnoMembershipTypeDeleteProceed}    ctl00_cphBody_uibtnProceed
#Create Membership Type
${loc_inputMembershipTypeCode}    ctl00_cphBody_uitxtMemTypeCode
${loc_inputMembershipTypeName}    ctl00_cphBody_uitxtMemTypeName
${loc_inputMembershipTypeFormat}    ctl00_cphBody_uitxtMemTypeFormat
${loc_inputMembershipTypePrintedNameLength}    ctl00_cphBody_uitxtMemTypePrintedNameLen
${loc_cboMembershipTypeEntity}    ctl00_cphBody_uiddlMemTypeEntity
${loc_cboMembershipTypeProgramType}    ctl00_cphBody_uiddlMemTypeProgramType
${loc_cboMembershipTypeRoundingOptions}    ctl00_cphBody_uiddlMemTypeRounding
${loc_inputMembershipTypeCodeTier}    ctl00_cphBody_uitxtMemTypeTier
${loc_inputMembershipTypeNameTier}    ctl00_cphBody_uitxtMemTypeTierName
${loc_cboMembershipTypeCycleDurationType}    ctl00_cphBody_uiddlMemTypeExpiryLenType
${loc_inputMembershipTypeCycleDurationLength}    ctl00_cphBody_uitxtMemTypeExpiryLen
${loc_btnMembershipTypeAddTier}    ctl00_cphBody_uibtnAddTier
