*** Variables ***
#Create System Code
${loc_inputCustomCode}        ctl00_cphBody_uitxtCustomCode                             
${loc_inputSystemCodeWarning}    ctl00_cphBody_uibtnCodeAlreadyUse
${loc_inputCustomCodeName}    ctl00_cphBody_uitxtCustomCodeName
${loc_inputCustomCodeDesc}     ctl00_cphBody_uitxtSysDesc
${loc_inputCustomCodePriority}    ctl00_cphBody_uitxtOrder  
${loc_chckBoxCustomActive}    uichkIsActive  
${loc_btnCustomCodeSubmit}    ctl00_cphBody_uibtnSaveItem
#Parent System Code Listing
${loc_customCodeMessage}      ctl00_cphBody_ucNotification_lblMessage
${loc_customCodeRowsCount}    ctl00_cphBody_ucgvCustomCode_uilblRowsVisible_Top
${loc_tableCustomCodeList}    ctl00_cphBody_ucgvCustomCode_uigvItemList
#Parent System Code Search
${loc_cboParentCustomSearchBy}    ctl00_cphBody_ucgvCustomCode_uiddlSearchItem
${loc_inputParentCustomValue}     ctl00_cphBody_ucgvCustomCode_uitxtSearchKeyword
${loc_btnParentCustomSearch}      ctl00_cphBody_ucgvCustomCode_uibtnSearch
#Child System Code Search
${loc_cboChildCustomSearchBy}    ctl00_cphBody_ucgvCustomCodeChild_uiddlSearchItem
${loc_inputChildCustomValue}     ctl00_cphBody_ucgvCustomCodeChild_uitxtSearchKeyword
${loc_btnChildCustomSearch}      ctl00_cphBody_ucgvCustomCodeChild_uibtnSearch
#Child System Code Listing
${loc_childCustomCodeRowsCount}    ctl00_cphBody_ucgvCustomCodeChild_uilblRowsVisible_Top
${loc_tableChildCustomCodeList}    ctl00_cphBody_ucgvCustomCodeChild_uigvItemList
#Delete
${loc_btnSystemCodeDeleteCancel}     ctl00_cphBody_uibtnCancel
${loc_inputSystemCodeDeleteReason}    uitxtDeleteReason
${loc_btnSystemCodeDeleteProceed}    ctl00_cphBody_uibtnProceed