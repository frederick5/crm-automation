*** Variables ***
#Create System Code
${loc_inputSystemCode}        ctl00_cphBody_uitxtSystemCode
${loc_inputSystemCodeWarning}    ctl00_cphBody_uibtnCodeAlreadyUse
${loc_inputSystemCodeName}    ctl00_cphBody_uitxtSystemCodeName
${loc_inputSysteCodeDesc}     ctl00_cphBody_uitxtSysDesc
${loc_inputSystemCodePriority}    ctl00_cphBody_uitxtOrder  
${loc_chckBoxSystemActive}    uichkIsActive  
${loc_btnSystemCodeSubmit}    ctl00_cphBody_uibtnSaveItem
#Parent System Code Listing
${loc_systemCodeMessage}      ctl00_cphBody_ucNotification_lblMessage
${loc_systemCodeRowsCount}    ctl00_cphBody_ucgvSystemCode_uilblRowsVisible_Top
${loc_tableSystemCodeList}    ctl00_cphBody_ucgvSystemCode_uigvItemList
#Parent System Code Search
${loc_cboParentSystemSearchBy}    ctl00_cphBody_ucgvSystemCode_uiddlSearchItem                            
${loc_inputParentSystemValue}     ctl00_cphBody_ucgvSystemCode_uitxtSearchKeyword
${loc_btnParentSystemSearch}      ctl00_cphBody_ucgvSystemCode_uibtnSearch
#Child System Code Search
${loc_cboChildSystemSearchBy}    ctl00_cphBody_ucgvSystemCodeChild_uiddlSearchItem
${loc_inputChildSystemValue}     ctl00_cphBody_ucgvSystemCodeChild_uitxtSearchKeyword
${loc_btnChildSystemSearch}      ctl00_cphBody_ucgvSystemCodeChild_uibtnSearch
#Child System Code Listing
${loc_childSystemCodeRowsCount}    ctl00_cphBody_ucgvSystemCodeChild_uilblRowsVisible_Top
${loc_tableChildSystemCodeList}    ctl00_cphBody_ucgvSystemCodeChild_uigvItemList
#Delete
${loc_btnSystemCodeDeleteCancel}     ctl00_cphBody_uibtnCancel
${loc_inputSystemCodeDeleteReason}    uitxtDeleteReason
${loc_btnSystemCodeDeleteProceed}    ctl00_cphBody_uibtnProceed